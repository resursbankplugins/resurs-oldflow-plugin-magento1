<?php

class Resursbank_Resursbank_Block_Adminhtml_Sales_Order_View_Info extends Mage_Adminhtml_Block_Sales_Order_View_Info
{
	protected function _toHtml()
	{
        $returninfo = "";
        $errorMsg = "";
        $errList = "";
        // Step by step-trials since there may different failures in this method, that makes the infoview not show properly.
		try
		{
			$html = parent::_toHtml();
            $order = $this->getOrder();
			$storeID = $order->getStoreId();
			$method = $order->getPayment()->getMethod();
			if(preg_match('/resurspayment/i', $method))
			{
                /*
                 * #38011
                 * #43495
				 *
                 */

                $repId = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_id', $storeID);

                // Pick up payment and order id from ecommerce. This part should make it safer to get information
                $payment = $order->getPayment();
                $payment->getAdditionalInformation("payment_id");
                $paymentId = $order->getId();
                $orderId = $order->getRealOrderId();
                $realPaymentMethod = null;
                $realPaymentMethodName = null;
                $orderPaymentMethod = null;
                try {
                    $getApiPayment = Mage::getModel('resursbank/api')->getPayment($orderId, $storeID);
                    $realPaymentMethod = $getApiPayment->paymentMethodId;
                    $method = $realPaymentMethod;
                    $orderPaymentMethod = "Resurspayment" . $repId . $realPaymentMethod;
                    $realPaymentMethodName = $getApiPayment->paymentMethodName;
                }
                catch (Exception $e) {}

                //////////////// 1.2.5 Method Structure fix - Trying to survive without it
                $hasMissingMethod = false;
                $methodFail = false;
                // Fetch information about this payment - if this first part is failing, mark the method as failed
                try {$info = $order->getPayment()->getMethodInstance()->getInfoInstance();} catch (Exception $fail) {$methodFail = true;}
                $doChangeId = null;
                // If the payment-test failed, let's check if it is a because of the payment method (method structure changed in 1.2.5 which is a issue for historical payments)
                if ($methodFail) {
                    $methodCollection = Mage::getModel('resursbank/methods')->getCollection();
                    // Loop through methods and see if there is a match between any of the "newer" methods, which is marked as resurspayment<representative>methodid.
                    foreach ($methodCollection as $methodCollectionInfo) {
                        if ($methodCollectionInfo->getId()) {
                            $existingID = $methodCollectionInfo->getPaymentId();
                            $existingUser = $methodCollectionInfo->getUserId();

                            // If we have a match with resurspayment<noRepresentative>methodid (meaning representative id is missing but the id is the same)...
                            $oldId = preg_replace("/^".$existingUser."/", 'resurspayment', $existingID);
                            // ... then ask Magento to change the payment method in the current historical order with $doChangeId
                            if ($method == $oldId) {$doChangeId = "resurspayment" . $existingID;}
                        }
                    }
                    // Now, if there is a method-renaming request, we will now save the new payment method to the order.
                    if (!is_null($doChangeId)) {
                        $method = $doChangeId;
                        //$order->addStatusToHistory(Mage::helper("resursbank")->__("Payment method renamed due to a plugin version change"));
                        $order->addStatusHistoryComment(Mage::helper("resursbank")->__("Payment method renamed due to a plugin version change"));
                        $payment = $order->getPayment();
                        $payment->setMethod($method);
                        $payment->save();
                        $order->save();
                        echo '
                        <script>
                        location.reload();
                        </script>
                        ';
                    }
                    else {
                        $hasMissingMethod = true;
                        // If we reach this point, Magento will throw an exception which takes you away completely from something sane to view.
                        //$errNotice = '<div style="border:1px solid black;background-color: #FFE7A1;margin:5px;padding:5px;">' . $orderPaymentMethod . ": " . $fail->getMessage() . "</div>";
                        //echo '<script>alert("'.$orderPaymentMethod . ": " . $fail->getMessage() .'")</script>';
                        //throw new Exception($errNotice);
                        //Mage::getSingleton("core/session")->addError($errNotice);
                    }
                }
                ////////////////////////

                // Look for our models with two different methods
                $modelTry1Fail = false;
                $modelTry2Fail = false;
                $modelMessage = null;
                try {
                    $model = Mage::getModel($orderPaymentMethod);
                    $infoTemplate = new Mage_Core_Block_Template();
                    $infoTemplate->setTemplate("resursbank/sales/order/info.phtml");
                    $info = $order->getPayment()->getMethodInstance()->getInfoInstance();
                    $ssn = $info->getAdditionalInformation('ssn');
                }
                catch (Exception $methodException)
                {
                    $modelTry1Fail = true;
                    $modelMessage = $methodException->getMessage();
                }
                if ($modelTry1Fail)
                {
                    try {
                        $model = Mage::getModel("Resursbank_Resursbank_Model_" . $orderPaymentMethod);
                        $infoTemplate = new Mage_Core_Block_Template();
                        $infoTemplate->setTemplate("resursbank/sales/order/info.phtml");
                        $info = $order->getPayment()->getMethodInstance()->getInfoInstance();
                        $ssn = $info->getAdditionalInformation('ssn');
                    }
                    catch (Exception $methodException)
                    {
                        // Still can't find any model...
                        $nextException = (!is_null($modelMessage) ? $modelMessage . "<br>\n" : "") . $methodException->getMessage();
                        $modelTry2Fail = true;
						// $orderPaymentMethod
                        $errorMsg = '<div style="border:1px solid black;background-color: #FFE7A1;margin:5px;padding:5px;">' . $nextException . "</div>";
                        $errList .= $errorMsg;
                        echo $errorMsg;
                    }
                }

                // Now, get the payment information from ecom, if we missed the first time, redo the action and continue
                try
                {
                    if ($realPaymentMethod) {$paymentInfo = $getApiPayment;} else {$paymentInfo = Mage::getModel('resursbank/api')->getPayment($paymentId, $storeID);}
                    $infoTemplate->assign("details", $paymentInfo);
                }
                catch (Exception $returnException)
                {
					// $method
                    $errorMsg = '<div style="border:1px solid black;background-color: #FFE7A1;margin:5px;padding:5px;">' . $returnException->getMessage() . "</div>";
                    $errList .= $errorMsg;
                    echo $errorMsg;
                }
                try
                {
                    if (!$modelTry2Fail)
                    {
                        $returninfo = $infoTemplate->toHtml() . $html;
                    }
                    else{
                        //return $nextException;
						// $method
                        $errorMsg = '<div style="border:1px solid black;background-color: #FFE7A1;margin:5px;padding:5px;">' . $nextException . "</div>";
                        $errList .= $errorMsg;
                        echo $errorMsg;
                    }
                }
                catch (Exception $genHtmlException)
                {
					// $method
                    $errorMsg = '<div style="border:1px solid black;background-color: #FFE7A1;margin:5px;padding:5px;">' . $genHtmlException->getMessage() . "</div>";
                    $errList .= $errorMsg;
                    echo $errorMsg;
                }
                if (!$returninfo) {$returninfo = $errList;}
                //if (!$html) {exit;}
                return $returninfo;
			}
			return $html;
		}
		catch (Exception $toHtmlException)
        {
        }
	}
}
