<?php

class Resursbank_Resursbank_Block_Adminhtml_System_Config_Backend_Baseurllive extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		$websites=Mage::app()->getWebsites();
		$current_scope = 0;
		// Catch the scope where we want to put this
		foreach ($websites as $website)
		{
			$code = $website->getCode();
			if ($code == $this->getRequest()->getParam('website')) {$current_scope = $website->getId();}
		}
		// Check current config and choose if we should use defaults or another setting
		$baseurl = Mage::getStoreConfig('payment/common_settings/baseurl_live', $current_scope);
        $cururl = $baseurl ? $baseurl : 'https://ecommerce.resurs.com/ws/V4/';
		return '<input id="payment_common_settings_baseurl_live" size="45" name="groups[common_settings][fields][baseurl_live][value]" value="'.$cururl.'">';
 	}
}