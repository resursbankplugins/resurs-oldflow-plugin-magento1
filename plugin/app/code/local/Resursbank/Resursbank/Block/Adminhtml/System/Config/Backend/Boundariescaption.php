<?php

class Resursbank_Resursbank_Block_Adminhtml_System_Config_Backend_Boundariescaption extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
        $paymentMethod = preg_replace("/^payment_resurspayment|_payment_method_maxmin$/", '', $element->getId());
        $methodCollection = Mage::getModel('resursbank/methods')->getCollection();
        $min = null;
        $max = null;
        try {
            foreach ($methodCollection as $methodCollectionInfo) {
                $methPaymentId = $methodCollectionInfo->getPaymentId();
                if ($methPaymentId == $paymentMethod) {
                    $min = $methodCollectionInfo->getMinLimit();
                    $max = $methodCollectionInfo->getMaxLimit();
                }
            }
        }
        catch (Exception $e) {}
		return $min . ' - ' . $max;
 	}
}
