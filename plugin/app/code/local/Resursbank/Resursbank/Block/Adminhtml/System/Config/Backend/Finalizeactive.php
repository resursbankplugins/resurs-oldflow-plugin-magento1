<?php

class Resursbank_Resursbank_Block_Adminhtml_System_Config_Backend_Finalizeactive extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		$finalizeactive = Mage::getStoreConfig('payment/resursapi/finalizeactive');

		return '
        <select size="1" id="payment_resursapi_finalizeactive" name="groups[resursapi][fields][finalizeactive][value]">
		<option value="0" '.($finalizeactive == "0" || $finalizeactive == "" ? 'selected="selected"' : '').'>' . Mage::helper("resursbank")->__("Do not handle automatically") . '</option>
		<option value="1" '.($finalizeactive == "1" ? 'selected="selected"' : '').'>' . Mage::helper("resursbank")->__("Handle automatically") . '</option>
		</select>
		';
 	}
}