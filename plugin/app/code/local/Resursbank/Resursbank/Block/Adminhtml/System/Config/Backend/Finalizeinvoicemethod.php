<?php

class Resursbank_Resursbank_Block_Adminhtml_System_Config_Backend_Finalizeinvoicemethod extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		$finalizeinvoicemethod = Mage::getStoreConfig('payment/resursapi/finalizeinvoicemethod');

		return '
        <select size="1" id="payment_resursapi_finalizeinvoicemethod" name="groups[resursapi][fields][finalizeinvoicemethod][value]">
		<option value="0" '.($finalizeinvoicemethod == "0" || $finalizeinvoicemethod == "" ? 'selected="selected"' : '').'>' . Mage::helper("resursbank")->__("Let Resurs Bank handle invoice numbers") . '</option>
		<!--<option value="1" '.($finalizeinvoicemethod == "1" ? 'selected="selected"' : '').'>' . Mage::helper("resursbank")->__("Let Magento handle invoice numbers through order id (not payment id)") . '</option>-->
		</select><br>
		<i>'.Mage::helper("resursbank")->__("Invoice numbers are automatically set to 1 if no invoice number initially can be found. If you wish to change the sequence to something else than 1, this can be done from Betaladmin.") . '</i><br>';
 	}
}