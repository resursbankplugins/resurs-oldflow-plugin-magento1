<?php

class Resursbank_Resursbank_Block_Adminhtml_System_Config_Backend_Locationsignfail extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		$websites=Mage::app()->getWebsites();
		$current_scope = 0;
		// Catch the scope where we want to put this
		foreach ($websites as $website)
		{
			$code = $website->getCode();
			if ($code == $this->getRequest()->getParam('website')) {$current_scope = $website->getId();}
		}
		// Check current config and choose if we should use defaults or another setting
		$locationsignfail = Mage::getStoreConfig('payment/common_settings/locationsignfail', $current_scope);
		
		return '<select size="1" id="payment_common_settings_locationsignfail" name="groups[common_settings][fields][locationsignfail][value]">
		<option value="0" '.($locationsignfail == "0" || $locationsignfail == "" ? 'selected="selected"' : '').'>'.Mage::helper('resursbank')->__('Cart').'</option>
		<option value="1" '.($locationsignfail == "1" ? 'selected="selected"' : '').'>'.Mage::helper('resursbank')->__('Checkout').'</option>
		</select>
		';
 	}
}