<?php

class Resursbank_Resursbank_Block_Adminhtml_System_Config_Backend_Logourl extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		if (strlen($code = Mage::getSingleton('adminhtml/config_data')->getStore())) // store level 
		{
			$current_scope = Mage::getModel('core/store')->load($code)->getId();
		}
		elseif (strlen($code = Mage::getSingleton('adminhtml/config_data')->getWebsite())) // website level
		{
			$website_id = Mage::getModel('core/website')->load($code)->getId();
			$current_scope = Mage::app()->getWebsite($website_id)->getDefaultStore()->getId();
		}
		else // default level
		{
			$current_scope = 0;
		}
	
		$html = parent::_getElementHtml($element);
		$htmlId = $element->getHtmlId();
		$htmlIdLogo = preg_replace("/^payment_|_paymentlogo$/i", '', $htmlId);
		
		// Check current config and choose if we should use defaults or another setting
		//$baseurl = Mage::getStoreConfig('payment/common_settings/baseurl_paymentlogo', $current_scope);
		$baseurl = Mage::getStoreConfig("payment/".$htmlIdLogo."/paymentlogo", $current_scope);
		
        $cururl = $baseurl ? $baseurl : '/frontend/base/default/resursbank/images/resursbank.png';
		$defaultLogo = Mage::getBaseUrl('skin') . '/frontend/base/default/resursbank/images/resursbank.png';
		$displogo = $cururl;
		// Always default to skin-path
		if (preg_match("/^\//", $displogo)) {$displogo = Mage::getBaseUrl('skin') . $displogo;}
		$skinPlace = Mage::getBaseUrl('skin');
		return '
		<input id="'.$htmlId.'" size="45" name="groups['.$htmlIdLogo.'][fields][paymentlogo][value]" onchange="changePaymentLogoUrl(\''.$htmlId.'\', \''.$skinPlace.'\', \''.$defaultLogo.'\')" value="' . $cururl . '"><br />
		<span style="font-weight:bold;cursor:pointer;font-style:italic;">'.$this->__("Current logotype").'</span><br><img id="'.$htmlId.'_img" src="'.$displogo.'" />';
 	}
}