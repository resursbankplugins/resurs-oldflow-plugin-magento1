<?php

class Resursbank_Resursbank_Block_Adminhtml_System_Config_Backend_Passwordfieldpasswordexpiration extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		$websites=Mage::app()->getWebsites();
		$current_scope = 0;
		// Catch the scope where we want to put this
		foreach ($websites as $website)
		{
			$code = $website->getCode();
			if ($code == $this->getRequest()->getParam('website')) {$current_scope = $website->getId();}
		}

		$saltkey = Mage::getStoreConfig('payment/callback_credentials/digestsalt_passwordexpiration', $current_scope);
		return '<input id="payment_callback_credentials_digestsalt_passwordexpiration" readonly type="password" name="groups[callback_credentials][fields][digestsalt_passwordexpiration][value]" value="'.($saltkey ? $saltkey : "").'">';
		//return '<input id="payment_callback_credentials_digestsalt_passwordexpiration" disabled="disabled" name="groups[callback_credentials][fields][digestsalt_passwordexpiration][value]" value="'.($saltkey ? $this->__('Salt key not shown') : $this->__('Salt key not set')).'">';
 	}
}