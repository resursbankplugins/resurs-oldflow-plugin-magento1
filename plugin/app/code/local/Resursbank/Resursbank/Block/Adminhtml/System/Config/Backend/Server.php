<?php

class Resursbank_Resursbank_Block_Adminhtml_System_Config_Backend_Server extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		$websites=Mage::app()->getWebsites();
		$current_scope = 0;
		// Catch the scope where we want to put this
		foreach ($websites as $website)
		{
			$code = $website->getCode();
			if ($code == $this->getRequest()->getParam('website')) {$current_scope = $website->getId();}
		}
		// Check current config and choose if we should use defaults or another setting
		$environment = Mage::getStoreConfig('payment/common_settings/server', $current_scope);
		
		$envLive = Mage::getStoreConfig('payment/common_settings/baseurl_live', $current_scope);
		$envTest = Mage::getStoreConfig('payment/common_settings/baseurl_test', $current_scope);

		$liveurl = 'https://ecommerce.resurs.com/ws/V4/';
		$testurl = 'https://test.resurs.com/ecommerce-test/ws/V4/';
		if (!$envLive) {$envLive = $liveurl;}
		if (!$envTest) {$envTest = $testurl;}
		
		return '<select size="1" id="payment_common_settings_server" name="groups[common_settings][fields][server][value]" onchange="showSelectedEnvironment(this.value)">
		<option value="1" '.($environment == "1" || $environment == "" ? 'selected="selected"' : '').'>Test</option>
		<option value="0" '.($environment == "0" ? 'selected="selected"' : '').'>Live</option>
		</select>
		<br>
		<script>
		function showSelectedEnvironment(chosenValue)
		{
			if (null != document.getElementById("currentEnvironment"))
			{
				if (chosenValue == "" || chosenValue == "1")
				{
					document.getElementById("currentEnvironment").innerHTML = document.getElementById("payment_common_settings_baseurl_test").value;
				}
				else
				{
					document.getElementById("currentEnvironment").innerHTML = document.getElementById("payment_common_settings_baseurl_live").value;
				}
			}
		}
		</script>
		<b>'.$this->__('Chosen environment URL').':</b><div id="currentEnvironment">'.($environment == "1" || $environment == "" ? $envTest : $envLive).'</div>
		';
 	}
}