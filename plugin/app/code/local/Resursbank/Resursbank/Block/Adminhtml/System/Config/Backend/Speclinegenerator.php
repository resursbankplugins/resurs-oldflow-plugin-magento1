<?php

class Resursbank_Resursbank_Block_Adminhtml_System_Config_Backend_Speclinegenerator extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		$speclinemethod = Mage::getStoreConfig('payment/resursapi/speclinemethod');

		return Mage::helper("resursbank")->__("Warning: Experimental settings! Only change them if you know what you are doing") . '
        <select size="1" id="payment_resursapi_speclinemethod" name="groups[resursapi][fields][speclinemethod][value]">
		<option value="0" '.($speclinemethod == "0" || $speclinemethod == "" ? 'selected="selected"' : '').'>' . Mage::helper("resursbank")->__("Use a regular flow to add speclines (Default and recommended)") . '</option>
		<option value="1" '.($speclinemethod == "1" ? 'selected="selected"' : '').'>' . Mage::helper("resursbank")->__("Add products from bundles into specline/betaladmin (Experimental)") . '</option>
		</select>
		';
 	}
}