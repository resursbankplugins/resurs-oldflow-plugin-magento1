<?php

class Resursbank_Resursbank_Block_Adminhtml_System_Config_Fieldset_Expanded extends Mage_Adminhtml_Block_System_Config_Form_Fieldset
{
    protected function _getCollapseState($element)
    {
        $extra = Mage::getSingleton('admin/session')->getUser()->getExtra();
        if (isset($extra['configState'][$element->getId()])) {
            return $extra['configState'][$element->getId()];
        }
        return true;
    }
	public function render(Varien_Data_Form_Element_Abstract $element)
	{
		$this->setElement($element);

		// Adding header as always
        $html = $this->_getHeaderHtml($element);
        foreach ($element->getSortedElements() as $field) {$html.= $field->toHtml();}
        $html .= $this->_getFooterHtml($element);

		$paymentId = null;
		if (preg_match("/^payment_resurspayment/i", $element->getId()))
		{
			$paymentId = preg_replace("/^payment_resurspayment/", '', $element->getId());

			//echo "Found method: $paymentId<br>\n";
			
			// Pick up methods and their representative usernames
			$methodCollection = Mage::getModel('resursbank/methods')->getCollection();
			$payments = array();
			foreach($methodCollection as $method)
			{
				$repId = $method->getUserId();
				$methId = $method->getPaymentId();
				$payments[$methId] = $repId;
			}

			// Now pick up the current store id
			if (strlen($code = Mage::getSingleton('adminhtml/config_data')->getStore())) // store level 
			{
				$store_id = Mage::getModel('core/store')->load($code)->getId();
			}
			elseif (strlen($code = Mage::getSingleton('adminhtml/config_data')->getWebsite())) // website level
			{
				$website_id = Mage::getModel('core/website')->load($code)->getId();
				$store_id = Mage::app()->getWebsite($website_id)->getDefaultStore()->getId();
			}
			else // default level
			{
				$store_id = 0;
			}
			
			// And then, pick up the current store's representative
			$representativeId = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_id', $store_id);
			
			$paymentRepresentative = isset($payments[$paymentId]) ? $payments[$paymentId] : null;
			
			// And last, but not least, compare this method, with both of the representatives
			$hideFromAdmin = false;
			if ($representativeId != $paymentRepresentative)
			{
				// ... and prepare hiding methods that does not belong to the representative
				$hideFromAdmin = true;
			}
			
			
			// Now, fulfil the above stuff.
			if ($hideFromAdmin)
			{
				// First var here is used for debugmodes
				//$header = preg_replace("/\"section-config\"/i", '"section-config" style="display:;border:3px inset gray;"', $this->_getHeaderHtml($element));
				
				// Note: If this header is ever shown, it should be marked with inactive methods
				$header = preg_replace("/\"section-config\"/i", '"section-config" style="border:3px inset gray;display:none;"', $this->_getHeaderHtml($element));
				$fields = null;
				foreach ($element->getSortedElements() as $field)
				{
					$curfield = $field->toHtml();
					$curFN = $field->getName();
					if (preg_match("/\[fields\]\[active\]\[value\]/", $curfield))
					{
						$curfield = preg_replace('/" selected="selected"/', '"', $curfield);
						$curfield = preg_replace('/disabled="disabled"/', '"', $curfield);	// This one, if exist, must get enabled if in a different space than main store since this is a default value for "use website".
						$curfield = preg_replace('/="0"/', '="0" selected="selected"', $curfield);
					}
					if (preg_match("/\[fields\]\[active\]\[inherit\]/", $curfield))
					{
						$curfield = preg_replace('/checked="checked"/', '', $curfield);
					}
					
					$fields .= $curfield;
				}
				$footer = $this->_getFooterHtml($element);
				$html = $header . $fields . $footer;
			}
		}
        return $html;
	}
}
