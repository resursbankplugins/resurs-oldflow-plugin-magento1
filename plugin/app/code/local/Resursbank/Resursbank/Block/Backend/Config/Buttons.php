<?php

class Resursbank_Resursbank_Block_Backend_Config_Buttons extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setTemplate('resursbank/buttons.phtml');
        $this->assign('field', "fetch_payment_methods");
        return $this->toHtml();
    }
}
