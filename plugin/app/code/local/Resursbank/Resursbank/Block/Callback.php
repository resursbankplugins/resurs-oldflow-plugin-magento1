<?php
class Resursbank_Resursbank_Block_Callback extends Mage_Adminhtml_Block_System_Config_Form_Fieldset
{
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
	$html = $this->_getHeaderHtml($element);
	$url = Mage::getUrl('resursbank/index/callback');
	$url = substr($url,0,-1);
	
	$html .= $this->__('Please set your callback password URL as %s',$url);
	$html .= $this->_getFooterHtml($element);

	return $html;
    }
}
