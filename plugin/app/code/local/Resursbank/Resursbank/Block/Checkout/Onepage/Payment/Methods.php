<?php

class Resursbank_Resursbank_Block_Checkout_Onepage_Payment_Methods extends Mage_Checkout_Block_Onepage_Payment_Methods
{
	// First method here is the wrong place. But we'll keep it for our historical records
	/*
    public function getMethodTitle(Mage_Payment_Model_Method_Abstract $method)
	{
        $form = $this->getChild('payment.method.' . $method->getCode());

		$scope = Mage::app()->getWebsite()->getId();
		$imgLogo = Mage::getStoreConfig('payment/' . $method->getCode() . '/paymentlogo');
		if (preg_match("/^\//", $imgLogo)) {$imgLogo = Mage::getBaseUrl('skin') . $imgLogo;}
		
		$imgsrc = '<img src="'.$imgLogo.'">';
		return $this->escapeHtml($method->getTitle());
	}
	*/

	// Method similar to an earlier logourl method, but this allows multiple logos at a site with branding requirements
	public function getMethodLabelAfterHtml(Mage_Payment_Model_Method_Abstract $method)
	{
		$methodCode = $method->getCode();
		// Make sure that only Resurs Bank is affected by this
		if (preg_match("/resurspayment/i", $methodCode))
		{
			$resurslogo = Mage::getConfig()->getBlockClassName('core/template');
			$resurslogo = new $resurslogo;
			$resurslogo->setTemplate('resursbank/brandinglogo.phtml')->setResursLogo($this->getLogo($methodCode));
			return $resurslogo->toHtml();
		}
		else
		{
			return parent::getMethodLabelAfterHtml($method);
		}
	}
	public function getLogo($methodCode = null)
	{
		$scope = Mage::app()->getWebsite()->getId();
		if (preg_match("/resurspayment/i", $methodCode))
		{
			$imgLogo = Mage::getStoreConfig('payment/' . $methodCode . '/paymentlogo');
			if ($imgLogo == "") {$imgLogo = Mage::getBaseUrl('skin') . '/frontend/base/default/resursbank/images/resursbank.png';}
			if (preg_match("/^\//", $imgLogo)) {$imgLogo = Mage::getBaseUrl('skin') . $imgLogo;}
			return $imgLogo;
		}
	}
}
?>