<?php
class Resursbank_Resursbank_Block_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        $resurslogo = Mage::getConfig()->getBlockClassName('core/template');
        $resurslogo = new $resurslogo;
        $resurslogo->setTemplate('resursbank/logo.phtml')
            ->setResursLogo($this->getLogo());

        $this->setTemplate('resursbank/form.phtml')
            ->setMethodLabelAfterHtml($resurslogo->toHtml());

        parent::_construct();
    }
    
    public function getLogo()
    {
		$scope = Mage::app()->getWebsite()->getId();
		$logourl = Mage::getStoreConfig('payment/common_settings/baseurl_paymentlogo', $scope);
		if ($logourl == "") {$logourl = Mage::getBaseUrl('skin') . '/frontend/base/default/resursbank/images/resursbank.png';}
		// Default all local uris to skin-path
		if (preg_match("/^\//", $logourl)) {$logourl = Mage::getBaseUrl('skin') . $logourl;}
        return $logourl;
    }
    
    protected function getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }
    
    public function getQuote()
    {
        return $this->getCheckout()->getQuote();
    }
}
?>
