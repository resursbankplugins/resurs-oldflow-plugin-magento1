<?php 

class Resursbank_Resursbank_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function formatFee()
    {
        return $this->__('Payment Fee');
    }
    
    public function isDebug()
    {
        return true;
    }
    
    public function getQuoteDiscountAmount()
    {
        $discount = 0;
        $totals = Mage::getSingleton('checkout/cart')->getQuote()->getTotals(); 

        if(isset($totals['discount']) && $totals['discount']->getValue())
        {
            //$discount = round($totals['discount']->getValue()); 
            $discount = $totals['discount']->getValue(); 
        }
        return $discount;
    }
    
    public function getQuoteShippingInclTax()
    {
        $address = Mage::getSingleton('checkout/cart')->getQuote()->getShippingAddress();
        return $address->getShippingInclTax();
    }

    public function getQuoteShippingTaxAmount()
    {
        $address = Mage::getSingleton('checkout/cart')->getQuote()->getShippingAddress();
        return $shippingTaxAmount = $address->getShippingTaxAmount();
    }

    public function getQuoteShippingExclTax()
    {
        $address = Mage::getSingleton('checkout/cart')->getQuote()->getShippingAddress();
        return $address->getShippingAmount();
    }

    public function getShippingTaxRate()
    {
        $calc = Mage::getSingleton('tax/calculation');
        $config = Mage::getSingleton('tax/config');
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $address = $quote->getShippingAddress();
        
        $store              = $quote->getStore();
        $storeTaxRequest    = $calc->getRateOriginRequest($store);
        $addressTaxRequest  = $calc->getRateRequest(
            $address,
            $address->getQuote()->getBillingAddress(),
            $address->getQuote()->getCustomerTaxClassId(),
            $store
        );

        $shippingTaxClass = $config->getShippingTaxClass($store);
        $storeTaxRequest->setProductClassId($shippingTaxClass);
        $addressTaxRequest->setProductClassId($shippingTaxClass);
        return $rate = $calc->getRate($addressTaxRequest);
    }
    
    public function getFeeTaxClassRate($order = null)
    {
        $calc = Mage::getSingleton('tax/calculation');
        if (is_null($order))
        {
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $address = $quote->getShippingAddress();
            $store              = $quote->getStore();
            $storeTaxRequest    = $calc->getRateOriginRequest($store);
            $addressTaxRequest  = $calc->getRateRequest(
                $address,
                $address->getQuote()->getBillingAddress(),
                $address->getQuote()->getCustomerTaxClassId(),
                $store
            );
            $taxClassId = Mage::getStoreConfig("payment/common_settings/tax_class",$quote->getStoreId());
            $storeTaxRequest->setProductClassId($taxClassId);
            $addressTaxRequest->setProductClassId($taxClassId);
        }
        else
        {
            $address = $order->getShippingAddress();
            $store              = $order->getStore();
            $storeTaxRequest    = $calc->getRateOriginRequest($store);
            $addressTaxRequest = $calc->getRateRequest(
                $address,
                $order->getBillingAddress(),
                $order->getCustomerTaxClassId(),
                $store
            );
            $taxClassId = Mage::getStoreConfig("payment/common_settings/tax_class",$order->getStoreId());
            $storeTaxRequest->setProductClassId($taxClassId);
            $addressTaxRequest->setProductClassId($taxClassId);
        }
		$taxRate = $calc->getRate($addressTaxRequest);
		return $taxRate;
        //return $calc->getRate($addressTaxRequest);
    }
    
    public function getCustomerMessage($e, $returnType = null)
    {
        // 131007 RB
        $customerMessage = "";
        if (isset($e->detail)) {
            $detail = $e->detail;
            $eError = $detail->ECommerceError;
            $customerMessage = $eError->userErrorMessage;
            if (empty($customerMessage)) {
                $customerMessage = $e->getMessage();
            }
            if (!is_null($returnType))
            {
                $customerMessage = array(
                    'errorTypeDescription' => $eError->errorTypeDescription,
                    'errorTypeId' => $eError->errorTypeId,
                    'userErrorMessage' => $eError->userErrorMessage
                );
                return $customerMessage[$returnType];
            }
        }
        return $customerMessage;
    }

    public function isSingleResursPayment()
    {
        $totalPaymentMethods = 0;
        $ignorePaymentModel = array('free','paypal_billing_agreement');
        
        $payments = Mage::getSingleton('payment/config')->getActiveMethods();
        foreach ($payments as $paymentCode=>$paymentModel)
        {
            if(!in_array($paymentCode, $ignorePaymentModel))
            {
                $totalPaymentMethods++;
            }
        }

        return $totalPaymentMethods;
    }
    
    public function formDataXml($formdata)
    {
        $domDoc = new DOMDocument;
        $rootElt = $domDoc->createElement('resurs-response');
        $rootNode = $domDoc->appendChild($rootElt);
        
        unset($formdata['method']);
        unset($formdata['disabled_methods']);
        foreach($formdata as $key=>$value)
        {
            $subElt = $domDoc->createElement($key);
            $subNode = $rootNode->appendChild($subElt);
            $textNode = $domDoc->createTextNode($value);
            $subNode->appendChild($textNode);
        }
        return $domDoc->saveXML();
    }
    
    public function _round($price, $rate, $direction, $type = 'regular')
    {
        if (!$price) {
            return Mage::getSingleton('tax/calculation')->round($price);
        }

        $deltas = Mage::getModel('sales/quote_address')->getRoundingDeltas();
        $key = $type.$direction;
        $rate = (string) $rate;
        $delta = isset($deltas[$key][$rate]) ? $deltas[$key][$rate] : 0;
        return Mage::getSingleton('tax/calculation')->round($price+$delta);
    }
}

?>
