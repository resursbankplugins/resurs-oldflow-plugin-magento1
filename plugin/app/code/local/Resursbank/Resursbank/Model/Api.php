<?php
class Resursbank_Resursbank_Model_Api extends Mage_Payment_Model_Method_Abstract
{
	protected $username;
	protected $password;
	protected $shopFlowService;
	protected $configurationService;
	protected $developerWebService;
	protected $afterShopFlowService;
	protected $_order;
	protected $_error;
    protected $isFinalizePayment;
    protected $isCreditPayment;
    protected $isAnnulPayment;

	protected $scope = 0;

    public function getResursVersion()
    {
        $plugversion = "";
        try {
            $codePath = Mage::getBaseDir('code');
            $extensionPath = $codePath.DS.'local'.DS.'Resursbank'.DS.'Resursbank';
            $etcPath = $extensionPath.DS.'etc';
            $configFile = $etcPath.DS.'config.xml';
            $string = file_get_contents($configFile);
            $xml = simplexml_load_string($string, 'Varien_Simplexml_Element');
            $plugversion = (string)$xml->modules->Resursbank_Resursbank->version;
        }
        catch (Exception $getResursVersionException) {return null;} // Suppress errors
        return $plugversion;
    }

    public function initWebservice($username='', $password='', $nothrow = false)
    {
		$testscope = (Mage::app()->getWebsite()->getId() ? Mage::app()->getWebsite()->getId() : null);
		if ($testscope != null && $this->scope != $testscope) {$this->scope = $testscope;}
		if(empty($username)) {$this->username = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_id', $this->scope);} else {$this->username = $username;}
        if(empty($password)) {$this->password = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_password', $this->scope);} else {$this->password = $password;}

		// Fetch correct Resurs Bank API URLs...
		$liveurl = Mage::getStoreConfig('payment/common_settings/baseurl_live', $this->scope);
		$testurl = Mage::getStoreConfig('payment/common_settings/baseurl_test', $this->scope);
		//$logourl = Mage::getStoreConfig('payment/common_settings/baseurl_paymentlogo', $this->scope);

		// ... and set defaults (including saving), if they do not exist!
		if (!$liveurl)
		{
			$liveurl = 'https://ecommerce.resurs.com/ws/V4/';
			Mage::getModel('core/config')->saveConfig('payment/common_settings/baseurl_live', $liveurl, $this->scope);
		}
		if (!$testurl)
		{
			$testurl = 'https://test.resurs.com/ecommerce-test/ws/V4/';
			Mage::getModel('core/config')->saveConfig('payment/common_settings/baseurl_test', $testurl, $this->scope);
		}

		$testMode = Mage::getStoreConfig('payment/common_settings/server');
		// Test environment should be our default
		if ($testMode == "") {Mage::getModel('core/config')->saveConfig('payment/common_settings/server', "1", $this->scope);}
		$inProduction = ($testMode == 1 ? false:true);
		$url = $inProduction ? $liveurl : $testurl;

		$extendedError = null;
		if (preg_match("/^https/i", $url)) {if (!in_array('https', @stream_get_wrappers())) {$extendedError = "SSL Failure: HTTPS wrapper can not be found";}}

		$streamContextOptions = array(
			'http' => array('user_agent' => "RBMagento v".$this->getResursVersion())
		);
		$soapStream = stream_context_create($streamContextOptions);
		try
		{
			// Create the client instance
			$this->shopFlowService = new SoapClient($url.'ShopFlowService?wsdl', array(
				'login'          => $this->username,
				'password'       => $this->password,
				'exceptions'=> 1,
				'connection_timeout' => 60,
				'trace'          => 1,
				'stream_context' => $soapStream
			));
			$this->configurationService = new SoapClient($url.'ConfigurationService?wsdl', array(
				'login'          => $this->username,
				'password'       => $this->password,
				'exceptions'=> 1,
				'connection_timeout' => 60,
				'trace'          => 1,
				'stream_context' => $soapStream
			));
			$this->developerWebService = new SoapClient($url.'DeveloperWebService?wsdl', array(
				'login'          => $this->username,
				'password'       => $this->password,
				'exceptions'=> 1,
				'connection_timeout' => 60,
				'trace'          => 1,
				'stream_context' => $soapStream
			));
			$this->afterShopFlowService = new SoapClient($url.'AfterShopFlowService?wsdl', array(
				'login'          => $this->username,
				'password'       => $this->password,
				'exceptions'=> 1,
				'connection_timeout' => 60,
				'trace'          => 1,
				'stream_context' => $soapStream
			));
		}
        catch(Exception $e) 
		{
			if ($extendedError != null) {$e->faultstring .= "\n\n".Mage::helper('resursbank')->__('Possibly caused by:')."\n" .Mage::helper('resursbank')->__($extendedError);}
			Mage::log(Mage::helper('resursbank')->__('Init API Service: '), null, 'resurs.log', true);
			Mage::log($e, null, 'resurs.log', true);
			if ($nothrow === true) {return array('success' => false, 'faultstring' => $e->faultstring);}
			throw $e;
		}
	}

    public function inProduction()
    {
        return ($this->getConfigData('test') == 1 ? false:true);
    }

    public function getOrder()
    {
        if (!$this->_order) {
            $this->_order = Mage::getSingleton('checkout/session')->getOrder();
        }
        return $this->_order;
    }

    //public function run()
    //{
    //    return Mage::getUrl('resursbank/processing/method');
    //}

	public function startPaymentSession($paymentMethodId,$order=null)
	{
		$this->initWebservice();
		$this->_order = $order;
		$tax =  Mage::getModel('checkout/cart')->getQuote()->getTaxAmount();
		$tax = ($tax)? $tax : '0.00';

		// Let Resurs Bank generation the session. Removed 140114
		//$preferredId = uniqid();
		// 'preferredId' => $preferredId,
		$startPaymentSession = array(
			'paymentMethodId' => str_replace($this->username, '', $paymentMethodId),
			'customerIpAddress' => $_SERVER['REMOTE_ADDR'],
			'formAction' => Mage::getUrl('resursbank/index/orderplaced/paymentMethodId/' . str_replace($this->username, '', $paymentMethodId) . "/"),
			'paymentSpec' => $this->getPaymentSpec($order)
		);
		try
		{
			Mage::log(Mage::helper('resursbank')->__('Start Payment Session: '), null, 'resurs.log', true);
			Mage::log($startPaymentSession, null, 'resurs.log', true);
			return $this->shopFlowService->startPaymentSession($startPaymentSession);
		}
		catch(Exception $e)
		{
			Mage::log(Mage::helper('resursbank')->__('Start Payment Session: '), null, 'resurs.log', true);
			Mage::log($e->getMessage(), null, 'resurs.log', true);

            $customerMessage = Mage::helper('resursbank')->getCustomerMessage($e);
            $errorType = Mage::helper('resursbank')->getCustomerMessage($e, "errorTypeId");
            // Error 8 = REFERENCED_DATA_DONT_EXISTS
            // If this error occurs here, we should recheck our payment methods and see if they are active
            // SoapFault exception: [soap:Server] Trying to start a payment session with an invalid payment method. Do you find this error strange? E-mail information about the payment to ehandel@resurs.se or follow the contact information page here: https://test.resurs.com/docs/x/9gAF in /var/www/mock.magento.cte.loc/vanilla/app/code/local/Resursbank/Resursbank/Model/Api.php:127
            if (preg_match("/method/i", $e) && intval($errorType) == 8)
            {
                $apiMethods = $this->getPaymentMethods();
                $methodListArray = array();
                if (is_array($apiMethods)) {
                    $methodList = $apiMethods['methods']->return;
					// The oneMethodIssue is a huge problem.
					if (gettype($methodList) == "object") {$methodList = array($apiMethods['methods']->return);}
                    foreach ($methodList as $method) {if ($method->id != "") {$methodListArray[] = $this->username . $method->id;}}
                }
                // If there is no payment methods available then something else has happened.
                if (!in_array($paymentMethodId, $methodListArray) && sizeof($methodListArray)> 0 )
                {
                    $storeCode = Mage::app()->getStore()->getCode();
                    $storeId = Mage::app()->getStore()->getStoreId();

                    $customerMessage = Mage::helper('resursbank')->__("Unavailable payment method");
                    Mage::log("startPaymentSession ($paymentMethodId): " . Mage::helper('resursbank')->__("Received error 8 from ecommerce") . " - " . $customerMessage, null, 'resurs.log', true);
                    Mage::log("startPaymentSession ($paymentMethodId): " . Mage::helper('resursbank')->__("Disable method") . " (Scope ".$storeId . "/" . $storeCode . ")", null, 'resurs.log', true);
                    Mage::getModel('core/config')->saveConfig('payment/resurspayment'.$paymentMethodId .'/active', 0, $storeId);
                    Mage::getModel('core/config')->saveConfig('payment/resurspayment'.$paymentMethodId .'/active', 0, $storeCode);
                }
            }
			return array(
				'success' => false,
				'error' => true,
				'message' => $customerMessage,
                'errorType' => $errorType,
				'return' => ''
			);
		}
	}

	public function submitLimitApplication($paymentSessionId,$customerId, $formData, $billingAddress = null)
	{
		try {
			$this->initWebservice();

			if($customerId == NULL) {$customerId = 0;}

			Mage::log(Mage::helper('resursbank')->__('Form Data to API Request: '), null, 'resurs.log', true);
			Mage::log($formData, null, 'resurs.log', true);

			// Fix for norway-requirements 140911 (#35108)
			// Must be able to submit billingaddressdata
			if ($billingAddress == null)
			{
				$result = $this->shopFlowService->submitLimitApplication(array(
					'paymentSessionId' => $paymentSessionId,
					'yourCustomerId' => $customerId,
					'formDataResponse' => $formData
				));
			}
			else
			{
				$result = $this->shopFlowService->submitLimitApplication(array(
					'paymentSessionId' => $paymentSessionId,
					'yourCustomerId' => $customerId,
					'formDataResponse' => $formData,
					'billingAddress' => $billingAddress
				));
			}
			Mage::log(Mage::helper('resursbank')->__('submitLimitApplication API Response: '), null, 'resurs.log', true);
			Mage::log($result, null, 'resurs.log', true);
			if($result->return->decision === "GRANTED")
			{
				return array('result'=>'success','message'=>'granted','addressdata'=>$result->return->customer->address, 'customerinfo' => $result->return->customer);
			} else {
				return array('result'=>'success','message'=>'not granted', 'customerError' => 'Your credit application has been rejected, contact Resurs Bank for more details.');
			}
        }
		catch(Exception $e)
		{
			Mage::log(Mage::helper('resursbank')->__('Submit Limit Application: '), null, 'resurs.log', true);
			Mage::log($e->getMessage(), null, 'resurs.log', true);
			return array('result'=>'error','message'=>Mage::helper('resursbank')->getCustomerMessage($e));
		}
    }

	public function prepareSigning($orderId, $paymentSessionId, $paymentId)
	{
		$successUrl = Mage::getUrl('resursbank/index/success', array('orderId' => $orderId, 'paymentSessionId' => $paymentSessionId));
		$failUrl = Mage::getUrl('resursbank/index/cancel', array('orderId' => $orderId, 'signFail' => '1', 'paymentId' => $paymentId));

		try 
		{
			$this->initWebservice();
			$result = $this->shopFlowService->prepareSigning(
					array(
						'paymentSessionId' => $paymentSessionId,
						'successUrl' => $successUrl,
						'failUrl' => $failUrl
					)
			);

			Mage::log(Mage::helper('resursbank')->__('Prepare Signing: '), null, 'resurs.log', true);
			Mage::log($result, null, 'resurs.log', true);

			$m_status = Mage::getStoreConfig('payment/common_settings/payment_signing_status', $this->scope);
			if($m_status)
			{
				$order = Mage::getModel('sales/order');
				$order->loadByIncrementId($orderId);
				$m_state_array = $this->getCustomOrderState($m_status);
				$m_state = $m_state_array['state'];
				$m_label = $m_state_array['label'];
				$m_translatestate = $this->orderStatus($m_state);
				
				Mage::log(Mage::helper('resursbank')->__('Signing state set to order') . " ".$orderId." (".$m_state."/".$m_label.")", null, 'resurs.log', true);
				$order->setState($m_state,$m_status,Mage::helper('resursbank')->__('Resurs Bank signing in progress'), false);
				$order->save();
			}
			
			if(get_class($result) == 'stdClass' AND !empty($result->return) )
			{
				Mage::log('Property Exists', null, 'resurs.log', true);
				return array(
					'success' => true,
					'error' => false,
					'message' => Mage::helper("resursbank")->__('preparesigningredirect'),
					'return' => $result->return
				);
			}
			else
			{
				return array(
				'success' => true,
				'error' => false,
				'message' => '',
				'return' => $successUrl
				);
			}
		}
		catch (Exception $e)
		{
			Mage::log(Mage::helper('resursbank')->__('Prepare Signing Exception: '), null, 'resurs.log', true);
			Mage::log($e->getMessage(), null, 'resurs.log', true);
			return array(
				'success' => false,
				'error' => true,
				'message' => Mage::helper('resursbank')->getCustomerMessage($e),
				'return' => ''
			);
		}
	}

    public function bookPayment($orderId, $paymentSessionId)
    {
		$currentOrderId = $orderId;
        $useMetaData = false;
        try
		{
			$this->initWebservice();
            $metaData = ($useMetaData === true ? array('key' => 'pluginVersion', 'value' => $this->getResursVersion()) : array());
			$bookPayment = array(
				'paymentSessionId' => $paymentSessionId,
				'preferredPaymentId' => $orderId,
				'waitForFraudControl' => false,
				'annulIfFrozen' => false,
				'metaData' => $metaData,
				'priority' => 1
			);

			$result = $this->shopFlowService->bookPayment($bookPayment);

			Mage::log(Mage::helper('resursbank')->__('Book payment Response: '), null, 'resurs.log', true);
			Mage::log($result, null, 'resurs.log', true);

			$paymentId = $result->return->paymentId;
			$status = $result->return->fraudControlStatus;
			$allowedResponse = array(
				'FROZEN',
				'NOT_FROZEN',
				'CONTROL_IN_PROGRESS'
			);

			if (in_array($status, $allowedResponse))
			{
				$order = Mage::getModel('sales/order');
				$order->loadByIncrementId($orderId);
				$payment = $order->getPayment();
				$payment->setAdditionalInformation('payment_id', $paymentId);
				$payment->save();
				return array(
					'success' => true,
					'error' => false,
					'message' => $paymentId,
					'return' => ''
				);
			}
			else
			{
				return array(
					'success' => false,
					'error' => true,
					'message' => Mage::helper("resursbank")->__('fraudfail'),
					'return' => ''
				);
			}
		}
        catch(Exception $e)
        {
			Mage::log(Mage::helper('resursbank')->__('Book payment exception: '), null, 'resurs.log', true);
			Mage::log($e->getMessage(), null, 'resurs.log', true);

			// Special case error fetching
			if (preg_match("/identity already exists/i", $e->getMessage()))
			{
				$failUrl = Mage::getUrl('resursbank/index/cancel', array('orderId' => (isset($currentOrderId) ? $currentOrderId : null), 'identityerror' => '1', 'paymentId' => (isset($paymentId) ? $paymentId : null)));
				Mage::log(Mage::helper('resursbank')->__('Book payment abort: '), null, 'resurs.log', true);
				Mage::log("(" . $currentOrderId . ") " . $e->getMessage(), null, 'resurs.log', true);
				return array(
					'success' => false,
					'error' => true,
					'message' => Mage::helper('resursbank')->__("Payment cancellation: Identity error - Already exists"),
					'return' => $failUrl
				);
			}

			// Regular error
			return array(
				'success' => false,
				'error' => true,
				'message' => Mage::helper('resursbank')->getCustomerMessage($e),
				'return' => ''
			);
		}
	}

	public function finalizePayment(Varien_Object $payment)
	{
        $finalizeactive = Mage::getStoreConfig('payment/resursapi/finalizeactive');
        if ($finalizeactive == "0" || $finalizeactive == "") {return;}
		$this->initWebservice();
		$order = $payment->getOrder();
		$this->_order = $order;

        $finalizeinvoicemethod = Mage::getStoreConfig('payment/resursapi/finalizeinvoicemethod');
        if ($finalizeinvoicemethod == "0" || $finalizeinvoicemethod == "") {$sendInvoiceId = false;} else {$sendInvoiceId = true;}

        $additionalData = $payment->getAdditionalInformation("payment_id");

		$grandTotal = 0;
		$totalTaxAmount = 0;
        $this->isFinalizePayment = true;
		$specLines = $this->getSpecLines($order);

		foreach ($specLines as $specLine)
		{
			$totalTaxAmount += $specLine['totalVatAmount'];
			$grandTotal += $specLine['totalAmount'];
		}

		$billing = $order->getBillingAddress();
		$orderId = $payment->getOrder()->getId();
		$paymentSpec = array(
			'specLines' => $specLines,
			'totalAmount' => round($grandTotal,2),
			'totalVatAmount' => round($totalTaxAmount,2)
		);
		if($sendInvoiceId == true) {$invoiceId = $orderId;} else {$invoiceId = null;}

		/* #62183 - Begin - Finalization with or without type INVOICE */
		$isInvoice = false;
		$methodType = null;
			try {
				$methodCode = $payment->getMethodInstance()->getCode();
				$methodInformation = Mage::getModel('resursbank/methods')->getCollection();
				$methodInformation->addFieldToFilter('payment_id', str_replace('resurspayment', '', $methodCode));
				foreach ($methodInformation as $method) {
					$methodType = $method->getPaymentType();
					if (!empty($methodType)) {break;}
				}
			} catch (Exception $e) {}
		if (strtoupper($methodType) === "INVOICE") { $isInvoice = true;	}
		$finalizePayment = array(
			'paymentId' => $additionalData,
			'preferredTransactionId' => $orderId,
			'partPaymentSpec' => $paymentSpec,
			'createdBy' => 'SHOP_FLOW',
			'orderId' => $orderId,
			'ourReference' => utf8_encode($order->getStore()->getWebsite()->getName()),
			'yourReference' => utf8_encode($billing->getFirstname() . " " . $billing->getLastname())
		);
		if ($isInvoice) {
			$finalizePayment['orderDate'] = date('Y-m-d', time());
			$finalizePayment['invoiceId'] = $invoiceId;
			$finalizePayment['invoiceDate'] = date('Y-m-d', time());
		}
		/* #62183 - Finish */

        Mage::log("Finalize of $additionalData in progress. Sending data, array follows.", null, 'resurs.log');
        Mage::log(print_r($finalizePayment, true), null, 'resurs.log');
		try
		{
			$result = $this->afterShopFlowService->finalizePayment($finalizePayment);
            $finalizeMessage = "[" . $additionalData . "] " . (Mage::helper("resursbank")->__("Payment finalized") ? Mage::helper("resursbank")->__("Payment finalized") : "Payment finalized");
            $order->addStatusHistoryComment($finalizeMessage);
            $order->save();
            Mage::log($finalizeMessage, null, 'resurs.log');
			return $this;
		}
		catch(Exception $e)
		{
			Mage::log(Mage::helper('resursbank')->__('Unable to save the invoice'), null, 'resurs.log', true);
			Mage::log($e->getMessage(), null, 'resurs.log', true);
			$errorMessage = Mage::helper('resursbank')->getCustomerMessage($e);
			$this->addError($this->__('Order # '.$order->getIncrementId().': Unable to save the invoice ').', '.$errorMessage);

            $historyMessage = "[" . $additionalData . "] " . (Mage::helper("resursbank")->__("Unable to save the invoice") ? Mage::helper("resursbank")->__("Unable to save the invoice") : "Unable to save the invoice");
            $order->addStatusHistoryComment($historyMessage);
            $order->save();
            return $this;
		}
	}

    /**
     * This will return payment spec for the payment
     * @param  int $orderId
     * @return array
     */
    public function getPaymentSpec($order)
    {
        $grandTotal = 0;
        $totalTaxAmount = 0;

        $specLines = $this->getSpecLines($order);

        foreach ($specLines as $specLine) {
            $totalTaxAmount += $specLine['totalVatAmount'];
            $grandTotal += $specLine['totalAmount'];
        }

        $paymentSpec = array(
            'specLines' => $specLines,
            'totalAmount' => $grandTotal,
            'totalVatAmount' => $totalTaxAmount
        );

        return $paymentSpec;
    }

    /**
     * This will return spec lines for the payment
     * @param  int $orderId
     * @return array
     */
	public function getSpecLines($order)
	{
		$specLines = array();
		$orderItems = $this->getGoodsList($order);

		//$this->applyafter = Mage::helper('tax')->applyTaxAfterDiscount($order->getStoreId());
		// Disabled htmlentities from id and artNo due to conflicts with longnames in db.

		$itemID = 0;
		foreach ($orderItems as $item)
		{
            $itemQuantity = intval($item['amount']);
            if ($this->isFinalizePayment === true && $itemQuantity == 0){$itemQuantity = 1;}

            $artNo = $item['code'];
            if (strlen($artNo) > 100) { $artNo = substr($artNo, 0, 68) . md5($artNo); }
			/**
			 * #57216 - Rounding issues when using floatval, where floatval was the only option.
			 * Refers to #44322 that describes some problems with calculations with integers.
			 */
			$theVat = floatval($item['vat']);
			$theVatInt = intval($theVat);
			if ($theVatInt > 0 && $theVat > 0 && $theVatInt != $theVat) {
				$theVatDiff = ceil($theVat - $theVatInt);
				if ($theVatDiff <= 1) {
					$theVat = $theVatInt;
				}
			}

			$specLines[] = array(
				'id' => $itemID,
				'artNo' => $artNo,
				'description' => strip_tags($item['name']),
				'quantity' => $itemQuantity,
				'unitMeasure' => Mage::helper('resursbank')->__('pcs'),
				'unitAmountWithoutVat' => round($item['price']/(1+($item['vat']/100)),2),
				'vatPct' => $theVat,
				'totalVatAmount' => round(($item['price']*$itemQuantity)-($item['price']/(1+($item['vat']/100))*$itemQuantity),2),
				'totalAmount' => round($item['price']*$itemQuantity,2)
			);
			$itemID++;
		}
		return $specLines;
	}

    /**
     * This will return information about the given payment id
     * @param  int $methodId
     * @param  float $amount
     */
    public function getCostOfPurchaseHtml($methodId, $amount = 5000)
    {
        try
        {
            $this->initWebservice();
            $result = $this->shopFlowService->getCostOfPurchaseHtml(array(
                'paymentMethodId' => str_replace($this->username, '', $methodId),
                'amount' => $amount
            ));
			if (!preg_match("/target=/is", $result->return)) {$result->return = preg_replace("/href=/is", 'target="_blank" href=', $result->return);}
            return $result->return;
        }
        catch(Exception $e)
        {
            Mage::log(Mage::helper('resursbank')->__('Get Cost Of Purchase Html: '), null, 'resurs.log', true);
            Mage::log($e->getMessage(), null, 'resurs.log', true);
            return Mage::helper('resursbank')->getCustomerMessage($e);
        }
    }

    public function getInvoiceSequenceNumber()
    {
        $this->initWebservice();
        $ret = $this->configurationService->peekInvoiceSequence(array('nextInvoiceNumber' => null));
        if (isset($ret->nextInvoiceNumber)) {return $ret->nextInvoiceNumber;} else {return null;}
    }
    public function setInvoiceSequenceNumber($sequenceNumber = null)
    {
        $this->initWebservice();
        $ret = $this->configurationService->setInvoiceSequence(array('nextInvoiceNumber' => $sequenceNumber));
        if (isset($ret->nextInvoiceNumber)) {return $ret->nextInvoiceNumber;} else {return null;}
    }

    /**
     * This will return the address for the given pno
     * @param  int|string  $pno
     * @param  string  $customerType
     */
    public function getAddresses($pno, $customerType)
    {
        $this->initWebservice();
        try {
            $data = $this->shopFlowService->getAddress(
                array(
                    'governmentId' => $pno,
                    'customerType' => $customerType
                )
            );

            $result = array(
                'Success' => 'true',
                'Error' => 'false',
                'Data' => $data->return,
                'CustomerType' => $customerType
            );
        }
        catch (Exception $e) {

            if(Mage::helper('resursbank')->isDebug())
            {
                Mage::log($e->getMessage(), null, 'resurs.log', true);
            }

            return array(
                'Success' => 'false',
                'Error' => 'true',
                'Data' => Mage::helper('resursbank')->getCustomerMessage($e),
                'CustomerType' => $customerType
            );
        }

        return $result;
    }

    /**
     * Get payment
     * Use this to get complete information about a payment
     * @param  int  $orderId
     */
    public function getPayment($paymentId, $scope = null)
    {
		if ($scope != null) {$this->scope = $scope;}
		if (null != $paymentId)
		{
			// Throw and catch? Not yet.
				$this->initWebservice();
				$result = $this->afterShopFlowService->getPayment(array(
					'paymentId' => $paymentId
				));
				return $result->return;
		}
    }

    /**
     * Get payment documents
     * @param  int  $paymentId
     */
    public function getPaymentDocumentNames($paymentId)
    {
        $ret = null;
        $result = $this->afterShopFlowService->getPaymentDocumentNames(array(
            'paymentId' => $paymentId
        ));
        if(isset($result->return))
            $ret = $result->return;

        return $ret;
    }

    /**
     * Credit a payment
     * Use this after the finalize payment
     */
    public function creditPayment($order)
    {
        $finalizeactive = Mage::getStoreConfig('payment/resursapi/finalizeactive');
        if ($finalizeactive == "0" || $finalizeactive == "") {return;}

        // Change behaviour during getGoodsList by setting isFinalizePayment to true.
        $this->isFinalizePayment = true;
        $this->isCreditPayment = true;

        $this->initWebservice();
        $payment = $order->getPayment();
        $paymentId = $payment->getAdditionalInformation("payment_id");
        $orderId = $payment->getOrder()->getId();

        $grandTotal = 0;
        $totalTaxAmount = 0;

        $specLines = $this->getSpecLines($order);

        foreach ($specLines as $specLine) {
            $totalTaxAmount += $specLine['totalVatAmount'];
            $grandTotal += $specLine['totalAmount'];
        }

        try {

            $paymentSpec = array(
                'specLines' => $specLines,
                'totalAmount' => $grandTotal,
                'totalVatAmount' => $totalTaxAmount
            );

            //'preferredTransactionId' => $orderId,
            //'creditNoteId' => $orderId,
            $result = $this->afterShopFlowService->creditPayment(
                array(
                    'paymentId' => $paymentId,
                    'partPaymentSpec' => $paymentSpec,
                    'createdBy' => 'SHOP_FLOW',
                    'creditNoteDate' => date('Y-m-d', time())
                )
            );
            $order->addStatusHistoryComment(Mage::helper("resursbank")->__("Payment credited"));
            $order->save();
            Mage::log("[$paymentId] " . Mage::helper('resursbank')->__('Payment credited'), null, 'resurs.log', true);

            return $this;
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            $this->_getSession()->addError($this->__('Unable to credit the invoice '.$errorMessage));
            $order->addStatusHistoryComment(Mage::helper("resursbank")->__("Unable to credit the invoice"));
            $order->save();
            Mage::log("[$paymentId] " . Mage::helper('resursbank')->__('Unable to credit the invoice'), null, 'resurs.log', true);
            Mage::logException($e);
            return $this;
        }
    }

    /**
     * Annul a payment
     * Use this before finalize payment but the payment must be booked
     * @param  int  $orderId
     */
    public function annulPayment($order)
    {
        $finalizeactive = Mage::getStoreConfig('payment/resursapi/finalizeactive');
        if ($finalizeactive == "0" || $finalizeactive == "") {return;}

        // Change behaviour during getGoodsList by setting isFinalizePayment to true.
        $this->isFinalizePayment = true;
        $this->isAnnulPayment = true;

        try {
            $this->initWebservice();

            $payment = $order->getPayment();
            $paymentId = $payment->getAdditionalInformation("payment_id");

            $grandTotal = 0;
            $totalTaxAmount = 0;

            $specLines = $this->getSpecLines($order);
            foreach ($specLines as $specLine) {
                $totalTaxAmount += $specLine['totalVatAmount'];
                $grandTotal += $specLine['totalAmount'];
            }
            $orderId = $payment->getOrder()->getId();

            $paymentSpec = array(
                'specLines' => $specLines,
                'totalAmount' => $grandTotal,
                'totalVatAmount' => $totalTaxAmount
            );

            $result = $this->afterShopFlowService->annulPayment(
                array(
                    'paymentId' => $paymentId,
                    'partPaymentSpec' => $paymentSpec,
                    'createdBy' => 'SHOP_FLOW'
                )
            );
            $order->addStatusHistoryComment(Mage::helper("resursbank")->__("Payment annulled"));
            $order->save();
            Mage::log("[$paymentId] " . Mage::helper('resursbank')->__('Payment annulled'), null, 'resurs.log', true);

            return $this;
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();

            $this->_getSession()->addError($this->__('Unable to annul the order.'.$errorMessage));
            $order->addStatusHistoryComment(Mage::helper("resursbank")->__("Unable to annul the order"));
            $order->save();
            Mage::log("[$paymentId] " . Mage::helper('resursbank')->__('Unable to annul the invoice'), null, 'resurs.log', true);
            Mage::logException($e);
            return $this;
        }
    }

	// Return true if successful
	// Return the returned error if failed
	// Also display js-stuff
	public function registerCallback($type,$username,$password, $digest= array(), $myscope = 0)
    {
        try {
            $regRes = $this->initWebservice($username,$password);
			// Someone did this
            //$uriTemplate = Mage::getUrl('resursbank/index/callback', array('id' => '{identifier}'));
			// But we do this, since we want to freely control parameters and digests from incoming calls. Adds callbacktype into the uritemplate.

			$uriTemplate = Mage::getUrl('resursbank/index/callback/event/' . strtolower($type) . "/storeidentifier/".$myscope."/digest/{digest}", (is_array($digest['parameters']) && sizeof($digest['parameters']) ? $digest['parameters'] : null));

			$digestparams = array();
			// Prepare and render digestive calls, depending on parameters and encryption. Default is SHA1, but can be requested externally.
			if (is_array($digest['parameters']) && sizeof($digest['parameters']))
			{
				$digestparams = array('digestAlgorithm' => ($digest['digestAlgorithm'] ? $digest['digestAlgorithm'] : 'SHA1'), 'digestSalt' => $digest['digestSalt']);  // Default parameters
		        foreach ($digest['parameters'] as $parameter => $value) {$dparm[] = $parameter;}	// Parameters must meet requirements
				$digestparams['digestParameters'] = $dparm;
			}
			$rendercb = (
                array(
                    'eventType' => $type,
                    'uriTemplate' => $uriTemplate,
                )
			);
			if (sizeof($digestparams)) {$rendercb['digestConfiguration'] = $digestparams;}
			// Always try to unregister callbacks first, before registration and look for errors by passing everything from here to unregisterCallback
			$unreg = $this->unregisterCallback($type, $username, $password, $digest, $myscope);
			// If unreg is null, then unregisterCallback has succeeded with its removal since no errorcodes are genereated.
			// If unreg is 8, then we tried to unregister a callback that don't exist (https://test.resurs.com/docs/display/ecom/Error+handling)
			//if ($unreg == 8 || $unreg == null) {$res = $this->configurationService->c($rendercb);} else {return false;}
			if ($unreg == 8 || $unreg == null) {$res = $this->configurationService->registerEventCallback($rendercb);} else {return false;}
			return true;
        }
        catch (Exception $e)
        {
			return $e;
        }
    }
	public function unregisterCallback($type, $username, $password, $digest = array(), $myscope = 0)
	{
        try {
            $this->initWebservice($username,$password);
			$rendercb = (
                array(
                    'eventType' => $type,
                    'uriTemplate' => (isset($uriTemplate) ? $uriTemplate : ""),
                )
			);
			$res = $this->configurationService->unregisterEventCallback($rendercb);
			return null;
		}
        catch (Exception $e)
        {
			return (isset($e->detail) ? $e->detail->ECommerceError->errorTypeId : "");
			return false;
        }
	}

    public function changePassword($password, $identifier = null)
    {
        try
        {
            $this->initWebservice();
            $result = $this->configurationService->changePassword(array(
				'identifier' => $identifier,
                'newPassword' => $password
            ));
			return true;
        }
        catch(Exception $e) {
            Mage::log('Password Change', null, 'resurs.log');
            Mage::log($e, null, 'resurs.log');
            return $e;
        }
    }

    public function getPaymentMethods($username='', $password='', $scope = 0)
    {
		try{
			$this->scope = $scope;	// Pre-Set scope internally (since initwebservice is already occupied with other parameters)
            $initResult = $this->initWebservice($username,$password);
            if(is_object($this->shopFlowService))
            {
                try{
					return array('error'=>'','methods' => $this->shopFlowService->getPaymentMethods());
                }
                catch(Exception $e) {
                    Mage::log($e, null, 'resurs.log');
                    return array('error'=>$e->getMessage(),'methods' => array());
                }
            }
            else
            {
                return array('error'=>'','methods' => array());
            }
        }
        catch(Exception $e) {
            Mage::log(Mage::helper('resursbank')->__('Get Payment Methods: '), null, 'resurs.log', true);
            Mage::log($e->getMessage(), null, 'resurs.log', true);
			$errorMessage = Mage::helper('resursbank')->getCustomerMessage($e);
			if (!$errorMessage)
			{
				$errorMessage = $e->getMessage();
			}
            return array('error'=> $errorMessage,'methods' => array());
        }
    }

    public function assignData($data)
    {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }

        $this->info = $this->getInfoInstance();
        $this->info->setAdditionalInformation('ssn', $data->getSsn());
        $this->info->setAdditionalInformation('cellphone', $data->getCellphone());

        $info = $this->getInfoInstance();
        $info->setSsn($data->getSsn());
        $info->setCellphone($data->getCellphone());

        $quote = $this->info->getQuote();
        $this->shippingAddress = $quote->getShippingAddress();
        $this->invoiceAddress = $quote->getBillingAddress();

        return $this;
    }

    public function validate()
    {
        $info = $this->getInfoInstance();
        $shippingAddress = $this->shippingAddress;
        $invoiceAddress = $this->invoiceAddress;
        $ssn = str_replace('-', '', $info->getSsn());
        $cell = str_replace('-', '', $info->getCellphone());

        if (!preg_match("/^\d{10}$/", $ssn)) {
            throw new Mage_Payment_Model_Info_Exception(wordwrap('Felaktigt personnummer', 80));
        }

        if (!preg_match("/^\+{0,1}[1-9]{0,1}[0-9]{7,12}$/", $cell)) {
            throw new Mage_Payment_Model_Info_Exception(wordwrap('Felaktigt mobilnummer', 80));
        }

        if (isset($invoiceAddress)) {

            $companyName = $invoiceAddress->getCompany();

            if (strlen($companyName) > 0) {
                $customerType = 'LEGAL';
            } else {
                $customerType = 'NATURAL';
            }

            $result = $this->getAddresses($ssn, $customerType);

            if ($result['Error'] === 'true') {
                error_log('FEL!!');
                throw new Mage_Payment_Model_Info_Exception($result['Data']);
            }
            else {
                $address = $result['Data'];

                error_log($address->firstName);

                $shippingAddress->setFirstname($address->firstName);
                $shippingAddress->setLastname($address->lastName);
                $shippingAddress->setStreet($address->addressRow1);
                $shippingAddress->setPostcode($address->postalCode);
                $shippingAddress->setCity($address->postalArea);

                $invoiceAddress->setFirstname($address->firstName);
                $invoiceAddress->setLastname($address->lastName);
                $invoiceAddress->setStreet($address->addressRow1);
                $invoiceAddress->setPostcode($address->postalCode);
                $invoiceAddress->setCity($address->postalArea);

                if($result['CustomerType'] == 'LEGAL') {
                    $shippingAddress->setCompany($address->fullName);
                    $invoiceAddress->setCompany($address->fullName);
                }
            }
        }

        return $this;
    }


	public function getBundleProductTaxPercent($item, $discount)
	{
		$taxAmount = $item->getTaxAmount();
        $itemPrice = $item->getPrice();
        $itemTaxPrice = $item->getPriceInclTax();
        if (floatval($itemPrice) != floatval($itemTaxPrice) && floatval($itemTaxPrice) > 0)
        {
            $testAmount = $item->getPriceInclTax() - $item->getPrice();
            $taxPercent = ($testAmount / $itemPrice)*100;
            return $taxPercent;
        }

        // FailoverMethod
        if ($item->getPriceInclTax() > 0 && $item->getPrice() > 0 && $item->getPriceInclTax() != $item->getPrice() && is_numeric($discount) && $discount != 0)
        {
            $discountTestAmount = $item->getPriceInclTax() - $item->getPrice();
            // If those two values differs this may have been caused by a discount (at least this is what we found out during tests).
            // In other cases, the tax will be calculated based on the discount, which makes the tax lower than it should be.
            if ($discountTestAmount != $taxAmount) {$taxAmount = $discountTestAmount;}
        }
        $itemPrice = $item->getPrice();
        $quantity = (int) ($item->getQtyOrdered() ? $item->getQtyOrdered() : $item->getQty());
        $taxPercent = (($taxAmount / $quantity) / $itemPrice ) * 100;
        return $taxPercent = Mage::getSingleton('tax/calculation')->round($taxPercent);
	}

    public function canFinalize($order, $virtualsOnly = false)
    {
        $getOrderInfo = $this->getOrderInfo($order, $virtualsOnly);
        return ($getOrderInfo['canFinalize'] === true ? true :false);
    }
    public function getOrderInfo($order, $virtualsOnly = false)
    {
        $returnArray = array();
        try {
            $noShipping = 0;
            $isShipping = 0;
            $canFinalize = false;
            $mCart = $order->getAllItems();
            $productTypes = array();
            if (count($mCart) > 0) {
                foreach ($mCart as $i) {
                    $productType = $i->getProductType();
                    if ($i->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL || $productType == "downloadable") {$noShipping++;} else {$isShipping++;}
                    if (!isset($productTypes[$productType])) {$productTypes[$productType] = 0;}
                    $productTypes[$productType]++;
                }
            }
            //$orderItems = $order->getItemsCollection();
            //$allOrderItemIds = $orderItems->getAllIds();
            $shipments = $order->getShipmentsCollection();
            $shippedItemIds = array();
            $shippedItems = array();
            foreach ($shipments as $shipment) {
                $shippedItems = $shipment->getItemsCollection();
                foreach ($shippedItems as $item) {
                    $shippedItemIds[] = $item->getOrderItemId();
                }
            }
            if (!$virtualsOnly) {
                if ($isShipping === 0 && $noShipping > 0) {
                    $canFinalize = true;
                } // If item count that is shipped is none, but there are a count of nonShipping over 0 (meaning this is only virtuals), finalize may occur
                elseif ($isShipping > 0 && $noShipping === 0) {
                    $canFinalize = true;
                } // If there are only items that will ship, finalize may occur
                elseif (count($shippedItemIds) >= $isShipping) {
                    $canFinalize = true;
                } // If shipped item count is over the count of items than can be shipped, finalize may occur
            } else {
                if ($isShipping === 0 && $noShipping > 0) {
                    $canFinalize = true;
                }
            }
            $returnArray = array(
                'types' => $productTypes,
                'hasNoShipping' => ($isShipping == 0 ? true : false),
                'hasMixed' => ($isShipping > 0 && $noShipping > 0 ? true : false),
                'shippingCount' => $isShipping,
                'noshippingCount' => $noShipping,
                'shippedCount' => count($shippedItemIds),
                'total' => $isShipping + $noShipping,
                'canFinalize' => $canFinalize
            );
        }
        catch (Exception $e)
        {
        }
        return $returnArray;
    }

    // Used to get payment data from Resurs Bank
    public function getPaymentSpecs($order, $shippingAndDiscounts = true)
    {
        // This does not give us correct info
//        $quoteListArray = $order->getAllVisibleItems();
//        $orderItems = $order->getItemsCollection()
//            ->addAttributeToSelect('*')
//            ->addAttributeToFilter('product_type', array('eq'=>'simple'))
//            ->load();
		$returnSpec = array();
        $paymentId = $order->getPayment()->getAdditionalInformation("payment_id");
        $paymentArray = $this->getPayment($paymentId);

        $specLines = null;
        $authLines = null;
        $debitLines = null;
        if (isset($paymentArray->paymentDiffs->paymentSpec->specLines))
        {
            $specLines = $paymentArray->paymentDiffs->paymentSpec->specLines;
			$newspec = array();
			if (!is_array($specLines))
			{
				$newspec[] = $specLines;
				$specLines = $newspec;
			}
		}
        else
        {
            // IF the paymentarray does not have speclines, something else has been done with this payment
            if (isset($paymentArray->paymentDiffs)) {
                foreach ($paymentArray->paymentDiffs as $specsObject)
                {
                    // Catch authorize, and debit speclines
                    if ($specsObject->type == "AUTHORIZE")
                    {
                        if (isset($specsObject->paymentSpec->specLines))
                        {
                            $authLines = $specsObject->paymentSpec->specLines;
							$newspec = array();
							if (!is_array($authLines))
							{
								$newspec[] = $authLines;
								$debitLines = $newspec;
							}
                        }
                    }
                    if ($specsObject->type == "DEBIT")
                    {
                        if (isset($specsObject->paymentSpec->specLines))
                        {
                            $debitLines = $specsObject->paymentSpec->specLines;
							$newspec = array();
							if (!is_array($debitLines))
							{
								$newspec[] = $debitLines;
								$debitLines = $newspec;
							}
                        }
                    }
                }
            }
        }

        // Rules for credit/annulment - for credit, speclines from debit should be used.
        if (count($debitLines) && $this->isCreditPayment === true) {$specLines = $debitLines;}
        if (count($debitLines) && $this->isAnnulPayment === true) {$specLines = $authLines;}

		$returnSpec = array();
        if (is_object($paymentArray) && !is_null($specLines))
        {
            foreach ($specLines as $specObject)
            {
				$price = $specObject->unitAmountWithoutVat * floatval("1." . $specObject->vatPct);
				if (!$specObject->vatPct)
				{
					if ($specObject->quantity == 1)
					{
						$price = ($specObject->unitAmountWithoutVat + $specObject->totalVatAmount);
					}
					else
					{
						$price = $specObject->unitAmountWithoutVat;
					}
				}
				$setArray = array(
                   'code' => $specObject->artNo,
                   'name' => $specObject->description,
                   'price' => $price,
                   'vat' => $specObject->vatPct,
                   'amount' => $specObject->quantity
                );
                //echo "<pre>" . print_r($setArray, true) . "</pre>";
                //foreach ($specObject as $specKey => $specVal) {$setArray[$specKey] = $specVal;}
                $returnSpec[] = $setArray;
            }
		}
        return $returnSpec;
    }


    /// Resurs Bank Rewrite 141125
	public function getGoodsList($order, $shippingAndDiscounts = true)
    {
        $curSpeclineMethod = Mage::getStoreConfig('payment/resursapi/speclinemethod');
        $speclineMethod = ($curSpeclineMethod != "" ? $curSpeclineMethod : "0");
        Mage::log("Chosen specLine method: $speclineMethod", null, 'resurs.log', true);

        $hasBundles = false;

        $this->applyafter = Mage::helper('tax')->applyTaxAfterDiscount($order->getStoreId());
		$this->applydiscounttax = Mage::helper('tax')->discountTax();
        $taxprice = 0;
        $tax_percent = null;
        $bundleTaxPercent = null;
        $usetax = null;
        $testtax = null;
        $shipmentCount = 0;


        // Add the totals discount
        $discount = Mage::helper('resursbank')->getQuoteDiscountAmount();

        /* Specline Method - Regular order, no bundle digging */
        $itemArray = array();
        // Using main method to get a complete taxprice
        //if ($speclineMethod == 0 || !$speclineMethod) {
        $mCart = $order->getAllVisibleItems();
        $simpleItemArray = array();

		
		if ($this->isFinalizePayment) {
            /*
             * Future notes
             *
             * If we are finalizing in advanced mode, we need to reload the order to get items back, since the default mode for goodslisting
             * is to get customer session data. When reading from sessions we get the current options of the order, where all prices and rules
             * can be found. On Finalizing, the session is lost and with the session, also all options. To add a finalizer this way, you need
             * to add another checker in the loop below after the isset($options) as an else, or you will not be able to add a specline, since the
             * options list is empty.
             *
             * 1.3.6 will generate an error instead.
             *
             */
            $paymentId = $order->getPayment()->getAdditionalInformation("payment_id");
            /*
            $speclineError = "Finalizepayment ($paymentId): " . Mage::helper("resursbank")->__("Resurs Bank does not support debit/finalize in advanced specline mode");
            Mage::log($speclineError, null, 'resurs.log', true);
            Mage::getSingleton('core/session')->addError($speclineError);
            */
            $order->loadByIncrementId($order->getId());
            $quoteListNewArray = $this->getPaymentSpecs($order);
            if (is_array($quoteListNewArray)) {
                return $quoteListNewArray;
            } else {
                $speclineError = "Finalizepayment ($paymentId): " . Mage::helper("resursbank")->__("No speclines rendered");
                Mage::log($speclineError, null, 'resurs.log', true);
            }
            exit;
        }
		
		
		
            $articleSumPrice = 0;   // Debugging
            if (count($mCart) > 0)
            {
                foreach ($mCart as $i)
                {
                    // $options for future use only, partially contains information about bundled productprices
                    //Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL
                    $options = $i->getProduct()->getTypeInstance(true)->getOrderOptions($i->getProduct());
                    if (($i->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE && $i->isChildrenCalculated())) {

                        /*
                         * Use this part to get speclines based on content (Resurs invoices does not support this, but we'll leave it here)
                            $totprice = 0;
                            foreach($i->getChildren() as $c) {}
                         *
                         */
                        $hasBundles = true;
                        $articleSumPrice += $i->getPrice(); // DebugOnly
                        $tax_percent = $this->getBundleProductTaxPercent($i, $discount);   // Get correct tax rate
                    } else {
                        $articleSumPrice += $i->getPrice(); // DebugOnly
                        $tax_percent = $i->getTaxPercent(); // Get correct tax rate

                    }
                    if ($tax_percent > 0 && !$usetax) {$usetax = $tax_percent;}
                    // Set a price
                    $item_price = ($i->getPriceInclTax() ? $i->getPriceInclTax() : ($i->getPrice() / 100 * (100 + $tax_percent)));
                    $taxprice += $item_price;
					$usedQuantity = $i->getQty();
					if ($this->isFinalizePayment)
					{
						try
						{
							if ($i->getData('qty_ordered') > 0) {$usedQuantity =  $i->getData('qty_ordered');}
						}
						catch (Exception $e) {}
					}
                    $itemArray[] = array(
                        'code' => $i->getSku(),
                        'name' => $i->getName(),
                        'price' => $item_price,
                        'vat' => $tax_percent,
                        'amount' => $usedQuantity
                    );                }
                $simpleItemArray = $itemArray;
            }
        //}
        /* Specline Method - Older method, bundle digging */
        if ($speclineMethod == "1")
        {
            // Fetch user session so we can walk through the current active quote also
            $session = Mage::getSingleton('checkout/session');

            $itemArray = array();
            $tax_percent = null;
            //$items = $order->getAllVisibleItems(); // The mcart
            $noTaxPrice = array();
            $quoteList = $session->getQuote()->getAllVisibleItems();

            foreach ($quoteList as $item)
            {
                $options = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct());
                $id = $item->getProductId();
                if ($item->getTaxPercent()) {$tax_percent = $item->getTaxPercent();}
                $item_price = ($item->getPriceInclTax() ? $item->getPriceInclTax() : ($item->getPrice() / 100 * (100 + $tax_percent)));
                $noTaxPrice = $item->getPrice();
                if (($item->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE && $item->isChildrenCalculated()))
                {
                    if (!floatval($item->getTaxPercent()))
                    {
                        //$tax_percent = $this->getBundleProductTaxPercent($item, $discount);   // Get correct tax rate
                        if (!floatval($tax_percent)) {$tax_percent= $usetax;}
                    }
                }
                $item_quantity = (int) ($item->getQtyOrdered() ? $item->getQtyOrdered() : $item->getQty());
                if (isset($options))
                {
                    if (isset($options['info_buyRequest']))
                    {
                        // isset($options['bundle_options'])
                        if (isset($options['info_buyRequest']['bundle_option']) && is_array($options['info_buyRequest']['bundle_option']))
                        {
                            $product = Mage::getModel('catalog/product')->load($id);
                            $selections = $product->getTypeInstance(true)->getSelectionsCollection($product->getTypeInstance(true)->getOptionsIds($product), $product);
                            $selArray = array();
                            foreach ($selections as $selection) {$selArray[$selection->getOptionId()][$selection->selection_id] = $selection->getSku();}
                            //echo "<pre>" . print_r($selArray, true) . "</pre>";
                            // Products chosen in this option
                            //$infoBuyRequest = $item->getOptionByCode('info_buyRequest');
                            //$buyRequest = new Varien_Object(unserialize($infoBuyRequest->getValue()));

                            /*
                             * Read infoBuyRequest-options, and get customer options. In the infoBuyRequest we may get information about special options and SKU's
                             * Together with $options[bundle_options] if this part exist, we will get the rest of the options where we also get
                             * prices (special prices, product titles and quantity).
                             *
                             * The $selArray that is collected above has information about ALL products in a bundle, even if it's outside customer's choice),
                             * so when reading the $options[bundle_options] we can't just read it as is, since there may products that is not selected.
                             *
                             */
                            $bundlePrice = null;
                            $bundleNoTax = 0;
                            $tmpItems = array();
                            foreach ($options['info_buyRequest']['bundle_option'] as $optionId => $optionArray)
                            {
                                $optionPosition = 0;
                                if (isset($optionArray) && is_array($optionArray))
                                {
                                    //$attributeDetails = Mage::getSingleton("eav/config")->getAttribute("catalog_product", $optionId);
                                    //$productModel = Mage::getModel('catalog/product')->getResource()->getAttribute($optionId);
                                    $optionValues = $options['bundle_options'][$optionId]['value'];
                                    $bundlePrice = 0;
                                    foreach ($optionArray as $curOption)
                                    {
                                        $sku = $selArray[$optionId][$curOption];
                                        $skuPrice = $optionValues[$optionPosition]['price'] / 100 * (100 + $tax_percent);
                                        $bundlePrice += $skuPrice;
                                        $bundleNoTax += $optionValues[$optionPosition]['price'];
                                        $skuQty = $optionValues[$optionPosition]['qty']*$item_quantity;
                                        $skuTitle = $optionValues[$optionPosition]['title'];
                                        $optionPosition ++;

                                        $tmpItems[] = array(
                                            'code' => $sku,
                                            'name' => $skuTitle,
                                            'price' => $skuPrice,
                                            'vat' => $tax_percent,
                                            'amount' => $skuQty
                                        );

                                        //echo "$optionId = OptionsID == $curOption: $sku - $skuPrice:-<br>\n";
                                    }
                                }
                                else
                                {
                                    if ($optionArray > 0)
                                    {
                                        $optionValues = $options['bundle_options'][$optionId]['value'];
                                        $sku = $selArray[$optionId][$optionArray];
                                        $skuPrice = $optionValues[$optionPosition]['price'] / 100 * (100 + $tax_percent);
                                        $bundlePrice += $skuPrice;
                                        $bundleNoTax += $optionValues[$optionPosition]['price'];
                                        $skuQty = $optionValues[$optionPosition]['qty'] * $item_quantity;
                                        $skuTitle = $optionValues[$optionPosition]['title'];
                                        $tmpItems[] = array(
                                            'code' => $sku,
                                            'name' => $skuTitle,
                                            'price' => $skuPrice,
                                            'vat' => $tax_percent,
                                            'amount' => $skuQty
                                        );
                                        //echo "$optionId = OptionsID == $optionArray: $sku - $skuPrice:-<br>\n";
                                    }
                                }
                            }

                            /*
                             * Look for differences between our calculated item price and our summed bundle price.
                             * If there is a diff here, we may want to be aware of some extra pricing somewhere.
                             */
                            if (!is_null($bundlePrice) && floatval($bundlePrice) != floatval($item_price))
                            {
                                $priceDiff = $noTaxPrice - $bundleNoTax;
                                /*
                                 * If there is a price of 0 in the bundle calculation, there should be a fallback to the primary article.
                                 * In that case, we will clear the array (tmpItems) since we don't want the paymentspec to contain any 0-price-products
                                 *
                                 * If there is a price-mix (combo with products with and without price), we can't do anything about it.
                                 */
                                if ($bundleNoTax == 0) {$tmpItems = array();}
                                $quantity = (int) ($item->getQtyOrdered() ? $item->getQtyOrdered() : $item->getQty());
                                $itemArray[] = array(
                                    'code' => $item->getSku(),
                                    'name' => $item->getName(),
                                    'price' => $priceDiff / 100 * (100 + $tax_percent),
                                    'vat' => $tax_percent,
                                    'amount' => $quantity
                                );

                            }
                            // After validation
                            if (is_array($tmpItems))
                            {
                                foreach ($tmpItems as $iTM => $iArr) {$itemArray[] = $iArr;}
                            }
                        }
                        else
                        {
                            $quantity = (int) ($item->getQtyOrdered() ? $item->getQtyOrdered() : $item->getQty());
                            $itemArray[] = array(
                                'code' => $item->getSku(),
                                'name' => $item->getName(),
                                'price' => $item_price,
                                'vat' => $tax_percent,
                                'amount' => $quantity
                            );
                        }
                    }
                    if (isset($options['options']))
                    {
                    }
                    if (isset($options['additional_options']))
                    {
                    }
                }
            }
            //echo "<pre>" . print_r($itemArray, true) . "</pre>";
        }

        //if ($this->isFinalizePayment && count($simpleItemArray) > 0 && !count($itemArray)) {$itemArray = $simpleItemArray;}
        Mage::log(print_r($itemArray, true), null, 'resurs.log', true);
		if ($shippingAndDiscounts)
		{
			$shipping_amount = Mage::helper('resursbank')->getQuoteShippingInclTax();
            if ($this->isFinalizePayment)
            {
                $shipping_amount = $order->getShippingAmount();
            }

			if ($shipping_amount > 0)
			{
                $shippingfeedesc = Mage::helper("resursbank")->__("Shipping fee");
				$itemArray[] = array(
					'code' => 'shippingfee',
					'name' => $shippingfeedesc ? $shippingfeedesc : "shippingfee",
					'price' => $shipping_amount,
					'vat' => Mage::helper('resursbank')->getShippingTaxRate(),
					'amount' => 1
				);
			}
			$giftCardsAmount = $order->getGiftCardsAmount();
			if ($giftCardsAmount > 0)
			{
                $giftcardDescription = Mage::helper("resursbank")->__("Giftcard") ? Mage::helper("resursbank")->__("Giftcard") : "giftcard";
				$itemArray[] = array(
					'code' => 'giftcard',
					'name' => $giftcardDescription,
					'price' => ($giftCardsAmount * -1),
					'vat' => 0,
					'amount' => 1
				);
			}
			$customerBalance = $order->getCustomerBalanceAmount();
			if ($customerBalance > 0)
			{
                $storecreditDescription = Mage::helper("resursbank")->__("Store credit") ? Mage::helper("resursbank")->__("Store credit") : "storecredit";
				$itemArray[] = array(
				'code' => 'storecredit',
				'name' => $storecreditDescription,
				'price' => ($customerBalance * -1),
				'vat' => 0,
				'amount' => 1
				);
			}
			$rewardCurrency = $order->getRewardCurrencyAmount();
			if ($rewardCurrency > 0)
			{
                $rewardpointsDescription = Mage::helper("resursbank")->__("Reward points") ? Mage::helper("resursbank")->__("Reward points") : "rewardpoints";
				$itemArray[] = array(
				'code' => 'rewardpoints',
				'name' => $rewardpointsDescription,
				'price' => ($rewardCurrency * -1),
				'vat' => 0,
				'amount' => 1
				);
			}

			if ($discount != 0)
			{
                $amount = 0;
                $discountDescription = Mage::helper("resursbank")->__("Discount") ? Mage::helper("resursbank")->__("Discount") :"discount";
                if ($this->applyafter === true)
                {
                    $taxPriceEx = $taxprice * 1/ floatval("1." . $usetax);		// The current total amount without tax, automated "reverse"-method for dynamic taxes
                    $priceDiscount = ($taxPriceEx + $discount);					// Calculate non-tax-totalamount with discount included
                    $priceTaxDiscount = $taxprice + $discount;					// Calculate tax-totalamount with discount included
                    $priceDiscountTax = $priceDiscount + ($priceDiscount * ($tax_percent / 100));	// Calculate new totalamount with discount and tax
                    $priceDiff = $priceTaxDiscount - $priceDiscountTax;				// Get the difference between the new two calculations
                    $currentdiscount = $discount;
                    $newdiscount = $discount - $priceDiff;						// Add the difference to new discount calculated on the afterdiscount setting)
                    $discount = $newdiscount;							// Change current discount
                    $itemArray[] = array(
                        'code' => 'discount',
                        'name' => $discountDescription,
                        'price' => round(($this->applydiscounttax ? $discount : $currentdiscount), 2),
                        'vat' => ($this->applydiscounttax ? $usetax : 0),
                        'amount' => 1
                    );
                }
                else
                {
                    $itemArray[] = array(
                        'code' => 'discount',
                        'name' => $discountDescription,
                        'price' => round($discount,2), //$discount * round(1/ floatval("1." . $usetax), 2),
                        'vat' => $usetax,
                        'amount' => 1
                    );
                }
			}
		}
        $logString = '';
		$methodCode = $order->getPayment()->getMethod();
		$paymentFee = $order->getFeeAmount();
		if ($this->isFinalizePayment)
        {
            $feeTaxClassRate = Mage::helper('resursbank')->getFeeTaxClassRate($order);
        }
        else{
            $feeTaxClassRate = Mage::helper('resursbank')->getFeeTaxClassRate();
        }
		if(!is_numeric($feeTaxClassRate))
		{
			$feeTaxClassRate = 0;
		}
		else if($feeTaxClassRate > 0)
		{
			$calc = Mage::getSingleton('tax/calculation');
			$tax = Mage::helper('resursbank')->_round($calc->calcTaxAmount($paymentFee, $feeTaxClassRate, false, false), $feeTaxClassRate, true);
			$paymentFee += $tax;
		}
		if ($paymentFee > 0)
		{
            $feedesc = Mage::getStoreConfig("payment/$methodCode/payment_fee_descrption");
            $itemArray[] = array(
                'code' => 'paymentfee',
                'name' => $feedesc ? $feedesc : "paymentfee",
                'price' => $paymentFee,
                'vat' => $feeTaxClassRate,
                'amount' => 1
            );
		}
		return $itemArray;
	}

    public function setDeliveryAddress($paymentSessionId,$address)
    {
        Mage::log(Mage::helper('resursbank')->__('Set Delivery Address: '), null, 'resurs.log', true);
        Mage::log($address, null, 'resurs.log', true);
        
        try {
            $this->initWebservice();
            $result = $this->shopFlowService->setDeliveryAddress(
                array('paymentSessionId' => $paymentSessionId,'address' => $address)
            );
            return is_object($result);
        }
        catch(Exception $e) {
            Mage::log(Mage::helper('resursbank')->__('Set Delivery Address: '), null, 'resurs.log', true);
            Mage::log($e->getMessage(), null, 'resurs.log', true);
            return Mage::helper('resursbank')->getCustomerMessage($e);
        }
    }
    
    public function getLimitApplicationTemplate($method,$amount)
    {
        try {
            $this->initWebservice();
            $result = $this->developerWebService->getLimitApplicationTemplate(
                        array('paymentMethodId' => str_replace($this->username, '', $method),
                               'sum' => $amount)
                      );
            return $result;
        }
        catch(Exception $e) {
            Mage::log(Mage::helper('resursbank')->__('Get Limit Application Templates: '), null, 'resurs.log', true);
            Mage::log($e->getMessage(), null, 'resurs.log', true);
            return Mage::helper('resursbank')->getCustomerMessage($e);
        }
    }
    
    private function _getShippingTaxRate()
    {
        $customer = Mage::getModel('customer/customer')->load($this->_order->getCustomerId());
        $taxClass = Mage::getStoreConfig('tax/classes/shipping_tax_class', $this->_order->getStoreId());
        $calculation = Mage::getSingleton('tax/calculation');
        $request = $calculation->getRateRequest(
            $this->_order->getShippingAddress(),
            $this->_order->getBillingAddress(),
            $customer->getTaxClassId(),
            Mage::app()->getStore($this->_order->getStoreId())
        );
        return $calculation->getRate($request->setProductClassId($taxClass));
    }

    public function getCustomOrderState($statusCode)
    {
        $label = $statusCode;
        $statuses = Mage::getModel('sales/order_status')
                    ->getCollection()
                    ->addFieldToFilter('status',$statusCode);
        foreach($statuses as $status)
        {
            $label = $status->getStoreLabel(Mage::app()->getStore());
        }
        
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $store_id = Mage::app()->getStore()->getStoreId();
        $status_code = 'custom_order_new';
        
        $query = 'SELECT state FROM ' . $resource->getTableName('sales_order_status_state'). ' WHERE status="'.$statusCode.'"';
        $results = $readConnection->fetchAll($query);
        $state = (isset($results[0]['state']) ? $results[0]['state'] : "");
                            
        return array('state' => $state, 'label' => $label);
    }
    protected function orderStatus($orderStatus)
    {
        switch($orderStatus)
        {
            case 'pending':     return Mage_Sales_Model_Order::STATE_NEW;
            case 'processing':  return Mage_Sales_Model_Order::STATE_PROCESSING; 
            case 'complete':    return Mage_Sales_Model_Order::STATE_PROCESSING;
            case 'closed':      return Mage_Sales_Model_Order::STATE_CLOSED; 
            case 'canceled':    return Mage_Sales_Model_Order::STATE_CANCELED;   
            case 'holded':      return Mage_Sales_Model_Order::STATE_HOLDED;
        }
    }

	
}
