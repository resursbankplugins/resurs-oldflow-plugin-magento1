<?php
class Resursbank_Resursbank_Model_Country
{
  public function toOptionArray()
  {
    return array(
      array('value' => '', 'label' => Mage::helper('resursbank')->__('Please choose country')),
      array('value' => 'DK', 'label' => Mage::helper('resursbank')->__('Denmark')),	
      array('value' => 'FI', 'label' => Mage::helper('resursbank')->__('Finland')),
      array('value' => 'NO', 'label' => Mage::helper('resursbank')->__('Norway')),
      array('value' => 'SE', 'label' => Mage::helper('resursbank')->__('Sweden')),
    );
  }
}