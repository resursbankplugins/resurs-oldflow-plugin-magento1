<?php

class Resursbank_Resursbank_Model_Cron 
{
    public function getPaymentMethods($username='', $password='', $scope = 0)
    {

        $codePath = Mage::getBaseDir('code');
        $extensionPath = $codePath.DS.'local'.DS.'Resursbank'.DS.'Resursbank';
        $systemXmlAbstract = @file_get_contents($extensionPath.DS.'etc'.DS.'system-abstract.xml');
        $systemResursDynamicPaymentMethodsXmlAbstract = @file_get_contents($extensionPath.DS.'etc'.DS.'system-resursbank-dynamic-payment-methods-abstract.xml');
        
        if(empty($username))
        {
            $username = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_id', $scope);
            $password = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_password', $scope);
        }
		if ($scope > 0)
		{
			// If the request comes from another scope, check username and password if it matches for that scope
			$username_scope = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_id', $scope);
			$password_scope = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_password', $scope);
			if ($username != $username_scope)
			{
				$username = $username_scope;
				$password = $password_scope;
			}
        }
        
        $apiModel = Mage::getModel('resursbank/api');
        $apiMethods = $apiModel->getPaymentMethods($username,$password, $scope);
        
        if(!empty($apiMethods['error']))
        {
            echo $apiMethods['error'];
            return;
        }
        
        $paymentMethods = $apiMethods['methods'];
        
        $methodsArray = array();		// Current active methods for current username, full array with methods configuration
        $methodsIdsArray = array();	// Current active methods for current username, listing only received names - this is what we get during soap
        $paymentMethod = isset($paymentMethods->return) ? $paymentMethods->return : null;
		
		$scopeArray = array();
		$paymentMethodArrayType = gettype($paymentMethod);
		if ($paymentMethodArrayType == "object" && count($paymentMethods->return) == 1)
		{
				$currentMethodId = $username . $paymentMethod->id;
				//$currentMethodId = $paymentMethod->id;		// OLD
				$scopeArray[$currentMethodId] = $scope;
				$methodsArray[] = array(
					'payment_id' => $currentMethodId,
					'description' => $paymentMethod->description,
					'min_limit' => $paymentMethod->minLimit,
					'max_limit' => $paymentMethod->maxLimit,
					'payment_type' => $paymentMethod->type,
					'user_id' => $username
				);
				$methodsIdsArray[] = $currentMethodId;
		}
		else
		{
			if(is_array($paymentMethod))
			{
				foreach ($paymentMethods->return as $paymentMethod)
				{
					$currentMethodId = $username . $paymentMethod->id;
					//$currentMethodId = $paymentMethod->id;		// OLD
					$scopeArray[$currentMethodId] = $scope;
					$methodsArray[] = array(
						'payment_id' => $currentMethodId,
						'description' => $paymentMethod->description,
						'min_limit' => $paymentMethod->minLimit,
						'max_limit' => $paymentMethod->maxLimit,
						'payment_type' => $paymentMethod->type,
						'user_id' => $username
					);
					$methodsIdsArray[] = $currentMethodId;
				}
			}
		}
        if (count($methodsArray)) {
            if (isset($_SERVER['RESURS_CLEAN_METHODS_CACHE']) && $_SERVER['RESURS_CLEAN_METHODS_CACHE'] == "true") {
                // Clean up on demand, and silently leave errors alone
                try {
                    $writer = Mage::getSingleton('core/resource')->getConnection('core_write');
                    // Using extended cleanup, will clean up everything in all stores if site is multistore
                    if (isset($_SERVER['RESURS_CLEAN_METHODS_CACHE_EXTEND']) && $_SERVER['RESURS_CLEAN_METHODS_CACHE_EXTEND'] == "true") {
                        $writer->query("DELETE FROM resursbank_payment_methods");
                    } else {
                        $writer->query("DELETE FROM resursbank_payment_methods WHERE user_id = '" . $username . "'");
                    }
                } catch (Exception $cleanupMethodsException) {}
            }
        }

        foreach($methodsArray as $methodData)
        {
            $methodCollection = Mage::getModel('resursbank/methods')->getCollection()->addFieldToFilter('payment_id',$methodData['payment_id']);
            $collectionAvailable = false;
            foreach($methodCollection as $method)
            {
                $collectionAvailable = true;
                if($method->getId())
                {
                    $method->addData($methodData);
                    $method->save();
                }
            }
            if(!$collectionAvailable)
            {
                $methodModel = Mage::getModel('resursbank/methods');
                $methodModel->setData($methodData);
                $methodModel->save();
            }
        }
        
        $systemXml = '';
        $systemMethodsXml = '';
                     
		// Get the rest
        $methodCollection = Mage::getModel('resursbank/methods')->getCollection();
        $index = 4;

		$websites=Mage::app()->getWebsites();
		$avail_scopes = array('0');
        foreach ($websites as $website)
		{
            $code = $website->getCode();
			$codescope = $website->getId();
			$avail_scopes[] = $codescope;
        }
		$paymentid_websites = array();
		// Saving some time by scanning through methods and scopes first, to collect payments and where they are active/stored
        foreach($methodCollection as $method)
        {
			// Work with the current
			$paymentid = $method->getPaymentId();
			foreach ($avail_scopes as $scopeid)
			{
				$currentRepresentative = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_id', $scopeid);
                if (!preg_match("/$currentRepresentative/", $paymentid)) {
                    $thePaymentId = $currentRepresentative . $paymentid;
                }
                else{
                    $thePaymentId = $paymentid;
                }
				//$thePaymentId = $paymentid;	// Old

                $ac = Mage::getStoreConfig('payment/resurspayment' . $thePaymentId . '/active', $scopeid);
                //echo "T: $thePaymentId / $scopeid = ". ($ac ? "Active" : "Not active") ."\n";

				if ($thePaymentId != "" && Mage::getStoreConfig('payment/resurspayment' . $thePaymentId . '/active', $scopeid))
				{
					$paymentid_websites[$thePaymentId][$scopeid] = true;
					if (!in_array($thePaymentId, $methodsIdsArray))
					{
						$methodsIdsArray[] = $thePaymentId;
					}
				}
			}
		}

		// Think this over. And remove it.
		// $cur_ac_method = null;
		
		// Write everything
		$dupechecker = array();
        foreach($methodCollection as $method)
        {
			$paymentid = $method->getPaymentId();
			if ($method->getUserId()) {$mUser = $method->getUserId();} else {$mUser = "null";}
			//$currentRepresentative = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_id', $scope);
			$currentRepresentative = $mUser;

			$currentPaymentId = $currentRepresentative . str_replace($currentRepresentative, '', $paymentid);

            //echo $currentMethodId . " | $paymentid\n";
			//$currentPaymentId = $paymentid;		// Old
			if(!in_array($currentPaymentId, $methodsIdsArray))
            {
				// Saving all other payment methods
                $cur_ac_method = intval(Mage::getStoreConfig('payment/resurspayment'.$currentPaymentId .'/active', $scope));
                //echo "\t$cur_ac_method\n";
                $method->setStatus($cur_ac_method);
                $method->save();
				Mage::getModel('core/config')->saveConfig('payment/resurspayment'.$currentPaymentId .'/active', (isset($cur_ac_method) ? $cur_ac_method : ""), $scope);
            }
            else
            {
				// Saving methods that belongs to this user
				if (isset($dupechecker[$currentPaymentId])) {continue;}
				$dupechecker[$currentPaymentId] = true;
                $search = array(
                    '[[PAYMENT-ID]]',
                    '[[PAYMENT-DESCRIPTION]]',
					'[[PAYMENT-SORTORDER]]',
					'[[REPRESENTATIVE-ID]]'
                );
                $replace = array(
					$currentPaymentId,
                    $method->getDescription(),
					$index++,
					$mUser
                );
								
                $systemMethodsXml .= str_replace($search, $replace, $systemResursDynamicPaymentMethodsXmlAbstract);
                $systemXml = str_replace('[[RESURSBANK-DYNAMIC-PAYMENT-METHODS-PLACE-HOLDER]]', $systemMethodsXml, $systemXmlAbstract);
            }
        }

        $xmlPath = $extensionPath.DS.'etc';
        if(is_writable($xmlPath))
        {
            //@copy($xmlPath.DS.'system.xml', $xmlPath.DS.'Backup'.DS.'etc'.DS.'system.xml');
			$fromfile = $xmlPath.DS.'system.xml';
			$tofile = $xmlPath.DS.'Backup'.DS.'etc'.DS.'system.xml';
			@copy($fromfile, $tofile);
			$errors= error_get_last();
			if ($errors['type'] > 0)
			{
				Mage::log('Cron.php Backup Error: ' . $fromfile . ' => ' . $tofile . ' failed', null, 'resurs.log', true);
				Mage::log(print_r($errors, true), null, 'resurs.log', true);
			}
			else
			{
				Mage::log('Cron.php Backup OK: ' . $fromfile . ' => ' . $tofile, null, 'resurs.log', true);
			}
            @file_put_contents($xmlPath.DS.'system.xml', $systemXml);
        }
    }
}
