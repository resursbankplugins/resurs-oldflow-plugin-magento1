<?php
class Resursbank_Resursbank_Model_Fee extends Varien_Object
{
	public static function getFee()
	{
		$method = Mage::getSingleton('checkout/session')->getQuote()->getPayment()->getMethod();
		$amount = Mage::getStoreConfig("payment/$method/payment_fee");
		
		return $amount;
	}
	
	public static function canApply($address)
	{
		$method = Mage::getSingleton('checkout/session')->getQuote()->getPayment()->getMethod();
                if(stristr($method,'resurspayment'))
		{
			$amount = Mage::getStoreConfig("payment/$method/payment_fee");
			if($amount > 0)
			{
				return true;
			}
		}
		return false;
	}

}