<?php
class Resursbank_Resursbank_Model_Observer
{
	public function saveOrderAfterSubmit(Varien_Event_Observer $observer)
	{
		$payments = Mage::getSingleton('payment/config')->getActiveMethods();
		foreach ($payments as $paymentCode=>$paymentModel)
		{
			if(preg_match('/resurspayment/', $paymentCode))
			{
				$functionName = 'setSsnAddress'.$paymentCode;
				Mage::getSingleton('checkout/session')->$functionName('');		
			}
		}
		return $this;
	}
	public function invoiceSaveAfter(Varien_Event_Observer $observer)
	{
		$invoice = $observer->getEvent()->getInvoice();
		if ($invoice->getBaseFeeAmount()) {
			$order = $invoice->getOrder();
			$order->setFeeAmountInvoiced($order->getFeeAmountInvoiced() + $invoice->getFeeAmount());
			$order->setBaseFeeAmountInvoiced($order->getBaseFeeAmountInvoiced() + $invoice->getBaseFeeAmount());
		}
		if ($invoice->getTaxBaseFeeAmount()) {
			$order = $invoice->getOrder();
			$order->setTaxFeeAmountInvoiced($order->getTaxFeeAmountInvoiced() + $invoice->getTaxFeeAmount());
			$order->setTaxBaseFeeAmountInvoiced($order->getTaxBaseFeeAmountInvoiced() + $invoice->getTaxBaseFeeAmount());
		}
		return $this;
	}
	public function creditmemoSaveAfter(Varien_Event_Observer $observer)
	{
		/* @var $creditmemo Mage_Sales_Model_Order_Creditmemo */
		$creditmemo = $observer->getEvent()->getCreditmemo();
		if ($creditmemo->getFeeAmount()) {
			$order = $creditmemo->getOrder();
			$order->setFeeAmountRefunded($order->getFeeAmountRefunded() + $creditmemo->getFeeAmount());
			$order->setBaseFeeAmountRefunded($order->getBaseFeeAmountRefunded() + $creditmemo->getBaseFeeAmount());
		}
		if ($creditmemo->getTaxFeeAmount()) {
			$order = $creditmemo->getOrder();
			$order->setTaxFeeAmountRefunded($order->getTaxFeeAmountRefunded() + $creditmemo->getTaxFeeAmount());
			$order->setTaxBaseFeeAmountRefunded($order->getTaxBaseFeeAmountRefunded() + $creditmemo->getTaxBaseFeeAmount());
		}
		return $this;
	}

    public function salesOrderCancelPayment(Varien_Event_Observer $observer)
    {
		// Silently fail this function if we get errors where we do not expect errors.
		try {
			// Section 1: Get payment info
			$payment = $observer->getEvent()->getPayment();
			$order = $payment->getOrder();
			$paymentId = $order->getPayment()->getAdditionalInformation("payment_id");
			$storeID = $order->getStoreId();
			$result = null;
			$method = "";
			// If we passed through section 1, without any errors, next step is to get the payment method information
			try {
				$oPayment = $order->getPayment();
				$method = $oPayment->method;
			} catch (Exception $e) {
				Mage::log("salesOrderCancelPayment/getPaymentMethodException: " . $e->getMessage(), null, 'resurs.log', true);
			}

			// If the payment method is a part of resurs, we are initiating the cancellation try
			if (preg_match("/resurspayment/i", $method)) {
				/* Get info about current payment */
				$paymentInfo = Mage::getModel('resursbank/api')->getPayment($paymentId, $storeID);
				$paymentStatus = array();
				// debitable, creditable, is_debited, is_credited, is_annulled
				if (isset($paymentInfo->status)) {
					if (!is_array($paymentInfo->status)) { $paymentStatus[] = $paymentInfo->status; } else { $paymentStatus = $paymentInfo->status; }
					$paymentStatus = array_map("strtolower", $paymentStatus);
					$newPaymentStatus = array();
					foreach ($paymentStatus as $paymentStatusTitle) { $newPaymentStatus[$paymentStatusTitle] = true; }
					if (count($newPaymentStatus)) { $paymentStatus = $newPaymentStatus;	}
				}
				if (Mage::getModel('resursbank/api')->canFinalize($order)) { $paymentStatus['canFinalize'] = true; }
				$cancelMode = null;
				if (isset($paymentStatus['creditable']) && isset($paymentStatus['canFinalize'])) { $cancelMode = "credit"; }
				if (isset($paymentStatus['debitable']) && isset($paymentStatus['canFinalize'])) { $cancelMode = "annul"; }
				if (isset($paymentStatus['debitable']) && count($paymentStatus) == 1) { $cancelMode = "annul"; }
				// If nothing else than debitable is set, this order is not finished (shipped) and can therefore be annuled
				// The inbetweener, where both crediting and debiting is available
				if (isset($paymentStatus['debitable']) && isset($paymentStatus['creditable']) && isset($paymentStatus['canFinalize'])) {
					// If it's already debited then let's credit the order. Otherwise annul it. This used to happen with bundle-detailed orders.
					if (isset($paymentStatus['is_debited'])) { $cancelMode = "credit"; } else { $cancelMode = "annul"; }
				}
				if ($cancelMode == "credit") { $result = Mage::getModel('resursbank/api')->creditPayment($order); }
				if ($cancelMode == "annul") { $result = Mage::getModel('resursbank/api')->annulPayment($order); }
				return $result;
			}
		} catch (Exception $e) {
			Mage::log("salesOrderCancelPayment/Exception: " . $e->getMessage(), null, 'resurs.log', true);
		}
    }

    public function salesOrderFinalizePayment(Varien_Event_Observer $observer)
    {
        $shipment = $observer->getEvent()->getShipment();
        $orderInfo = $shipment->getOrder();
        $order = Mage::getModel('sales/order')->load($orderInfo->getId());
        $payment = $order->getPayment();
        if (Mage::getModel('resursbank/api')->canFinalize($order)) {Mage::getModel('resursbank/api')->finalizePayment($payment);}
    }
	
}
