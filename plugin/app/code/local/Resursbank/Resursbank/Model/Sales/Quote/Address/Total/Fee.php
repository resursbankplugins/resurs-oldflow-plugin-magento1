<?php
class Resursbank_Resursbank_Model_Sales_Quote_Address_Total_Fee extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
	protected $_code = 'fee';

	public function collect(Mage_Sales_Model_Quote_Address $address)
	{
		parent::collect($address);
		
		$this->_setAmount(0);
		$this->_setBaseAmount(0);

		$items = $this->_getAddressItems($address);
		if (!count($items)) {
			return $this;
		}

		$quote = $address->getQuote();

		if(Resursbank_Resursbank_Model_Fee::canApply($address))
		{
			$paymentFee = Resursbank_Resursbank_Model_Fee::getFee();
			
			$address->setFeeAmount($paymentFee);
			$address->setBaseFeeAmount($paymentFee);
			
			$quote->setFeeAmount($paymentFee);
			
			$address->setGrandTotal($address->getGrandTotal() + $address->getFeeAmount());
			$address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseFeeAmount());
		}
	}

	public function fetch(Mage_Sales_Model_Quote_Address $address)
	{
		$amt = $address->getFeeAmount();
		if($amt) {
			$address->addTotal(array(
				'code'=>$this->getCode(),
				'title'=> Mage::helper('resursbank')->__('Payment Fee'),
				'value'=> $amt,
				'style'=> 'padding: 3px -2px 0 15px;'
			));
		}
		return $this;
	}
}