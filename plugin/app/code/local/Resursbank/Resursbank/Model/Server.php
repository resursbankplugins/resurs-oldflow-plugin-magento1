<?php
class Resursbank_Resursbank_Model_Server
{
  public function toOptionArray()
  {
    return array(
      array('value' => '1', 'label' => Mage::helper('resursbank')->__('Test')),	
      array('value' => '0', 'label' =>Mage::helper('resursbank')->__('Live')),	  
    );
  }
}
