<?php
class Resursbank_Resursbank_Model_Tax_Sales_Total_Quote_Tax extends Mage_Tax_Model_Sales_Total_Quote_Tax
{
	public function collect(Mage_Sales_Model_Quote_Address $address)
	{
		/* Begin Mage_Sales_Model_Quote_Address_Total_Abstract - collect method */

		$this->_setAddress($address);

		$this->_setAmount(0);
		$this->_setBaseAmount(0);

		/* End Mage_Sales_Model_Quote_Address_Total_Abstract - collect method */

		/* Begin Mage_Tax_Model_Sales_Total_Quote_Tax - collect method */

		$this->_roundingDeltas = array();
		$this->_baseRoundingDeltas = array();
		$this->_hiddenTaxes = array();
		$address->setShippingTaxAmount(0);
		$address->setBaseShippingTaxAmount(0);

		$this->_store = $address->getQuote()->getStore();
		$customer = $address->getQuote()->getCustomer();
		if ($customer) {
			$this->_calculator->setCustomer($customer);
		}

		if (!$address->getAppliedTaxesReset()) {
			$address->setAppliedTaxes(array());
		}

		$items = $this->_getAddressItems($address);
		if (!count($items)) {
			return $this;
		}
		$request = $this->_calculator->getRateRequest(
			$address,
			$address->getQuote()->getBillingAddress(),
			$address->getQuote()->getCustomerTaxClassId(),
			$this->_store
		);

		if ($this->_config->priceIncludesTax($this->_store)) {
			$this->_areTaxRequestsSimilar = $this->_calculator->compareRequests(
				$this->_calculator->getRateOriginRequest($this->_store),
				$request
			);
		}

		switch ($this->_config->getAlgorithm($this->_store)) {
			case Mage_Tax_Model_Calculation::CALC_UNIT_BASE:
				$this->_unitBaseCalculation($address, $request);
				break;
			case Mage_Tax_Model_Calculation::CALC_ROW_BASE:
				$this->_rowBaseCalculation($address, $request);
				break;
			case Mage_Tax_Model_Calculation::CALC_TOTAL_BASE:
				$this->_totalBaseCalculation($address, $request);
				break;
			default:
				break;
		}

		$this->_addAmount($address->getExtraTaxAmount());
		$this->_addBaseAmount($address->getBaseExtraTaxAmount());
		$this->_calculateShippingTax($address, $request);

		$this->_processHiddenTaxes();

		/* End Mage_Tax_Model_Sales_Total_Quote_Tax - collect method */

		$calc = Mage::getSingleton('tax/calculation');
		$quote = Mage::getSingleton('checkout/cart')->getQuote();
		$address = $quote->getShippingAddress();

		$store = $quote->getStore();
		$storeTaxRequest = $calc->getRateOriginRequest($store);
		$addressTaxRequest = $calc->getRateRequest(
			$address,
			$address->getQuote()->getBillingAddress(),
			$address->getQuote()->getCustomerTaxClassId(),
			$store
		);

		$taxClassId = Mage::getStoreConfig("payment/common_settings/tax_class", $quote->getStoreId());

		$storeTaxRequest->setProductClassId($taxClassId);
		$addressTaxRequest->setProductClassId($taxClassId);
		$rate = $calc->getRate($addressTaxRequest);
		$paymentFee = Resursbank_Resursbank_Model_Fee::getFee();

		$tax = 0;
		if ($rate > 0) {
			$tax = self::_round($calc->calcTaxAmount($paymentFee, $rate, false, false), $rate, true);
		}

		$this->_addAmount($tax);
		$this->_addBaseAmount($tax);
		return $this;
	}
	
	protected function _saveAppliedTaxes(Mage_Sales_Model_Quote_Address $address, $applied, $amount, $baseAmount, $rate)
	{
		$previouslyAppliedTaxes = $address->getAppliedTaxes();
		$process = count($previouslyAppliedTaxes);

		foreach ($applied as $row) {
			if ($row['percent'] == 0) {
				continue;
			}

			if (!isset($previouslyAppliedTaxes[$row['id']])) {
				$row['process'] = $process;
				$row['amount'] = 0;
				$row['base_amount'] = 0;
				$previouslyAppliedTaxes[$row['id']] = $row;
			}

			if (!is_null($row['percent'])) {
				$row['percent'] = $row['percent'] ? $row['percent'] : 1;
				$rate = $rate ? $rate : 1;

				$appliedAmount = $amount / $rate * $row['percent'];
				$baseAppliedAmount = $baseAmount / $rate * $row['percent'];
			} else {
				$appliedAmount = 0;
				$baseAppliedAmount = 0;
				foreach ($row['rates'] as $rate) {
					$appliedAmount += $rate['amount'];
					$baseAppliedAmount += $rate['base_amount'];
				}
			}

			if ($appliedAmount || $previouslyAppliedTaxes[$row['id']]['amount']) {
				$previouslyAppliedTaxes[$row['id']]['amount'] += $appliedAmount;
				$previouslyAppliedTaxes[$row['id']]['base_amount'] += $baseAppliedAmount;
			} else {
				unset($previouslyAppliedTaxes[$row['id']]);
			}
		}


		/* Begin Payment fee tax rate apply */

		$calc = Mage::getSingleton('tax/calculation');
		$quote = Mage::getSingleton('checkout/cart')->getQuote();
		$address = $quote->getShippingAddress();

		$store = $quote->getStore();
		$storeTaxRequest = $calc->getRateOriginRequest($store);
		$addressTaxRequest = $calc->getRateRequest(
			$address,
			$address->getQuote()->getBillingAddress(),
			$address->getQuote()->getCustomerTaxClassId(),
			$store
		);

		$taxClassId = Mage::getStoreConfig("payment/common_settings/tax_class", $quote->getStoreId());

		$storeTaxRequest->setProductClassId($taxClassId);
		$addressTaxRequest->setProductClassId($taxClassId);

		$rate = $calc->getRate($addressTaxRequest);

		$paymentFee = Resursbank_Resursbank_Model_Fee::getFee();

		$tax = 0;
		if ($rate > 0) {
			$tax = self::_round($calc->calcTaxAmount($paymentFee, $rate, false, false), $rate, true);

			$paymentFeeTaxRateName = Mage::helper('resursbank')->__('Payment Fee Tax');
			$rates = array();
			$rates['code'] = $paymentFeeTaxRateName;
			$rates['title'] = $paymentFeeTaxRateName;
			$rates['percent'] = $rate;
			$rates['position'] = 2;
			$rates['priority'] = 3;
			$rates['rule_id'] = $taxClassId;

			$previouslyAppliedTaxes[$paymentFeeTaxRateName]['rates'][0] = $rates;
			$previouslyAppliedTaxes[$paymentFeeTaxRateName]['percent'] = $rate;
			$previouslyAppliedTaxes[$paymentFeeTaxRateName]['id'] = $paymentFeeTaxRateName;
			$previouslyAppliedTaxes[$paymentFeeTaxRateName]['process'] = 0;
			$previouslyAppliedTaxes[$paymentFeeTaxRateName]['amount'] = $tax;
			$previouslyAppliedTaxes[$paymentFeeTaxRateName]['base_amount'] = $tax;
		}
		/* End Payment fee tax rate apply */

		$address->setAppliedTaxes($previouslyAppliedTaxes);
	}
	
	public static function _round($price, $rate, $direction, $type = 'regular')
	{
	    if (!$price) {
		return Mage::getSingleton('tax/calculation')->round($price);
	    }
    
	    $deltas = Mage::getModel('sales/quote_address')->getRoundingDeltas();
	    $key = $type.$direction;
	    $rate = (string) $rate;
	    $delta = isset($deltas[$key][$rate]) ? $deltas[$key][$rate] : 0;
	    return Mage::getSingleton('tax/calculation')->round($price+$delta);
	}
	
}