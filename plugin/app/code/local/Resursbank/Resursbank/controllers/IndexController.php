<?php

class Resursbank_Resursbank_IndexController extends Mage_Core_Controller_Front_Action
{
	var $scope = 0;
	var $forcescope = null;

	public function indexAction()
	{
		;
	}

	public function throttle_payment_methodAction()
	{
		exit;
	}

	public function getJSPhraseAction()
	{
		$phraseName = $this->getRequest()->getParam('phrase');
		echo Mage::helper('resursbank')->__($phraseName);
	}

	public function getPasswordexpirationSaltAction($return = true, $req_scope = null)
	{
		// Always generate a new saltkey
		$current_salt = $this->mkpass();
		//Mage::getModel('core/config')->saveConfig("payment/callback_credentials/digestsalt_passwordexpiration", $current_salt);
		//$this->flushCache();	// Needs immediate action on the cache to make this setting active!
		$usescope = ($this->getRequest()->getParam('scope') != "" ? $this->getRequest()->getParam('scope') : 0);

		$username = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_id', $usescope);
		$password = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_password', $usescope);
		$digest = array(
			'digestAlgorithm' => 'SHA1',
			'digestSalt' => $current_salt,
			'parameters' => array('identifier' => '{identifier}')
		);
		$res = Mage::getModel('resursbank/api')->registerCallback("PASSWORD_EXPIRATION", $username, $password, $digest, $usescope, $current_salt);
		if (is_bool($res)) {
			$jsreturn = array();
			$jsreturn[] = 'var approved = true;';
			$jsreturn[] = 'var error = false;';
			$jsreturn[] = 'var errstring = ""';
			$jsreturn[] = 'var confirmed_scope = "' . $usescope . '";';
			//$jsreturn[] = 'var displaymsg = unescape("'.$this->__('Salt key updated').'");';
			$jsreturn[] = 'var displaymsg = "' . $current_salt . '";';
		} else {
			$jsreturn[] = 'var approved = false;';
			$jsreturn[] = 'var error = true;';
			$jsreturn[] = 'var errstring = unescape("' . rawurlencode((isset($res->faultstring) ? $res->faultstring : "")) . '");';
			//$jsreturn[] = 'var displaymsg = unescape("'.$this->__('Salt key failed').'");';
			$jsreturn[] = 'var displaymsg = "";';
		}
		if ($return) echo implode("\n", $jsreturn);
	}

	public function getUnfreezeSaltAction($return = true, $req_scope = null)
	{
		// Always generate a new saltkey
		$current_salt = $this->mkpass();
		if (is_null($req_scope)) {
			$usescope = ($this->getRequest()->getParam('scope') != "" ? $this->getRequest()->getParam('scope') : 0);
		} else {
			$usescope = $req_scope;
		}
		//Mage::getModel('core/config')->saveConfig("payment/callback_credentials/digestsalt_unfreeze", $current_salt);
		//$this->flushCache();	// Needs immediate action on the cache to make this setting active!

		$username = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_id', $usescope);
		$password = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_password', $usescope);
		$digest = array(
			'digestAlgorithm' => 'SHA1',
			'digestSalt' => $current_salt,
			'parameters' => array('paymentId' => '{paymentId}')
		);
		$res = Mage::getModel('resursbank/api')->registerCallback("UNFREEZE", $username, $password, $digest, $usescope);
		if (is_bool($res)) {
			$this->saveResursCallback("unfreeze", $current_salt, $usescope);
			$jsreturn = array();
			$jsreturn[] = 'var approved = true;';
			$jsreturn[] = 'var error = false;';
			$jsreturn[] = 'var errstring = ""';
			$jsreturn[] = 'var confirmed_scope = "' . $usescope . '";';
			//$jsreturn[] = 'var displaymsg = unescape("'.$this->__('Salt key updated').'");';
			$jsreturn[] = 'var displaymsg = "' . $current_salt . '";';
		} else {
			$jsreturn[] = 'var approved = false;';
			$jsreturn[] = 'var error = true;';
			$jsreturn[] = 'var errstring = unescape("' . rawurlencode((isset($res->faultstring) ? $res->faultstring : "")) . '");';
			//$jsreturn[] = 'var displaymsg = unescape("'.$this->__('Salt key failed').'");';
			$jsreturn[] = 'var displaymsg = "";';
		}
		if ($return) echo implode("\n", $jsreturn);
	}

	public function getAnnulmentSaltAction($return = true, $req_scope = null)
	{
		// Always generate a new saltkey
		$current_salt = $this->mkpass();
		if (is_null($req_scope)) {
			$usescope = ($this->getRequest()->getParam('scope') != "" ? $this->getRequest()->getParam('scope') : 0);
		} else {
			$usescope = $req_scope;
		}

		// SaveConfig Must be moved. This should not happen here
		// Mage::getModel('core/config')->saveConfig("payment/callback_credentials/digestsalt_annulment", $current_salt);
		//$this->flushCache();	// Needs immediate action on the cache to make this setting active!

		$username = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_id', $usescope);
		$password = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_password', $usescope);
		$digest = array(
			'digestAlgorithm' => 'SHA1',
			'digestSalt' => $current_salt,
			'parameters' => array('paymentId' => '{paymentId}')
		);
		$res = Mage::getModel('resursbank/api')->registerCallback("ANNULMENT", $username, $password, $digest, $usescope);
		if (is_bool($res)) {
			$this->saveResursCallback("annulment", $current_salt, $usescope);
			$jsreturn = array();
			$jsreturn[] = 'var approved = true;';
			$jsreturn[] = 'var error = false;';
			$jsreturn[] = 'var errstring = ""';
			$jsreturn[] = 'var confirmed_scope = "' . $usescope . '";';
			//$jsreturn[] = 'var displaymsg = unescape("'.$this->__('Salt key updated').'");';
			$jsreturn[] = 'var displaymsg = "' . $current_salt . '";';
		} else {
			$jsreturn[] = 'var approved = false;';
			$jsreturn[] = 'var error = true;';
			$jsreturn[] = 'var errstring = unescape("' . rawurlencode((isset($res->faultstring) ? $res->faultstring : "")) . '");';
			//$jsreturn[] = 'var displaymsg = unescape("'.$this->__('Salt key failed').'");';
			$jsreturn[] = 'var displaymsg = "";';
		}
		if ($return) echo implode("\n", $jsreturn);
	}

	public function getAutofraudcontrolSaltAction($return = true, $req_scope = null)
	{
		// Always generate a new saltkey
		$current_salt = $this->mkpass();
		if (is_null($req_scope)) {
			$usescope = ($this->getRequest()->getParam('scope') != "" ? $this->getRequest()->getParam('scope') : 0);
		} else {
			$usescope = $req_scope;
		}
		//Mage::getModel('core/config')->saveConfig("payment/callback_credentials/digestsalt_autofraudcontrol", $current_salt);
		//$this->flushCache();	// Needs immediate action on the cache to make this setting active!

		$username = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_id', $usescope);
		$password = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_password', $usescope);
		$digest = array(
			'digestAlgorithm' => 'SHA1',
			'digestSalt' => $current_salt,
			'parameters' => array('paymentId' => '{paymentId}', 'result' => '{result}')
		);
		$res = Mage::getModel('resursbank/api')->registerCallback("AUTOMATIC_FRAUD_CONTROL", $username, $password, $digest, $usescope);
		if (is_bool($res)) {
			$this->saveResursCallback("autofraudcontrol", $current_salt, $usescope);
			$jsreturn = array();
			$jsreturn[] = 'var approved = true;';
			$jsreturn[] = 'var error = false;';
			$jsreturn[] = 'var errstring = ""';
			$jsreturn[] = 'var confirmed_scope = "' . $usescope . '";';
			//$jsreturn[] = 'var displaymsg = unescape("'.$this->__('Salt key updated').'");';
			$jsreturn[] = 'var displaymsg = "' . $current_salt . '";';
		} else {
			$jsreturn[] = 'var approved = false;';
			$jsreturn[] = 'var error = true;';
			$jsreturn[] = 'var errstring = unescape("' . rawurlencode((isset($res->faultstring) ? $res->faultstring : "")) . '");';
			$jsreturn[] = 'var confirmed_scope = "' . $usescope . '";';
			//$jsreturn[] = 'var displaymsg = unescape("'.$this->__('Salt key failed').'");';
			$jsreturn[] = 'var displaymsg = "";';
		}
		if ($return) echo implode("\n", $jsreturn);
	}


	public function getPaymentMethodsAction()
	{
		$post = $this->getRequest()->getPost();
		$username = '';
		$password = '';
		$scope = 0;
		if (isset($post['customerId'])) {
			$username = $post['customerId'];
		}
		if (isset($post['password'])) {
			$password = $post['password'];
		}
		if (isset($post['setscope'])) {
			$scope = $post['setscope'];
			$this->forcescope = $scope;
		}
		$this->updateResursSettings($scope);
		$cron = Mage::getModel('resursbank/cron');
		$cron->getPaymentMethods($username, $password, $scope);
	}

	public function updateCallbackRegistrations($req_scope = null)
	{
		$this->getUnfreezeSaltAction(false, $req_scope);
		$this->getAnnulmentSaltAction(false, $req_scope);
		$this->getAutofraudcontrolSaltAction(false, $req_scope);
		$this->updateResursInvoiceSequenceAction();
	}

	public function updateResursInvoiceSequenceAction()
	{
		$scope = ($this->getRequest()->getParam('scope') != "" ? $this->getRequest()->getParam('scope') : 0);
		$sqv = Mage::getModel('resursbank/api')->getInvoiceSequenceNumber($scope);
		if (is_null($sqv) || $sqv == "0") {
			try {
				Mage::getModel('resursbank/api')->setInvoiceSequenceNumber("1");
				Mage::log("No previous invoice sequence found, setting new.", null, 'resurs.log');
			} catch (Exception $e) {
				Mage::log("updateResursInvoiceSequence Error: " . $e->getMessage(), null, 'resurs.log');
			}
		}
	}

	public function updateResursCallbacksAction()
	{
		$post = $this->getRequest()->getPost();
		$scope = ($this->getRequest()->getParam('scope') != "" ? $this->getRequest()->getParam('scope') : 0);
		$this->scope = $scope;
		$this->updateCallbackRegistrations($this->scope, false);
	}

	function saveResursCallback($settingParam = '', $settingValue = '', $scope = '')
	{
		if ($scope == '') {
			return;
		}    // Skip

		// Example:
		// payment/callback_credentials/digestsalt_XXX

		// What we want
		// unfreeze
		// annulment
		// autofraudcontrol
		// ---
		// passwordexpiration (deprecated)

		$savePath = "payment/callback_credentials/digestsalt_" . $settingParam;
		$writer = Mage::getSingleton('core/resource')->getConnection('core_write');
		$writer->query("DELETE FROM core_config_data WHERE path = '" . $savePath . "' AND scope_id = '" . intval($scope) . "' AND scope = '" . intval($scope) . "'");
		Mage::getModel('core/config')->saveConfig($savePath, $settingValue, 'default', $scope);
	}

	function deleteResursCallbackSaltAction()
	{
		//$writer = Mage::getSingleton('core/resource')->getConnection('core_write');
		//$delRes = $writer->query("DELETE FROM core_config_data WHERE path LIKE 'payment/callback_credentials/digestsalt_%'");
	}

	public function updateResursSettings($scope, $callUpdateRegistrations = true)
	{
		$post = $this->getRequest()->getPost();
		if (isset($post['customerId'])) {
			Mage::getModel('core/config')->saveConfig('payment/store_credentials/customer_represtantive_id', $post['customerId'], $scope);
		}
		if (isset($post['password'])) {
			Mage::getModel('core/config')->saveConfig('payment/store_credentials/customer_represtantive_password', $post['password'], $scope);
		}
		if (isset($post['settings'])) {
			Mage::getModel('core/config')->saveConfig('payment/common_settings/server', $post['settings'], $scope);
		}
		if (isset($post['countries'])) {
			Mage::getModel('core/config')->saveConfig('payment/common_settings/specificcountry', $post['countries'], $scope);
		}
		if (isset($post['tax_class'])) {
			Mage::getModel('core/config')->saveConfig('payment/common_settings/tax_class', $post['tax_class'], $scope);
		}
		if (isset($post['new_status'])) {
			Mage::getModel('core/config')->saveConfig('payment/common_settings/new_order_status', $post['new_status'], $scope);
		}
		if (isset($post['not_status'])) {
			Mage::getModel('core/config')->saveConfig('payment/common_settings/not_accepted_order_status', $post['not_status'], $scope);
		}
		if (isset($post['pending_status'])) {
			Mage::getModel('core/config')->saveConfig('payment/common_settings/payment_pending_status', $post['pending_status'], $scope);
		}
		if (isset($post['error_message'])) {
			Mage::getModel('core/config')->saveConfig('payment/common_settings/custom_error_text_message', $post['error_message'], $scope);
		}
		if ($callUpdateRegistrations) {
			$this->updateCallbackRegistrations($scope);
		}
	}

	protected function getCheckout()
	{
		return Mage::getSingleton('checkout/session');
	}

	public function getQuote()
	{
		return $this->getCheckout()->getQuote();
	}

	public function getAddressAction()
	{
		$post = $this->getRequest()->getPost();
		$result = Mage::getModel('resursbank/api')->getAddresses($post['ssn'], $post['ctype']);

		$address = $result['Data'];
		$checkout = $this->getQuote();
		$billingAddress = $checkout->getBillingAddress();
		$shippingAddress = $checkout->getShippingAddress();

		if ($result['Success'] == "true" && $address) {
			/* #55920 - In extremely verbose systems lastName is empty on company lookups */
			if (!isset($address->firstName)) {
				$address->firstName = "";
			}
			if (!isset($address->lastName)) {
				$address->lastName = "";
			}

			try {
				// Some isset-checks here are needed so we won't fail on company names
				$billingAddress->setFirstname($address->firstName);
				$billingAddress->setLastname($address->lastName);
				$billingAddress->setStreet($address->addressRow1);
				$billingAddress->setPostcode($address->postalCode);
				$billingAddress->setCity($address->postalArea);
				$billingAddress->save();

				if ($this->getQuote()->getShippingAddress()->getData('same_as_billing') == '1') {
					$shippingAddress->setFirstname($address->firstName);
					$shippingAddress->setLastname($address->lastName);
					$shippingAddress->setStreet($address->addressRow1);
					$shippingAddress->setPostcode($address->postalCode);
					$shippingAddress->setCity($address->postalArea);
					$shippingAddress->save();
				}
				$formattedAddress = "<div class='address-from-resurs'>
                        <p class='address-from-resurs-name'>{$address->firstName} $address->lastName</p>
                        <p>{$address->addressRow1}</p>
                        <p>{$address->postalCode}</p>
                        <p>{$address->postalArea}</p>
                </div>";

				$formattedAddress2 = "{$address->firstName},{$address->lastName},{$address->addressRow1},{$address->postalCode},{$address->postalArea}";
				$formattedAddress2 .= "," . $billingAddress->getCountry();
				if ($billingAddress->getTelephone()) {
					$formattedAddress2 .= "," . $billingAddress->getTelephone();
				}

				Mage::getSingleton('checkout/session')->setSsnAddress($formattedAddress);
				echo $formattedAddress2;
			} catch (Exception $getAddressError) {
				// DO NOT USE COMMAS IN THIS ERROR MESSAGE
				echo preg_replace("/,/", '', Mage::helper('resursbank')->__('Address service is currently unavailable. Please try again later.') . "\n" . $getAddressError->getMessage());
				return null;
			}
		}
		if (empty($result['Data'])) {
			// No error, but no response
			echo preg_replace("/,/", '', Mage::helper('resursbank')->__('Address information is currently unavailable.'));
		}

		if ($result['Error'] == "true") {
			echo $result['Data'];
		}
	}

	// A way to retrieve the current plugin version instead of reading from core_resource which may give us false information
	// depending on installations etc. Requirement for this to work: Send in the representative password to this plugin callback - encrypted.
	public function getVersionAction()
	{
		$post = $this->getRequest()->getParams();
		if ($post['rpid']) {
			$reader = Mage::getSingleton('core/resource')->getConnection('core_read');
			$dbcontent = $reader->fetchAll("SELECT * FROM `core_config_data` WHERE path = 'payment/store_credentials/customer_represtantive_password' AND scope = 'default' AND scope_id = '0' LIMIT 1");
			$val = sha1($dbcontent[0]['value']);
			if ($post['rpid'] === $val) {
				$moduleInfo = Mage::getConfig()->getModuleConfig("Resursbank_Resursbank");
				header('HTTP/1.0 200 OK');
				echo $moduleInfo->version;
				exit;
			} else {
				header('HTTP/1.0 403 Forbidden');
				echo "<b>Deny</b>";
				exit;
			}
		} else {
			header('HTTP/1.0 403 Forbidden');
			echo "<b>Deny</b>";
			exit;

		}
		exit;
	}

	public function getCostAction()
	{
		//$post = $this->getRequest()->getPost();
		$post = $this->getRequest()->getParams();

        if (!isset($post['amount']) ||isset($post['amount']) && $post['amount'] == "0") {
            $checkout = Mage::getSingleton('checkout/session')->getQuote();
            $post['amount'] = $checkout->getBaseGrandTotal();
        }

        $cssPath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN, true) . "frontend/default/default/resursbank/css/costofpurchase.css";
		$result = Mage::getModel('resursbank/api')->getCostOfPurchaseHtml($post['methodid'], $post['amount']);
		echo '<html>
        <head>
        <title>Information</title>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" media="all" href="' . $cssPath . '" />
        <script>
        function closeWin()
        {
            window.close();
        }
        </script>
        </head>
        <body>
        <div class="readme-popup-box readme-popup-box-representative">
        <a class="link-button" onclick="closeWin()" href="javascript:void(0);">' . Mage::helper('resursbank')->__("Close") . '</a>
        ' . $result . '
        </div>
        </body>
        </html>
        ';
	}

	// Simpified complex password generation
	public function mkpass()
	{
		$retp = null;
		$characterListArray = array(
			'abcdefghijklmnopqrstuvwxyz',
			'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
			'0123456789',
			'!@#$%*?'
		);
		$chars = array();
		$max = 10; // This is for now static
		foreach ($characterListArray as $charListIndex => $charList) {
			for ($i = 0; $i <= ceil($max / sizeof($characterListArray)); $i++) {
				$chars[] = $charList{mt_rand(0, (strlen($charList) - 1))};
			}
		}
		shuffle($chars);
		$retp = implode("", $chars);
		return $retp;
	}

	public function flushCache()
	{
		try {
			$allTypes = Mage::app()->useCache();
			foreach ($allTypes as $type => $blah) {
				Mage::app()->getCacheInstance()->cleanType($type);
			}
		} catch (Exception $e) {
			error_log($e->getMessage());
		}
		return true;
	}

	public function getStoreSalt($saltname = null, $storeid = 0)
	{
		$reader = Mage::getSingleton('core/resource')->getConnection('core_read');
		$dbcontent = $reader->fetchAll("SELECT * FROM `core_config_data` WHERE path = 'payment/callback_credentials/digestsalt_" . $saltname . "' AND scope = 'default' AND scope_id = '" . intval($storeid) . "' LIMIT 1");
		return (isset($dbcontent[0]['value']) ? $dbcontent[0]['value'] : null);
	}

    /**
     * Centralized callbacks (RB Edition) - one place to edit. Everything starts and (hopefully) ends here.
     * Fetching all callbacks here, via event-parameters.
     */
    public function callbackAction() {
        $state = "";
        $params = $this->getRequest()->getParams();

        //Mage::log('Callback is called. Identifier: '.$identifier, null, 'resurs.log');
        if (isset($params['paymentId'])) // Globally used in events
        {
            $order = Mage::getModel('sales/order')->loadByIncrementId($params['paymentId']);
            if (!$order->entity_id) {
                header("HTTP/1.0 403 paymentId missing");
                $callbacklog[] = "paymentId missing";
            }
        }

        //$identifier = $params['id'];	// Not in use
        $callbacklog = array();    // Initialize a FollowLog, so that we can follow the procedure to the end in our logs
        $usescope = (isset($params['storeidentifier']) ? $params['storeidentifier'] : 0);

        if (isset($params['event'])) {
            Mage::log('Callback is called. Event: ' . $params['event'], null, 'resurs.log', true);

            $siteid = (Mage::app()->getWebsite()->getId() ? Mage::app()->getWebsite()->getId() : null);

            if ($params['event'] == "unfreeze") {
                //$current_salt = Mage::getStoreConfig("payment/callback_credentials/digestsalt_unfreeze", $usescope);
                //echo "I use current Salt Test (Scope $usescope siteid $siteid, storeCode $storeCode): " . $current_salt;
                $current_salt = $this->getStoreSalt("unfreeze", $usescope);

                $digest = strtoupper(sha1($params['paymentId'] . $current_salt));
                $currentOrderStatus = $order->getData('status');
                if (!empty($params['paymentId']) && $digest == $params['digest']) {
                    if ($currentOrderStatus != "canceled" && $currentOrderStatus != "closed") {
                        $m_status = Mage::getStoreConfig('payment/common_settings/unfreeze_to_status', $usescope);
                        $m_state_array = $this->getCustomOrderState($m_status);
                        $m_state = $m_state_array['state'];
                        $m_label = $m_state_array['label'];

                        $callbacklog[] = "Current rule for " . $params['paymentId'] . ": State $m_state, Status $m_status, Label $m_label\n";
                        $m_translatestate = $this->orderStatus($m_state);    // Preserved

                        $comment = $this->__('Resurs Bank unfreezing order status');
                        $isCustomerNotified = false;
                        if ($m_state == "complete" || $m_label == "complete") {
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE);
                            $order->setData('status', Mage_Sales_Model_Order::STATE_COMPLETE);
                        } else {
                            try {
                                $order->setState($m_state, $m_status, $comment, $isCustomerNotified);
                            } catch (Exception $setStateError) {
                                $callbacklog[] = $setStateError;
                            }
                        }
                        $order->save();
                        header("HTTP/1.0 204 OK");
                        $callbacklog[] = "Order " . $params['paymentId'] . " unfrozen (forced)";
                    } else {
                        header("HTTP/1.0 403 Unfreeze on closed order prohibited");
                        $callbacklog[] = "Unfreeze on closed order prohibited";
                    }
                } else {
                    header("HTTP/1.0 403 Permission denied");
                    $callbacklog[] = "Paymentid missing or digest mismatch";
                    if ($currentOrderStatus == "canceled" || $currentOrderStatus == "closed") {
                        $callbacklog[] = "NoteOnly: Order status was already closed";
                    }
                }
            }
            if ($params['event'] == "annulment") {
                //$current_salt = Mage::getStoreConfig("payment/callback_credentials/digestsalt_annulment", $usescope);
                $current_salt = $this->getStoreSalt("annulment", $usescope);
                $digest = strtoupper(sha1($params['paymentId'] . $current_salt));
                if (!empty($params['paymentId']) && $digest == $params['digest']) {
                    $m_status = Mage::getStoreConfig('payment/common_settings/annulment_order_status', $usescope);
                    $m_state_array = $this->getCustomOrderState($m_status);
                    $m_state = $m_state_array['state'];
                    $m_label = $m_state_array['label'];

                    $callbacklog[] = "Current rule for " . $params['paymentId'] . ": State $m_state, Status $m_status, Label $m_label\n";
                    $m_translatestate = $this->orderStatus($m_state);    // Preserved

                    $comment = $this->__('Resurs Bank annuling order');
                    $isCustomerNotified = false;
                    try {
                        $order->setState($m_state, $m_status, $comment, $isCustomerNotified);
                    } catch (Exception $setStateError) {
                        $callbacklog[] = $setStateError;
                    }

                    $order->save();
                    header("HTTP/1.0 204 OK");
                    $callbacklog[] = "Order " . $params['paymentId'] . " annuled";
                } else {
                    header("HTTP/1.0 403 Order not annuled");
                    $callbacklog[] = "Denied: Order " . $params['paymentId'] . " not annuled (paymentid, current state ['.$order->getState().'] or salt mismatch)";
                }
            }
            if ($params['event'] == "automatic_fraud_control") {
                $current_salt = $this->getStoreSalt("autofraudcontrol", $usescope);
                //$current_salt = Mage::getStoreConfig("payment/callback_credentials/digestsalt_autofraudcontrol", $usescope);
                $digest = strtoupper(sha1($params['paymentId'] . strtoupper($params['result']) . $current_salt));

                //if (isset($_SERVER['MAGE_IS_RESURS_MODE']) && $_SERVER['MAGE_IS_RESURS_MODE'] == true && isset($_SERVER['DEBUG_EVENT_CALLBACK_FRAUD']) && $_SERVER['DEBUG_EVENT_CALLBACK_FRAUD'] == true) {if ($digest != $params['digest']) {$params['digest'] = $digest;}}
                if (!empty($params['paymentId']) && $digest == $params['digest']) {
                    //$goods = Mage::getModel("resursbank/api")->getProductTypes($order, true);
                    if (strtoupper($params['result']) == 'THAWED')    // Be sure we receive this in correct format
                    {
                        // downloadable && virtual and virtual products should get finalized as soon as we receive a thaw from resurs
                        // if the order contains virtuals/downloadable products only (no matter of payment method)
                        if (Mage::getModel('resursbank/api')->canFinalize($order, true))    // Only finalize if order contains virtuals only
                        {
                            $callbacklog[] = "This order can finalize immediately";
                            try {
                                Mage::getModel('resursbank/api')->finalizePayment($order->getPayment());
                                $callbacklog[] = "Resurs Finalize Status: OK - No errors";
                            } catch (Exception $finalizeException) {
                                $callbacklog[] = "Resurs Finalize Error: " . $finalizeException->getMessage();
                            }
                        }

                        // Get the correct state for custom statuses (Not needed unless we want to begin to not force this anymore)
                        $correctstate = $this->getCustomOrderState(Mage::getStoreConfig('payment/common_settings/freeze_order_status', $usescope));
                        $realstate = $correctstate['state'];    // Custom statuses has assigned states, check which it is here and use it below
                        $reallabel = $correctstate['label'];

                        $m_status = Mage::getStoreConfig('payment/common_settings/unfreeze_to_status');
                        $m_state_array = $this->getCustomOrderState($m_status);
                        $m_state = $m_state_array['state'];
                        $m_label = $m_state_array['label'];

                        $callbacklog[] = "Current rule for " . $params['paymentId'] . ": State $m_state, Status $m_status, Label $m_label";
                        $m_translatestate = $this->orderStatus($m_state);    // Preserved

                        $comment = $this->__('Resurs Bank unfreezing order status');
                        $isCustomerNotified = false;
                        if ($m_state == "complete" || $m_label == "complete") {
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE);
                            $order->setData('status', Mage_Sales_Model_Order::STATE_COMPLETE);
                        } else {
                            try {
                                $order->setState($m_state, $m_status, $comment, $isCustomerNotified);
                            } catch (Exception $setStateError) {
                                $callbacklog[] = $setStateError;
                            }
                        }
                        $order->save();
                        header("HTTP/1.0 204 OK");
                        $callbacklog[] = "Order " . $params['paymentId'] . " thawed [AUTOMATIC_FRAUD_CONTROL]";
                    } elseif (strtoupper($params['result']) == 'FROZEN') {
                        // Sanitizecheck received result
                        $m_status = Mage::getStoreConfig('payment/common_settings/freeze_order_status', $usescope);
                        $m_state_array = $this->getCustomOrderState($m_status);
                        $m_state = $m_state_array['state'];
                        $m_label = $m_state_array['label'];

                        $callbacklog[] = "Current rule for " . $params['paymentId'] . ": State $m_state, Status $m_status, Label $m_label\n";
                        $m_translatestate = $this->orderStatus($m_state);    // Preserved

                        $comment = $this->__('Resurs Bank freezing order status');
                        $isCustomerNotified = false;

                        if ($m_state == "complete" || $m_label == "complete") {
                            $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE);
                            $order->setData('status', Mage_Sales_Model_Order::STATE_COMPLETE);
                        } else {
                            try {
                                $order->setState($m_state, $m_status, $comment, $isCustomerNotified);
                            } catch (Exception $setStateError) {
                                $callbacklog[] = $setStateError;
                            }
                        }
                        $order->save();
                        header("HTTP/1.0 204 OK");
                        $callbacklog[] = "Order " . $params['paymentId'] . " frozen (forced) [AUTOMATIC_FRAUD_CONTROL]";
                    } else {
                        header("HTTP/1.0 403 Not thawed nor frozen");
                        $callbacklog[] = "Order " . $params['paymentId'] . " ignored due to THAW/FROZEN state mismatch - none of them received [AUTOMATIC_FRAUD_CONTROL]";
                    }
                } else {
                    header("HTTP/1.0 403 Automatic_Fraud_Control not invoked");
                    $callbacklog[] = "AUTOMATIC_FRAUD_CONTROL not invoked (paymentid or salt mismatch)";
                }
            }
            Mage::log('CallbackActionEnd: ' . implode(". ", $callbacklog), null, 'resurs.log', true);
        } else {
            // Ending procedure - Send complete event to log
            Mage::log('Callback is called with no event', null, 'resurs.log', true);
        }
    }

	public function setDeliveryAddress($order)
	{
		$shippingAddress = $order->getShippingAddress();
		$fullName = $shippingAddress->getFirstname() . " " . $shippingAddress->getLastname();
		$street = $shippingAddress->getStreet();

		$address = array('fullName' => $fullName, 'firstName' => $shippingAddress->getFirstname(), 'lastName' => $shippingAddress->getLastname(),
			'addressRow1' => $street[0],
			'postalArea' => $shippingAddress->getCity(), 'postalCode' => $shippingAddress->getPostcode(),
			'country' => $shippingAddress->getCountry()
		);
		$paymentSessionId = Mage::getSingleton('checkout/session')->getResursPaymentSession();
		return Mage::getModel('resursbank/api')->setDeliveryAddress($paymentSessionId, $address);
	}

	public function successAction()
	{
		$params = $this->getRequest()->getParams();
		$statuses = array();

		$model = Mage::getModel('resursbank/api');
		$result = $model->bookPayment($params['orderId'], $params['paymentSessionId']);
		Mage::log("bookPayment Begin, Result Follows:", null, 'resurs.log', true);
		Mage::log(print_r($result, true), null, 'resurs.log', true);
		$message = null;

		if ($result['success'] === true) {
			$order = Mage::getModel('sales/order');
			$order->loadByIncrementId($params['orderId']);
			$order->sendNewOrderEmail();
			$order->setEmailSent(true);
			$m_status = Mage::getStoreConfig('payment/common_settings/payment_pending_status');
			$m_state_array = $this->getCustomOrderState($m_status);
			$m_state = $m_state_array['state'];
			$m_label = $m_state_array['label'];
			$m_translatestate = $this->orderStatus($m_state);
			Mage::log("Current rule for " . (isset($params['paymentId']) ? $params['paymentId'] : "") . ": State $m_state, Status $m_status, Label $m_label", null, 'resurs.log', true);

			if ($m_status) {
				/*
				$use_order_status = $orderstatus;
				$use_custom_status = $this->getCustomOrderState($orderstatus);
				$state = ($use_custom_status['state'] ? $use_custom_status['state'] : $use_order_status);
				$status = $use_order_status;
				$isCustomerNotified = false;
				$order->setState($state, $status, $this->__('Success at Resurs Bank'), $isCustomerNotified);
				*/
				$isCustomerNotified = false;
				$order->setState($m_state, $m_status, $this->__('Success at Resurs Bank'), $isCustomerNotified);
			} else {
				$orderstatus = 'processing';
				$order->setState($orderstatus, $orderstatus, $this->__('Success at Resurs Bank'));
			}

			$order->save();

			// Internal note: registerAuthorization overrides processing-states and authorizes an order even if it should not be processed.
			// Therefore it is disabled since RB needs to confirm the order first.
			// If this fails in a future process, just put the authorizationnotification back.
			$payment = $order->getPayment();
			$payment->setTransactionId($result['message'])
				->setIsTransactionClosed(0);
			//->registerAuthorizationNotification($order->getGrandTotal());
			$order->save();

			$this->_redirect('checkout/onepage/success');
			return;
		} else {
			$order = Mage::getModel('sales/order');
			$order->loadByIncrementId($params['orderId']);
			if (!$order->getId()) {
				Mage::throwException(Mage::helper('resursbank')->__('No order for processing found'));
			}

			// To be removed
			/*
			$orderstatus = Mage::getStoreConfig('payment/common_settings/not_accepted_order_status');
			if($orderstatus)
			{
				$statuses = $this->getCustomOrderState($orderstatus);
				$order->setState($statuses['state'],$statuses['label'],$this->__('The order was canceled. An error occurred.'));
			}
			else
			{
				$order->setState(Mage_Sales_Model_Order::STATE_CANCELED, Mage_Sales_Model_Order::STATE_CANCELED,$this->__('The order was canceled. An error occurred.'));
			}
			*/

			$m_status = Mage::getStoreConfig('payment/common_settings/not_accepted_order_status');
			if ($m_status) {
				$m_state_array = $this->getCustomOrderState($m_status);
				$m_state = $m_state_array['state'];
				$m_label = $m_state_array['label'];
				$m_translatestate = $this->orderStatus($m_state);
				$order->setState($m_state, $m_status, $this->__('The order was canceled. An error occurred.'));
			} else {
				$message = isset($return['message']) && $return['message'] != "" ? $return['message'] : $this->__('The order was canceled. An error occurred.');
				$order->setState(Mage_Sales_Model_Order::STATE_CANCELED, Mage_Sales_Model_Order::STATE_CANCELED, $message);
			}

			$order->save();
			$this->maintainQuote();

			$errorMessage = $result['message'];

			$customErrorMessage = Mage::getStoreConfig('payment/common_settings/custom_error_text_message');
			if ($message != null) {
				$customErrorMessage = $message;
			}

			if (empty($customErrorMessage)) {
				if ($result['message'] != "") {
					$customErrorMessage = $result['message'];
				} else {
					$customErrorMessage = $this->__('The order was canceled. An error occurred.');
				}
				$customErrorMessage .= ' ' . $errorMessage;
			}

			$this->getCheckout()->addError($customErrorMessage);
			$this->_redirectUrl($result['return']);
			//$this->_redirect('checkout/cart');
		}
	}

	public function cancelAction()
	{
		$params = $this->getRequest()->getParams();
		$order = Mage::getModel('sales/order');
		$signfail = (isset($params['signFail']) ? $params['signFail'] : null);
		$paymentId = (isset($params['paymentId']) ? $params['paymentId'] : null);
		$identityerror = (isset($params['identityerror']) ? $params['identityerror'] : null);
		$errormsg = null;    // This errormessage is received from a catch below.
		$order->loadByIncrementId($params['orderId']);

		// Set default display error message
		$useerror = $this->__('The customer canceled the payment.');
		if ($signfail) {
			// Payment method should pass through here in case of throttling
			$signFailMessage = Mage::helper('resursbank')->__('Message from Resurs Bank: Something went wrong at the payment signing.');
			$useerror = $signFailMessage;
			if (Mage::getStoreConfig('payment/common_settings/locationsignfail') == "1") {
				$order->setState(Mage_Sales_Model_Order::STATE_CANCELED, Mage_Sales_Model_Order::STATE_CANCELED, $this->__('Payment canceled: Signing failed'));
				$order->save();
				$this->maintainQuote();
				$this->getCheckout()->addError($signFailMessage);
				$this->_redirectUrl(Mage::helper('checkout/url')->getCheckoutUrl());
				return;
			}
		}

		if (!$order->getId()) {
			Mage::throwException(Mage::helper('resursbank')->__('No order for processing found'));
		}
		if ($identityerror == "1") {
			$useerror = $this->__("Payment cancellation: Identity error - Already exists");
		}

		$orderstatus = Mage::getStoreConfig('payment/common_settings/not_accepted_order_status');
		if ($orderstatus) {
			$statuses = $this->getCustomOrderState($orderstatus);
			try {
				$order->setState($statuses['state'], $statuses['label'], $useerror);
			} catch (Exception $exErr) {
				error_log($exErr->getMessage());
				$errormsg = $exErr->getMessage();
				$order->setState(Mage_Sales_Model_Order::STATE_CANCELED, Mage_Sales_Model_Order::STATE_CANCELED, $this->__($errormsg));
			}
		} else {
			$order->setState(Mage_Sales_Model_Order::STATE_CANCELED, Mage_Sales_Model_Order::STATE_CANCELED, $useerror);
		}

		$order->save();
		$this->maintainQuote();

		// Default error message
		$error = Mage::getStoreConfig('payment/common_settings/custom_error_text_message');
		if ($errormsg) {
			$error = $errormsg;
		}    // If there is an errormsg, use that instead
		if (!$errormsg && $useerror) {
			$error = $useerror;
		}

		$this->getCheckout()->addError($error);
		$this->_redirect('checkout/cart');
	}

	public function isUseBillingAddressForShipping()
	{
		if (($this->getQuote()->getIsVirtual())
			|| !$this->getQuote()->getShippingAddress()->getSameAsBilling()
		) {
			return true;
		}
		return true;
	}

	public function orderplacedAction()
	{
		try {
			$checkoutsession = $this->getCheckout();
			$checkoutsession->setResursQuoteId($checkoutsession->getQuoteId());
			$checkoutsession->setResursRealOrderId($checkoutsession->getLastRealOrderId());
			$checkoutsession->getQuote()->setIsActive(false)->save();

			$order = Mage::getModel('sales/order');
			$order->loadByIncrementId($checkoutsession->getLastRealOrderId());
			$paymentId = $this->getRequest()->getParam('paymentMethodId'); // Just for throttling

			$error = Mage::getStoreConfig('payment/common_settings/custom_error_text_message');

			if (is_object($order) && $order->getId() > 0) {
				$orderstatus = Mage::getStoreConfig('payment/common_settings/new_order_status');
				if ($orderstatus) {
					$statuses = $this->getCustomOrderState($orderstatus);
					$order->setState($statuses['state'], $statuses['label'], $this->__('Order created'));
					$order->save();
				}

				$billingAddress = $order->getBillingAddress()->getData();

				if ($order->getShippingAddress()) {
					$deliveryAddress = $order->getShippingAddress()->getData();
					$resetFields = array('entity_id', 'parent_id', 'customer_address_id', 'quote_address_id', 'customer_id', 'address_type', 'prefix', 'middlename', 'suffix', 'vat_id', 'vat_is_valid', 'vat_request_id', 'vat_request_date', 'vat_request_success');

					foreach ($resetFields as $resetField) {
						unset($billingAddress[$resetField]);
						unset($deliveryAddress[$resetField]);
					}

					$sBillingAddress = serialize($billingAddress);
					$sDeliveryAddress = serialize($deliveryAddress);

					$eBillingAddress = base64_encode($sBillingAddress);
					$eDeliveryAddress = base64_encode($sDeliveryAddress);
				} else {
					$eBillingAddress = $eDeliveryAddress = true;
				}

				if ($eBillingAddress == $eDeliveryAddress) {
					$paymentSessionId = Mage::getSingleton('checkout/session')->getResursPaymentSession();
					$result = Mage::getModel('resursbank/api')->prepareSigning($checkoutsession->getLastRealOrderId(), $paymentSessionId, $paymentId);

					if ($result['success'] == true) {
						$this->_redirectUrl($result['return']);
					} elseif ($result['error'] == true) {
						$orderstatus = Mage::getStoreConfig('payment/common_settings/not_accepted_order_status');
						if ($orderstatus) {
							$statuses = $this->getCustomOrderState($orderstatus);
							$order->setState($statuses['state'], $statuses['label'], $this->__('The order was canceled.'));
						} else {
							$order->setState(Mage_Sales_Model_Order::STATE_CANCELED, Mage_Sales_Model_Order::STATE_CANCELED, $this->__('The order was canceled.'));
						}
						$order->save();
						$this->maintainQuote();
						$this->getCheckout()->addError($error . ".  " . $result['message']);
						$this->_redirect('checkout/cart');
					}
				} else {
					if ($this->setDeliveryAddress($order)) {
						$paymentSessionId = Mage::getSingleton('checkout/session')->getResursPaymentSession();
						$result = Mage::getModel('resursbank/api')->prepareSigning($checkoutsession->getLastRealOrderId(), $paymentSessionId, $paymentId);

						if ($result['success'] == true) {
							$this->_redirectUrl($result['return']);
						} elseif ($result['error'] == true) {
							$orderstatus = Mage::getStoreConfig('payment/common_settings/not_accepted_order_status');
							if ($orderstatus) {
								$statuses = $this->getCustomOrderState($orderstatus);
								$order->setState($statuses['state'], $statuses['label'], $this->__('The order was canceled.'));
							} else {
								$order->setState(Mage_Sales_Model_Order::STATE_CANCELED, Mage_Sales_Model_Order::STATE_CANCELED, $this->__('The order was canceled.'));
							}
							$order->save();
							$this->maintainQuote();
							$this->getCheckout()->addError($error . ".  " . $result['message']);
							$this->_redirect('checkout/cart');
						}
					} else {
						$orderstatus = Mage::getStoreConfig('payment/common_settings/not_accepted_order_status');
						if ($orderstatus) {
							$statuses = $this->getCustomOrderState($orderstatus);
							$order->setState($statuses['state'], $statuses['label'], $this->__('The order was canceled. setDeliveryAddress Api Error'));
						} else {
							$order->setState(Mage_Sales_Model_Order::STATE_CANCELED, Mage_Sales_Model_Order::STATE_CANCELED, $this->__('The order was canceled. setDeliveryAddress Api Error'));
						}
						$order->save();
						$this->maintainQuote();
						$this->getCheckout()->addError($error . ".  " . $this->__('setDeliveryAddress error'));
						$this->_redirect('checkout/cart');
					}
				}
			} else {
				echo $this->__('Error: ') . $this->__('Can not load order object. Session expired.');
			}
		} catch (Exception $e) {
			echo $this->__('Error:') . $e->getMessage();
		}
	}

	protected function orderStatus($orderStatus)
	{
		switch ($orderStatus) {
			case 'pending':
				return Mage_Sales_Model_Order::STATE_NEW;
			case 'processing':
				return Mage_Sales_Model_Order::STATE_PROCESSING;
			case 'complete':
				return Mage_Sales_Model_Order::STATE_PROCESSING;
			case 'closed':
				return Mage_Sales_Model_Order::STATE_CLOSED;
			case 'canceled':
				return Mage_Sales_Model_Order::STATE_CANCELED;
			case 'holded':
				return Mage_Sales_Model_Order::STATE_HOLDED;
		}
	}

	protected function maintainQuote()
	{
		$session = $this->getCheckout();
		if ($quoteId = $session->getResursQuoteId()) {
			$quote = Mage::getModel('sales/quote')->load($quoteId);
			if ($quote->getId()) {
				$quote->setIsActive(true)->save();
				$session->setQuoteId($quoteId);
			}
		}
	}

	public function dynamicformAction()
	{
		$checkout = Mage::getSingleton('checkout/session')->getQuote();
        $grandTotal = $checkout->getBaseGrandTotal();

		$paymentId = $this->getRequest()->getParam('paymentId');
		$billingAddress = $this->getQuote()->getBillingAddress();
		$paymentData = array('method' => 'resurspayment' . $paymentId);
		Mage::getSingleton('checkout/type_onepage')->savePayment($paymentData);
		$customer = Mage::getSingleton('customer/session')->getCustomer();
		if (Mage::getSingleton('customer/session')->isLoggedIn()) {
			$email = $customer->getEmail();
		} else {
			$email = $billingAddress->getEmail();
		}

		$paymentSession = Mage::getModel('resursbank/api')->startPaymentSession($paymentId, $checkout);
		if (!isset($paymentSession->return->limitApplicationFormAsObjectGraph->formElement)) {
			echo $paymentSession['message'];
			return;
		}
		$formElements = array();
		foreach ($paymentSession->return->limitApplicationFormAsObjectGraph->formElement as $row) {
			if (isset($row->format)) {
				$row->format = str_replace('\\\\', '\\', $row->format);
				$formElements[$row->name] = $row;
			}
		}
		$dynamicFormHtml = '
		    <script>
		        resursDataFormats[\'formElement\'] = ' . json_encode($formElements) . '
		    </script>
		';
		$dynamicFormHtml .= '<ul class="resurs-dynamic-form-list form-list">';
		Mage::getSingleton('checkout/session')->setResursPaymentSession($paymentSession->return->id);
		foreach ($paymentSession->return->limitApplicationFormAsObjectGraph->formElement as $row) {
			$dynamicFormHtml .= '<li>';
			$errorMessagePleaseEnter = isset($row->description) ? $row->description : "";
			switch ($row->type) {
				case 'text':
					$dynamicFormHtml .= '<div class="resurs-dynamic-form-input-field">';
					if ($row->name == 'applicant-telephone-number') {
						$type = " type='text' value='{$billingAddress->getTelephone()}'";
					} elseif ($row->name == 'applicant-mobile-number') {
						$type = " type='text' value='{$billingAddress->getTelephone()}'";
					} elseif ($row->name == 'applicant-email-address') {
						$type = " type='text' value='{$email}'";
					} else {
						$type = "type='text'";
					}
					$divDisplay = '';
					if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
						$divDisplay = 'style="display:none;"';
						if ($row->name == "applicant-government-id") {
							$divDisplay = 'style="display:"';
						}
						if ($row->name == "card-number") {
							$divDisplay = 'style="display:"';
						}
						if ($row->name == "applicant-full-name") {
							$divDisplay = 'style="display:"';
						}
						if ($row->name == "contact-government-id") {
							$divDisplay = 'style="display:"';
						}
					}
					$dynamicFormHtml .= '<div id="f_' . $row->name . '" ' . $divDisplay . '>';
					$dynamicFormHtml .= '<div' . (isset($row->mandatory) && $row->mandatory == 1 ? ' class="required"' : '') . ' id="l_' . $row->name . '">' . (isset($row->mandatory) && $row->mandatory == 1 ? '<em>*</em>' : '') . $row->label . '</div>';
					$dynamicFormHtml .= "<input " . $type . ' maxlength="' . (isset($row->length) ? $row->length : 32) . '"';
					$dynamicFormHtml .= ' data-mandatory="' . $row->mandatory . '"';
					$dynamicFormHtml .= ' id="' . $row->name . '"';
					$dynamicFormHtml .= ' name="' . $paymentId . '[' . $row->name . ']"';
					$dynamicFormHtml .= ' style="width:75% !important" />';
					$dynamicFormHtml .= '</div></div>';
			}
			$dynamicFormHtml .= '</li>';
		}
		$dynamicFormHtml .= "</ul>";
		$dynamicFormHtml .= "<input type='hidden' id='is_dynamic_form_loaded_$paymentId' name='is_dynamic_form_loaded_$paymentId' value='1'>";
		//$dynamicFormHtml .= '<script>DetectPreclickedForms();</script>';
        $this->getResponse()->setBody($dynamicFormHtml);
    }

    /**
     * The following method will be used for testing and purposes
     */
    public function practiceAction()
    {
    }

    public function getCustomOrderState($statusCode)
    {
        $label = $statusCode;
        $statuses = Mage::getModel('sales/order_status')
            ->getCollection()
            ->addFieldToFilter('status',$statusCode);
        foreach($statuses as $status)
        {
            $label = $status->getStoreLabel(Mage::app()->getStore());
        }

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $store_id = Mage::app()->getStore()->getStoreId();
        $status_code = 'custom_order_new';

        $query = 'SELECT state FROM ' . $resource->getTableName('sales_order_status_state'). ' WHERE status="'.$statusCode.'"';
        $results = $readConnection->fetchAll($query);
        $state = (isset($results[0]['state']) ? $results[0]['state'] : "");

        return array('state' => $state, 'label' => $label);
    }
}

