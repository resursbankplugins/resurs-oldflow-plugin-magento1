<?php
include_once("Mage/Adminhtml/controllers/System/ConfigController.php");
class Resursbank_Resursbank_System_ConfigController extends Mage_Adminhtml_System_ConfigController
{
    public function saveAction()
    {
        $session = Mage::getSingleton('adminhtml/session');
        $groups = $this->getRequest()->getPost('groups');
        
        $codePath = Mage::getBaseDir('code');
        $extensionPath = $codePath.DS.'local'.DS.'Resursbank'.DS.'Resursbank';
        $etcPath = $extensionPath.DS.'etc';
        $modelPath = $extensionPath.DS.'Model';
        
        $paymentConfigFlag = false;

        $paymentConfig = '<default><payment>';
		$storeCred = $groups['store_credentials']['fields']['customer_represtantive_id']['value'];

        foreach($groups as $payment=>$group)
        {
            if(preg_match('/resurspayment/', $payment))
            {
                $modelName = ucwords($payment);
                //$paymentId = substr($payment, 13);
				$paymentId = str_replace("resurspayment", '', $payment);
                $active = $group['fields']['active']['value'];

                if($active)
                {
                    //Create Payment Model
                    $modelAbstract = @file_get_contents($etcPath.DS.'resurs-payment-model-abstract.phtml');
                    $search = array(
                        '[[MODELNAME]]',
                        '[[PAYMENTCODE]]'
                    );
                    $replace = array(
                        $modelName,
                        $payment
                    );
                    $model = str_replace($search, $replace, $modelAbstract);
                    if(is_writable($modelPath))
                    {
                        //copy($modelPath.DS.$modelName.'.php', $etcPath.DS.'Backup'.DS.'Model'.DS.$modelName.'.php');
						$fromfile = $modelPath.DS.$modelName.'.php';
						$tofile = $etcPath.DS.'Backup'.DS.'Model'.DS.$modelName.'.php';
						
						@copy($fromfile, $tofile);
						$errors= error_get_last();
						if ($errors['type'] > 0)
						{
							Mage::log('System/ConfigController Backup Error: ' . $fromfile . ' => ' . $tofile . ' failed', null, 'resurs.log', true);
							Mage::log(print_r($errors, true), null, 'resurs.log', true);
						}
						else
						{
							Mage::log('System/ConfigController Backup OK: ' . $fromfile . ' => ' . $tofile, null, 'resurs.log', true);
						}
					
                        @file_put_contents($modelPath.DS.$modelName.'.php', $model);
                    }
                    $paymentConfig .= "<{$payment}><model>resursbank/{$payment}</model></{$payment}>";
                    $paymentConfigFlag = true;
                }
            }
        }

        $paymentConfig .= '</payment></default>';
        
        if($paymentConfigFlag)
        {
            //Update Configuration
            $configAbstract = @file_get_contents($etcPath.DS.'config-abstract.xml');
            $config = str_replace('[[PAYMENT-CONFIG]]', $paymentConfig, $configAbstract);
            
            if(is_writable($etcPath))
            {
                copy($etcPath.DS.'config.xml', $etcPath.DS.'Backup'.DS.'etc'.DS.'config.xml');
                @file_put_contents($etcPath.DS.'config.xml', $config);
            }
        }
		// A workaround for missing (nulled) payment titles
		/*
		$paymentTitlesQuery = ("
        UPDATE core_config_data u
		INNER JOIN core_config_data d ON u.path = d.path AND (u.value IS NOT NULL AND u.value != '')
		SET d.value = u.value
		WHERE (d.value IS NULL OR d.value = '') AND d.path LIKE 'payment/resurspayment%/title'
		");
		$writer = Mage::getSingleton('core/resource')->getConnection('core_write');
		$writer->query($paymentTitlesQuery);
		*/
        
        parent::saveAction();
    }
}
?>
