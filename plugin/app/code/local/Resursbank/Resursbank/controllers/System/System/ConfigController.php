<?php
include_once("Mage/Adminhtml/controllers/System/ConfigController.php");
class Resursbank_Resursbank_System_System_ConfigController extends Mage_Adminhtml_System_ConfigController
{
    public function saveAction()
    {
		//$methodCollection = Mage::getModel('resursbank/methods')->getCollection();
        $session = Mage::getSingleton('adminhtml/session');
        $groups = $this->getRequest()->getPost('groups');

		// Fetch our scopes with methods
		$websites=Mage::app()->getWebsites();
		$avail_scopes = array('0');
        // Collect scopes for which the payment methods should save later on
        foreach ($websites as $website)
		{
            //$code = $website->getCode();
			$codescope = $website->getId();
			$avail_scopes[] = $codescope;
        }
        $websiteCode = Mage::app()->getRequest()->getParam('website');
        if ($websiteCode) {
            $website = Mage::getModel('core/website')->load($websiteCode);
            $websiteId = $website->getId();
        }
        else{
            $websiteId = 0;
            $websiteCode = "default";
        }

		// Fetch our methods
		$methodCollection = Mage::getModel('resursbank/methods')->getCollection();
		$callbackScope = null;
        $paymentList = array();
        $currentScopeTitle = "";
		foreach($methodCollection as $method)
        {
			$paymentid = $method->getPaymentId();
			// Loop through our scopes from above to see what's still active - and make them join into the xml later on
            // This is where we want to make sure that all scopes have the same data
			foreach ($avail_scopes as $scopeid)
			{
                $thisScopeTitle = Mage::getStoreConfig('payment/resurspayment'.$paymentid.'/title', $scopeid);
                $paymentList[$paymentid][$scopeid] = $thisScopeTitle;

				if ($paymentid != "" && Mage::getStoreConfig('payment/resurspayment'.$paymentid.'/active', $scopeid))
				{
                    $currentScopeTitle = $thisScopeTitle;
					$groups['resurspayment' . $paymentid]['fields']['active']['value'] = 1;
				}
			}
		}

        // paymentmethod->fields->title->value
        // paymentmethod->fields->title->inherit
        // websiteId = cscope

        foreach($groups as $payment=>$group)
        {
            if (preg_match('/resurspayment/', $payment))
            {
                if ($websiteId > 0)
                {
                    if (isset($group['fields']['title']['value'])) {
                        $title = $group['fields']['title']['value'];
                        Mage::getModel('core/config')->saveConfig('payment/'.$payment.'/title', $title);
                    }
                    else{
                        // Inherit space
                    }
                }
            }
        }

        $codePath = Mage::getBaseDir('code');
        $extensionPath = $codePath.DS.'local'.DS.'Resursbank'.DS.'Resursbank';
        $etcPath = $extensionPath.DS.'etc';
        $modelPath = $extensionPath.DS.'Model';
        
        $paymentConfigFlag = false;
        
        $isUsernameChangeActionCompleted = false;
        $isPasswordChangeActionCompleted = false;
        
        $currentResursbankUsername = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_id');
        $currentResursbankPassword = Mage::getStoreConfig('payment/store_credentials/customer_represtantive_password');
		$storeCred = isset($groups['store_credentials']['fields']['customer_represtantive_id']['value']) ? $groups['store_credentials']['fields']['customer_represtantive_id']['value'] : $currentResursbankUsername;
		$storeCredPass = isset($groups['store_credentials']['fields']['customer_represtantive_password']['value']) ? $groups['store_credentials']['fields']['customer_represtantive_password']['value'] : $currentResursbankPassword;


        $paymentConfig = '<default><payment>';
        foreach($groups as $payment=>$group)
        {
            if(preg_match('/resurspayment/', $payment))
            {
                $modelName = ucwords($payment);
				$paymentId = str_replace("resurspayment", '', $modelName);
                $active = (isset($group['fields']['active']['value']) ? $group['fields']['active']['value'] : 0);
                if($active)
                {
                    //Create Payment Model
                    $modelAbstract = @file_get_contents($etcPath.DS.'resurs-payment-model-abstract.phtml');

                    $search = array(
                        '[[MODELNAME]]',
                        '[[PAYMENTCODE]]'
                    );
                    $replace = array(
                        $modelName,
                        $payment
                    );
                    $model = str_replace($search, $replace, $modelAbstract);
                    if(is_writable($modelPath))
                    {
						// Silent copy in case of errors - if there is something to backup
						if (file_exists($modelPath.DS.$modelName.'.php'))
						{
							//@copy($modelPath.DS.$modelName.'.php', $etcPath.DS.'Backup'.DS.'Model'.DS.$modelName.'.php');
							$fromfile = $modelPath.DS.$modelName.'.php';
							$tofile = $etcPath.DS.'Backup'.DS.'Model'.DS.$modelName.'.php';
							@copy($fromfile, $tofile);
							$errors= error_get_last();
							if ($errors['type'] > 0)
							{
								Mage::log('System/System/ConfigController Backup Error: ' . $fromfile . ' => ' . $tofile . ' failed', null, 'resurs.log', true);
								Mage::log(print_r($errors, true), null, 'resurs.log', true);
							}
							else
							{
								Mage::log('System/System/ConfigController Backup OK: ' . $fromfile . ' => ' . $tofile, null, 'resurs.log', true);
							}
						}
						// Write the new file
                        @file_put_contents($modelPath.DS.$modelName.'.php', $model);
                    }
					$paymentConfig .= "<{$payment}><model>resursbank/{$payment}</model></{$payment}>";
                    $paymentConfigFlag = true;
                }
            }
        }
        $paymentConfig .= '</payment></default>';
        
        if($paymentConfigFlag)
        {
            //Update Configuration
            $configAbstract = @file_get_contents($etcPath.DS.'config-abstract.xml');
            $config = str_replace('[[PAYMENT-CONFIG]]', $paymentConfig, $configAbstract);
            
            if(is_writable($etcPath.DS.'config.xml'))
            {
                copy($etcPath.DS.'config.xml', $etcPath.DS.'Backup'.DS.'etc'.DS.'config.xml');
                @file_put_contents($etcPath.DS.'config.xml', $config);
            }
            else
            {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('resursbank')->__('Payment methods could not updated. Config file not writable').': '.$etcPath.DS.'config.xml');
            }
        }

        parent::saveAction();
        $callbackRegUrl = Mage::getUrl('resursbank/index/updateResursCallbacks', array('scope' => $websiteId));
        $postdata = http_build_query(
            array(
                'u' => $storeCred,
                'p' => $storeCredPass
            )
        );
        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );
        $context = stream_context_create($opts);		$res = file_get_contents($callbackRegUrl, false, $context);

		// A workaround for missing (nulled) payment titles - Not recommended, since this only updates titles once, when saved and no more due to removal of nulldata
        /*
		$paymentTitlesQuery = ("
        UPDATE core_config_data u
		INNER JOIN core_config_data d ON u.path = d.path AND (u.value IS NOT NULL AND u.value != '')
		SET d.value = u.value
		WHERE (d.value IS NULL OR d.value = '') AND d.path LIKE 'payment/resurspayment%/title'
		");
		$writer = Mage::getSingleton('core/resource')->getConnection('core_write');
		$writer->query($paymentTitlesQuery);
        */
    }
}
?>
