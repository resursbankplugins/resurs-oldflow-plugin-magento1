<?php
$installer = $this;
$installer->startSetup();

$installer->run("
	DROP TABLE IF EXISTS {$this->getTable('resursbank_payment_methods')};
	CREATE TABLE IF NOT EXISTS {$this->getTable('resursbank_payment_methods')} (
	  `id` int(11) unsigned NOT NULL auto_increment,
	  `payment_id` varchar(50) NOT NULL default '',
	  `description` varchar(255) NOT NULL default '',
	  `legal_info_links` varchar(255) NOT NULL default '',
	  `min_limit` double,
	  `max_limit` double,
	  `payment_type` varchar(100) NOT NULL default '',
	  `status` tinyint(1) NOT NULL default '1',
          `user_id` varchar(100) NOT NULL default '',
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	");
$installer->endSetup(); 
?>