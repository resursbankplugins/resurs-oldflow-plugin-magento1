<?php
$installer = $this;
$installer->startSetup();

// As long as the column type is always the same (DECIMAL 10,2), this works just fine.
$addtosql['sales/order'] = array('tax_fee_amount', 'tax_base_fee_amount', 'tax_fee_amount_refunded', 'tax_base_fee_amount_refunded', 'tax_fee_amount_invoiced', 'tax_base_fee_amount_invoiced');
$addtosql['sales/quote_address'] = array('tax_fee_amount', 'tax_base_fee_amount');
$addtosql['sales/invoice'] = array('tax_fee_amount', 'tax_base_fee_amount');
$addtosql['sales/creditmemo'] = array('tax_fee_amount', 'tax_base_fee_amount');

// Anti-Colliding Installer Script - 2013-11-08
$reader = Mage::getSingleton('core/resource')->getConnection('core_read');
foreach ($addtosql as $name => $arr)
{
	// Fetch a row from the tables defined above, just in case there are already entries there, to save time and errors.
	$dbcontent = $reader->fetchAll("SELECT * FROM `".$this->getTable($name) . "` LIMIT 1");
	foreach ($arr as $column)
	{
		if (!$dbcontent[$column])
		{
			$str = "ALTER TABLE  `".$this->getTable($name)."` ADD  `".$column."` DECIMAL( 10, 2 ) NOT NULL;\n";
			// Try to alter the table and ignore errors if any. Errors should ONLY occur if the table is completely empty (in that case, this is probably a completely new installation of magento with no content)
			try {$installer->run($str);} catch (Exception $e) {$info .= "Column $column failed\n";}			
		}
	}
}
$installer->endSetup(); 

?>