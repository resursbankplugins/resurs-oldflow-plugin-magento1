function changePaymentLogoUrl(paymentid, skinplace, defaultlogo)
{
	if (null != document.getElementById(paymentid) && null != document.getElementById(paymentid + "_img"))
	{
		var logoVal = document.getElementById(paymentid).value;
		if (logoVal == "") {logoVal = defaultlogo;}
		// Look for absolute and relative pathways and fix them
		if (logoVal.substring(0,1) == "/") {logoVal = skinplace + logoVal;}
		document.getElementById(paymentid + "_img").src = logoVal;
	}
}
