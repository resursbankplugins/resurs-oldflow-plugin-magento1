/*! Resurs Bank Addon-Scripts for clearing numeric form fields */

if (typeof $jRB === "undefined") {
    $jRB = jQuery.noConflict();
}

var sh_a_GovId = "";
var sh_a_Tel = "";
var sh_a_Mob = "";
var sh_a_Eml = "";
var ga_ssn = "";
var sh_Method = "";
var sh_MethodChoice = "";

var sh_a_contactGovId = "";
var sh_a_contactName = "";
var sh_a_cardNum = "";
var totalPaymentMethodsAvailable = 0;
var resursDataFormats = {};


function cleanNaN(stringvalue) {
    var isClean = false;
    var testClean = "";
    if (typeof stringvalue == "undefined") {
        return "";
    }
    try {
        var clearVal = stringvalue.replace(/[^0-9\+\-\.\ $]/g, '');
        testClean = stringvalue.replace(/[0-9|\+|\-|\.\ ]/g, '');
    }
    catch (e) {
        //alert("str: " + stringvalue + " = " + e);
    }

    if (clearVal.length != 0) {
        isClean = true;
        if (testClean.length > 0) {
            isClean = false;
        }
    }
    return isClean;
}
function getCleanNaN(stringvalue) {
    var useCleanValues = true;
    if (useCleanValues) {
        var clearVal = "";
        if (typeof stringvalue == "undefined") {
            return "";
        }
        try {
            clearVal = stringvalue.replace(/[^0-9\+\-\.$]/g, '');
        }
        catch (e) {
        }
        if (clearVal.length != 0) {
            return clearVal;
        }
    }
    else {
        return stringvalue;
    }
}


/* 2015-09-25 */
var checkFormEmail = null;
var checkFormPhone = null;
var checkFormMobile = null;
var checkFormGovId = null;

var resursMethodChoice = null;
var resursMethodChoiceShort = null;
var resursMethodList = [];
var resursRoute = null;
var resursDynForm = null;
var loaderImage = "";
var getCostUrl = "";

var applicantFullNameChanged = false;