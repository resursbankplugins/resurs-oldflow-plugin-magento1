/*!
 * Resurs Bank Addons 1.4.0
 * Refactored Edition
 *
 * Container
 *  - Parts for form.phtml
 *
 * Internal note: default/default is the script that usually has higher priority than base/default
 *
 * Rules of enforced numbering:
 *
 * Telephone field must be numeric
 * Dynamic telephone number changes every time telephone field is updated
 * Dynamic mobile will only change to what's in telephone field as long as the field is empty or non numeric
 *
 */

var transPhrase = null;
var postQuery = null;
var postQueryTry = 0;
var postQueryStart = 0;
var postQueryStop = 0;

if (typeof $jRB === "undefined") {
    $jRB = jQuery.noConflict();
}
$jRB(document).ready(function() {
    if (resursRoute != "checkout") {
        ResursMethodBind();
    }
});

function ResursMethodBind() {
    if (typeof $jRB === "undefined") {
        $jRB = jQuery.noConflict();
    }
    if (typeof resursMethodList !== "undefined") {
        if (resursMethodList.length > 0) {
            for (var ml = 0; ml < resursMethodList.length; ml++) {
                if (typeof $jRB().live === "function") {
                    if ($jRB('#p_method_resurspayment' + resursMethodList[ml]).length > 0) {
                        $jRB('#p_method_resurspayment' + resursMethodList[ml]).die('click');
                        $jRB('#p_method_resurspayment' + resursMethodList[ml]).live('click', function () {
                            try {
                                ResursDynamicFormLoader(this.id.substr(22), resursRoute, resursDynForm);
                            } catch (e) {
                                console.log("ResursDynamicFormLoaderException: " + e);
                            }
                        });
                    } else {
                        console.log("Bind fail: " + resursMethodList[ml]);
                    }
                } else {
                    if ($jRB('#p_method_resurspayment' + resursMethodList[ml]).length > 0) {
                        $jRB('#p_method_resurspayment' + resursMethodList[ml]).off("click");
                        $jRB('#p_method_resurspayment' + resursMethodList[ml]).on("click", function () {
                            try {
                                ResursDynamicFormLoader(this.id.substr(22), resursRoute, resursDynForm);
                            } catch (e) {
                                console.log("ResursDynamicFormLoaderException: " + e);
                            }
                        });
                    }
                }
                if ($jRB('#p_method_resurspayment' + resursMethodList[ml]).is(":checked")) {
                    $jRB('#p_method_resurspayment' + resursMethodList[ml]).click();
                }
            }
        }
    } else {
        console.log("Method list is empty");
    }
}

/* Instantly bind formloader to a click (Called from form.phtml) since document.ready fails */
function ResursMethodInstantBind(ResursMethodId, RMethodRoute, RMethodForm) {
    if (typeof $jRB().live === "function") {
        $jRB('#p_method_resurspayment' + ResursMethodId).die("click");
        $jRB('#p_method_resurspayment' + ResursMethodId).live("click", function () {
            try {
                ResursDynamicFormLoader(ResursMethodId, RMethodRoute, RMethodForm);
            } catch (e) {
                console.log("ResursDynamicFormLoaderException: " + e);
            }
        });
    } else {
        $jRB('#p_method_resurspayment' + ResursMethodId).off("click");
        $jRB('#p_method_resurspayment' + ResursMethodId).on("click", function () {
            try {
                ResursDynamicFormLoader(ResursMethodId, RMethodRoute, RMethodForm);
            } catch (e) {
                console.log("ResursDynamicFormLoaderException: " + e);
            }
        });
    }
    /* Found a prechecked method, lets call for the form */
    if ($jRB('#p_method_resurspayment' + ResursMethodId).is(":checked")) {
        $jRB('#p_method_resurspayment' + ResursMethodId).click();
    }
}

var ssnLanguageText = "Enter social security number";


// Old actionurl does not exist here
function getAddress(actionUrl,customerType, loadImage)
{
    var phraseUrl = actionUrl.replace('getAddress', 'getJSPhrase');
    ssn = $jRB('#resurs-ssn').val();
    if(ssn == "")
    {
        $jRB.ajax(
            {
                type:'POST',
                url: phraseUrl,
                data: {'phrase': ssnLanguageText},
                success: function(output) {alert(output);},
                error: function(output) {alert(ssnLanguageText);}
            }
        );
        return;
    }
    ga_ssn = ssn;
    if (typeof sh_a_GovId !== "undefined" && sh_a_GovId == "") {
        sh_a_GovId = ssn;
    }
    // Address containter is not necessary
    if ($jRB('#resurs-ssn-address-container').length > 0) {setText('#resurs-ssn-address-container', (typeof loaderImage != "undefined" ? loaderImage : "Please wait..."));}
    var customerTypeTest = "NATURAL";
    try
    {
        var billingCompanyField = $jRB('body').children().find('input[name="billing[company]"]');
        if (typeof billingCompanyField == "object") {customerTypeTest = (billingCompanyField.val() == "" ? "NATURAL" : "LEGAL");}
    }
    catch(e) {}
    customerType = customerTypeTest;
    $jRB.ajax({
        type:'POST',
        url:actionUrl,
        data: {'ssn': ssn, 'ctype':customerType},
        success: function(output)
        {
            output = $jRB.trim(output);

            var formattedAddress = "<div class='address-from-resurs'>";
            var addressDetails = output.split(',');

            if(addressDetails[1] != undefined )
            {
                formattedAddress += "<p class='address-from-resurs-name'>"+addressDetails[0]+" "+addressDetails[1]+"</p>";
                formattedAddress += "<p>"+addressDetails[2]+"</p>";
                formattedAddress += "<p>"+addressDetails[3]+"</p>";
                formattedAddress += "<p>"+addressDetails[4]+"</p>";

                var billingFirstname = $jRB('body').children().find('input[name="billing[firstname]"]');
                var billingLastname = $jRB('body').children().find('input[name="billing[lastname]"]');
                var billingStreet = $jRB('body').children().find('input[name="billing[street][]"]');
                var billingPostcode = $jRB('body').children().find('input[name="billing[postcode]"]');
                var billingCity = $jRB('body').children().find('input[name="billing[city]"]');

                if ($jRB('#applicant-full-name').length > 0 && applicantFullNameChanged === false) {
                    $jRB('#applicant-full-name').val(addressDetails[0]+" "+addressDetails[1]);
                }

                //var billingCountry = $jRB('body').children().find('input[name="billing[country_id]"]');

                // Returned from getAddress: firstname,lastname,street,postcode,city
                billingFirstname.val(typeof addressDetails[0] == "string" ? addressDetails[0] : "");
                billingLastname.val(typeof addressDetails[0] == "string" ? addressDetails[1] : "");
                billingStreet.each(function(i){
                    var streetId = $jRB(this).attr('id');
                    if(streetId == 'billing:street1')
                    {
                        $jRB(this).val(addressDetails[2]);
                    }
                });
                billingPostcode.val(typeof addressDetails[0] == "string" ? addressDetails[3] : "");
                billingCity.val(typeof addressDetails[0] == "string" ? addressDetails[4] : "");
                //billingCountry = (typeof addressDetails[0] == "string" ? addressDetails[5] : "");

                // Continue delegate SSN to the next field
                /*
                 if ($jRB('#applicant-government-id').length > 0 && ssn != "" && $jRB('#applicant-government-id').val() != "")
                 {
                 sh_a_GovId = ssn;
                 $jRB('#applicant-government-id').val(ssn);
                 }
                 */
            }
            else
            {
                alert(addressDetails[0]);
                formattedAddress += "<p class='address-from-resurs-name'>"+addressDetails[0]+"</p>";
            }
            formattedAddress += "</div>";

            if ($jRB('#resurs-ssn-address-container').length > 0) {setText('#resurs-ssn-address-container', "");}

            if(addressDetails[1] != undefined )
            {
                var billingAddress = "<strong>Billing Address</strong> <a href=\"javascript:void(0)\" onclick=\"reviewInfo.editBlock('billing-address')\">Change</a><br>";
                var shippingAddress = "<strong>Shipping Address</strong> <a href=\"javascript:void(0)\" onclick=\"reviewInfo.editBlock('shipping-address')\">Change</a><br>";

                var fireCheckoutAddress = addressDetails[0];
                if(addressDetails[1] != undefined ) {fireCheckoutAddress += " "+addressDetails[1];}
                for(var i=2; i<addressDetails.length; i++)
                    fireCheckoutAddress += "<br>"+addressDetails[i];

                if($jRB('body').children().find('input[name="shipping[same_as_billing]"]').is(':checked'))
                {
                    $jRB('#billing-address-review').html(billingAddress+fireCheckoutAddress);
                    $jRB('#shipping-address-review').html(shippingAddress+fireCheckoutAddress);
                }
                else
                {
                    $jRB('#billing-address-review').html(billingAddress+fireCheckoutAddress);
                }
            }
        }
    });
}
function setText(selector,value)
{
    $jRB(selector).html(value);
}
function getCostPopup(paymentId,actionUrl,amount)
{
    var costWindow = window.open(actionUrl, "costOfPurchasePopup", "menubar=0,scrollbars=1,status=0,titlebar=1");
    costWindow.focus();
}

function closePopup(paymentId)
{
    $jRB('.readme-popup-box').hide();
}


/*** form.phtml js extraction ***/
function openPopup(paymentId)
{
    $jRB('.readme-popup-box'+paymentId).show();
    $jRB('#popup-ajax-loader'+paymentId).show();
    setText('#get-cost-html-content'+paymentId, '');
    $jRB('.readme-popup-box').css('left',($jRB(window).width()-$jRB('.readme-popup-box').outerWidth())/ 2 + 'px');
}
function getText(selector)
{
    return $jRB(selector).html();
}


// For future use
function fetchPhrase() {}

function MagentoResTransField(MagentoFieldObject, MagentoDestObject, IsNumeric) {
    try {
        if (null != MagentoFieldObject) {
            if (MagentoFieldObject.length > 0 && MagentoDestObject.length > 0) {
                if (!IsNumeric) {
                    MagentoDestObject.val(MagentoFieldObject.val());
                    return true;
                } else {
                    MagentoDestObject.val(getCleanNaN(MagentoFieldObject.val()));
                    return true;
                }
            }
        } else {
            console.log("MagentoResTransField problem: Tried to fetch the origin object, but it was null (Destination value "+MagentoDestObject.val()+")");
        }
    } catch (e) {
        console.log("MagentoResTransFieldException: " +e);
    }
}

function fetchNameFields(firstNameObj, lastNameObj) {
    if (applicantFullNameChanged === false) {
        if ($jRB('#applicant-full-name').length > 0) {
            $jRB('#applicant-full-name').val(firstNameObj.val() + " " + lastNameObj.val());
        }
    }
}

/*
 * A statically bound fieldreader for memorizing field data.
 */
function FetchMagentoFields() {
    checkFormEmail = $jRB('body').children().find('input[name="billing[email]"]');

    if ($jRB('#resurs-ssn').length > 0) {
        if ($jRB('#contact-government-id').length > 0) {
            if ($jRB('#contact-government-id').val() == "" && $jRB('#resurs-ssn').length > 0 && $jRB('#resurs-ssn').val() != "") {
                $jRB('#contact-government-id').val($jRB('#resurs-ssn').val());
                sh_a_GovId = $jRB('#contact-government-id').val();
            }
        } else {
            if ($jRB('#applicant-government-id').length > 0 && $jRB('#contact-government-id').length == 0) {
                if ($jRB('#applicant-government-id').val() == "" && $jRB('#resurs-ssn').length > 0 && $jRB('#resurs-ssn').val() != "") {
                    $jRB('#applicant-government-id').val($jRB('#resurs-ssn').val());
                    sh_a_GovId = $jRB('#applicant-government-id').val();
                }
            }
        }
        $jRB('#resurs-ssn').bind('blur', function () {
            /*
             * See if there is a used snippet located on the page and put our data in the correct fields.
             * If there is a contact government id located on the page, use that field as a primary target.
             */
            if ($jRB('#contact-government-id').length > 0) {
                if ($jRB('#contact-government-id').val() == "" && $jRB('#resurs-ssn').length > 0 && $jRB('#resurs-ssn').val() != "") {
                    $jRB('#contact-government-id').val($jRB('#resurs-ssn').val());
                    sh_a_GovId = $jRB('#contact-government-id').val();
                }
            } else {
                if ($jRB('#applicant-government-id').length > 0 && $jRB('#contact-government-id').length == 0) {
                    if ($jRB('#applicant-government-id').val() == "" && $jRB('#resurs-ssn').length > 0 && $jRB('#resurs-ssn').val() != "") {
                        $jRB('#applicant-government-id').val($jRB('#resurs-ssn').val());
                        sh_a_GovId = $jRB('#applicant-government-id').val();
                    }
                }
            }
        });
    }
    if (checkFormEmail.length > 0) {
        checkFormPhone = $jRB('body').children().find('input[name="billing[telephone]"]');
        checkFormMobile = $jRB('body').children().find('input[name="billing[telephone]"]');

        checkFormEmail.bind('blur', function () {
            if (MagentoResTransField(checkFormEmail, $jRB('#dynamicForm' + sh_Method).children().find('input#applicant-email-address'), false)) {
                sh_a_Eml = checkFormEmail.val();
            }
        });
        if ($jRB('#applicant-government-id').length > 0) {
            CreateValidation($jRB('#applicant-government-id'), 'resursbank-validate-govid', 'applicant-government-id');
            $jRB('#applicant-government-id').bind('blur', function () {
                if ($jRB('#applicant-government-id').val() != "") {
                    sh_a_GovId = $jRB('#applicant-government-id').val();
                }
            });
        }
        CreateValidation($jRB('#applicant-telephone-number'), 'resursbank-validate-phone', 'applicant-telephone-number');
        CreateValidation($jRB('#applicant-mobile-number'), 'resursbank-validate-mobile', 'applicant-mobile-number');
        CreateValidation($jRB('#applicant-email-address'), 'resursbank-validate-mail', 'applicant-email-address');

        /* Bind content changes */
        checkFormPhone.bind('blur', function () {
            if (MagentoResTransField(checkFormPhone, $jRB('#dynamicForm' + sh_Method).children().find('input#applicant-telephone-number'), true)) {
                sh_a_Tel = checkFormPhone.val();
            }
            if (MagentoResTransField(checkFormMobile, $jRB('#dynamicForm' + sh_Method).children().find('input#applicant-mobile-number'), true)) {
                sh_a_Mob = checkFormMobile.val();
            }
        });
        checkFormMobile.bind('blur', function () {
            if (MagentoResTransField(checkFormPhone, $jRB('#dynamicForm' + sh_Method).children().find('input#applicant-telephone-number'), true)) {
                sh_a_Tel = checkFormPhone.val();
            }
            if (MagentoResTransField(checkFormMobile, $jRB('#dynamicForm' + sh_Method).children().find('input#applicant-mobile-number'), true)) {
                sh_a_Mob = checkFormMobile.val();
            }
        });
    }
    if ($jRB('#contact-government-id').length > 0) {
        $jRB('#contact-government-id').bind('blur', function () {
            if ($(this).value != '') {
                sh_a_contactGovId = $(this).value;
            }
        });
    }
    if ($jRB('#card-number').length > 0) {
        $jRB('#card-number').bind('blur', function () {
            if ($(this).value != '') {
                sh_a_cardNum = $(this).value;
            }
        });
    }
    if ($jRB('#applicant-full-name').length) {
        $jRB('#applicant-full-name').bind('blur', function () {
            if ($(this).value != '') {
                sh_a_contactName = $(this).value;
                applicantFullNameChanged = true;
                console.log("Appl Full Change");
            }
        });
        var firstNameBind = $jRB('body').children().find('input[name="billing[firstname]"]');
        var lastNameBind = $jRB('body').children().find('input[name="billing[lastname]"]');
        if (firstNameBind.length > 0 && lastNameBind.length > 0) {
            firstNameBind.bind('change', function() {
                fetchNameFields(firstNameBind, lastNameBind);
            });
            lastNameBind.bind('change', function() {
                fetchNameFields(firstNameBind, lastNameBind);
            });
        }
    }


    if ($jRB('#applicant-government-id').length > 0 && $jRB('#contact-government-id').length == 0 && $jRB('#applicant-government-id').val() == "" && typeof sh_a_GovId !== "undefined" && sh_a_GovId != "") {
        $jRB('#applicant-government-id').val(sh_a_GovId);
    }

    if ($jRB('#applicant-telephone-number').length > 0 && $jRB('#applicant-telephone-number').val() == "" && typeof sh_a_Tel !== "undefined" && sh_a_Tel != "") {
        $jRB('#applicant-telephone-number').val(sh_a_Tel);
    }
    if ($jRB('#applicant-mobile-number').length > 0 && $jRB('#applicant-mobile-number').val() == "" && typeof sh_a_Mob !== "undefined" && sh_a_Mob != "") {
        $jRB('#applicant-mobile-number').val(sh_a_Mob);
    }
    if ($jRB('#applicant-email-address').length > 0 && $jRB('#applicant-email-address').val() == "" && typeof sh_a_Eml !== "undefined" && sh_a_Eml != "") {
        $jRB('#applicant-email-address').val(sh_a_Eml);
    }

    if ($jRB('#contact-government-id').length > 0 && $jRB('#contact-government-id').val() == "" && typeof sh_a_contactGovId !== "undefined" && sh_a_contactGovId != "") {
        $jRB('#contact-government-id').val(sh_a_contactGovId);
    }
    if ($jRB('#card-number').length > 0 && $jRB('#card-number').val() == "" && typeof sh_a_cardNum !== "undefined" && sh_a_cardNum != "") {
        $jRB('#card-number').val(sh_a_cardNum);
    }
    if ($jRB('#applicant-full-name').length > 0 && $jRB('#applicant-full-name').val() == "") {
        if (typeof sh_a_contactName !== "undefined" && sh_a_contactName != "") {
            $jRB('#applicant-full-name').val(sh_a_contactName);
        } else {
            if ($jRB('#billing\\:firstname').length > 0 && $jRB('#billing\\:lastname').length > 0) {
                $jRB('#applicant-full-name').val($jRB('#billing\\:firstname').val() + " " + $jRB('#billing\\:lastname').val());
            }
        }
    }

    /*
     * After bindings, also do transfer-current-field-data-into-Resurs-data-fields-call!
     * The TransField-function will return a true as long as the fields exists, even if they are empty, which makes us be able to
     * use the fieldtransfer with other vital functions.
     *
     */
    MagentoResTransField(checkFormEmail, $jRB('#dynamicForm' + sh_Method).children().find('input#applicant-email-address'), false);
    MagentoResTransField(checkFormPhone, $jRB('#dynamicForm' + sh_Method).children().find('input#applicant-telephone-number'), true);
    MagentoResTransField(checkFormMobile, $jRB('#dynamicForm' + sh_Method).children().find('input#applicant-mobile-number'), true);
}

function CreateValidation(Object, ClassName, ResursDataFormatName) {
    if (Object.length > 0) {
        Object.addClass(ClassName);
        if ((typeof resursDataFormats['formElement'] !== "undefined" && typeof resursDataFormats['formElement'][ResursDataFormatName] !== "undefined" && typeof resursDataFormats['formElement'][ResursDataFormatName]["mandatory"] === "undefined") || (typeof resursDataFormats['formElement'] !== "undefined" && typeof resursDataFormats['formElement'][ResursDataFormatName] !== "undefined" && typeof resursDataFormats['formElement'][ResursDataFormatName]["mandatory"] === false)) {
            if (typeof resursDataFormats['formElement'][ResursDataFormatName]['description'] !== "undefined") {
                Validation.add(ClassName, resursDataFormats['formElement'][ResursDataFormatName]['description'], function (value, obj) {
                    var returnValue = false;
                    var objectid = obj.id;
                    if (typeof resursDataFormats['formElement'][objectid] !== "undefined") {
                        var dataFormat = (resursDataFormats['formElement'][objectid]['format']);
                        try {
                            if (value.length > 0) {
                                var regExp = new RegExp(dataFormat, 'gi');
                                returnValue = regExp.test(value);
                            } else {
                                returnValue = true;
                            }
                        } catch (e) {
                            console.log(e);
                        }
                        return returnValue;
                    }
                    return true;
                });
            }
        } else {
            if (typeof resursDataFormats['formElement'] !== "undefined" && typeof resursDataFormats['formElement'][ResursDataFormatName] !== "undefined" && typeof resursDataFormats['formElement'][ResursDataFormatName]['description'] !== "undefined") {
                Validation.add(ClassName, resursDataFormats['formElement'][ResursDataFormatName]['description'], function (value, obj) {
                    var returnValue = false;
                    var objectid = obj.id;
                    if (typeof resursDataFormats['formElement'][objectid] !== "undefined") {
                        var dataFormat = (resursDataFormats['formElement'][objectid]['format']);
                        try {
                            var regExp = new RegExp(dataFormat, 'gi');
                            returnValue = regExp.test(value);
                        } catch (e) {
                            console.log(e);
                        }
                        if (!returnValue) {
                            $jRB('#f_' + objectid).show();
                        }
                        return returnValue;
                    }
                    return true;
                });
            }
        }
    }
}

function ClearResursForms(CurrentMethod) {
    if (typeof sh_Method !== "undefined" && sh_Method != "") {
        if ($jRB('#applicant-government-id').length > 0 && $jRB('#applicant-government-id').val() != "" && typeof sh_a_GovId !== "undefined") {
            sh_a_GovId = $jRB('#applicant-government-id').val();
        }
        if ($jRB('#applicant-telephone-number').length > 0 && $jRB('#applicant-telephone-number').val() != "" && typeof sh_a_Tel !== "undefined") {
            sh_a_Tel = $jRB('#applicant-telephone-number').val();
        }
        if ($jRB('#applicant-mobile-number').length > 0 && $jRB('#applicant-mobile-number').val() != "" && typeof sh_a_Mob !== "undefined") {
            sh_a_Mob = $jRB('#applicant-mobile-number').val();
        }
        if ($jRB('#applicant-email-address').length > 0 && $jRB('#applicant-email-address').val() != "" && typeof sh_a_Eml !== "undefined") {
            sh_a_Eml = $jRB('#applicant-email-address').val();
        }
    }
}

function ResursDynamicFormLoader(dynamicPaymentMethod, routeName, dynFormURL) {
    if (sh_MethodChoice) {
        return;
    }
    sh_MethodChoice = true;
    ClearResursForms(dynamicPaymentMethod);
    sh_Method = dynamicPaymentMethod;

    /* Special rules for FC-Readmore */
    if(routeName=='firecheckout')
    {
        $jRB('.payment-method-list-readme').css('margin-top','-10px'); // -24 to -10
        $jRB('.readme-popup-box'+dynamicPaymentMethod).prev().css('margin-top','-12px'); // -34 to -20 (-12)
    }
    postQueryTry++;
    postQuery = {
        url: dynFormURL,
        data: { "paymentId": dynamicPaymentMethod }
    };

    postQueryStart = Math.round((new Date()).getTime() / 1000);
    $jRB.ajax(
        {
            type: "POST",
            url: dynFormURL,
            data: { "paymentId": dynamicPaymentMethod },
            xhrFields: {withCredentials: true},
            timeout: 30000
        }
    ).done(
        function (result) {
            ResursDynamicFormHandler(result, dynamicPaymentMethod, false);
        }
    ).error(
        function (request, status, error) {
            console.log("ResursDynamicFormLoader Error: " + request + ", Status - " + status + ", Message " + error);
            ResursDynamicFormHandler(error, dynamicPaymentMethod, true);
        }
    );
}

function ResursDynamicFormHandler(content, dynamicPaymentMethod, error) {
    var currentDynamicForm = $jRB('#dynamicForm' + dynamicPaymentMethod);
    sh_MethodChoice = false;
    postQueryStop = Math.round((new Date()).getTime() / 1000);
    var postQueryDiff = postQueryStop - postQueryStart;

    //console.log("ResursDynamicFormHandlerConfirm [" + dynamicPaymentMethod + "] - " + postQueryDiff + "s");
    if (error === false) {
        currentDynamicForm.html(content);
        if (currentDynamicForm.find('input').length > 1) {
            $jRB('#from-ajax-loader' + dynamicPaymentMethod).hide();
            var formFields = [];
            var formFieldObjects = {};
            /* Collecting formfield names */
            /* Fallback: is_dynamic_form_loaded_<methodid>, meaning the length of this array will always be 1 or more. */
            currentDynamicForm.find('input').each(function () {
                formFields.push(this.id);
                formFieldObjects[this.id] = this;
            });
        } else {
            $jRB('#from-ajax-loader' + dynamicPaymentMethod).hide();
            currentDynamicForm.html("Something went wrong during formloading: " + content);
        }

        FetchMagentoFields();

    } else {
        if (postQueryTry > 1) {
            currentDynamicForm.html("Something went wrong during formloading<br>\n" + content);
        } else {
            console.log("Something went wrong on first ResursDynFormLoaderCall. Retrying!");
            ResursDynamicFormLoader(dynamicPaymentMethod, resursRoute, resursDynForm);
        }
    }
}
function DetectPreclickedForms() {
    if (typeof sh_Method !== "undefined") {
        console.log("Looking at sh_Method, that is currently configured to '"+sh_Method+"'");
    }
}